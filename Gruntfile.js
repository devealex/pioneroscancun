module.exports = function(grunt) {

	grunt.initConfig({
		pkg: grunt.file.readJSON('package.json'),
		cssmin: {
		  	target: {
		    	files: [{
					expand: true,
					cwd: 'assets/css/build/pioneros',
					src: ['*.css', '*.min.css'],
					dest: 'assets/css',
					ext: '.min.css',
					cwd: 'assets/css/build/plugins',
					src: ['*.css','*.min.css'],
					dest: 'assets/css/components/',
					ext: '.min.css',
					cwd: 'assets/manager/css/build/',
					src: ['*.css', '*.min.css'],
					dest: 'assets/manager/css',
					ext: '.min.css',
				}]
			}
		},

		stylus: {
		  	compile: {
				files: {
					'assets/css/build/pioneros/pioneros.css' : 'assets/css/build/website.pioneros.styl',
					'assets/css/pioneros.min.css' : 'assets/css/build/pioneros/pioneros.css',
					'assets/manager/css/manager.min.css' : 'assets/manager/css/build/manager.pioneros.styl'
				}
		  	}
		},

		concat: {
		    dist: {
		      	src: ['assets/css/components/*.css'],
		      	dest: 'assets/css/components.css',
		    }
		},

		uglify : {
			my_target: {
		     	files: {
		        	'assets/js/pioneros.js': ['assets/js/build/pioneros/*.js'],
					'assets/js/components.js': ['assets/js/build/plugins/*.js'],
					'assets/manager/js/components.js' : ['assets/manager/js/build/components/*.js']
		      	}
		    }
		},

		//Watch changes
		watch: {
			css: {
				files: ['assets/css/build/*.styl', 'assets/css/build/plugins/*.css', 'assets/js/build/pioneros/*.js', 'assets/js/build/plugins/*.js', 'assets/manager/js/build/components/*.js', 'assets/manager/css/build/*.styl'],
				tasks: ['stylus', 'cssmin', 'concat', 'uglify']
			}
		}
	});

	// Load the plugin that provides the "uglify" task.
	grunt.loadNpmTasks('grunt-contrib-stylus');
	grunt.loadNpmTasks('grunt-contrib-cssmin');
	grunt.loadNpmTasks('grunt-contrib-concat');
	grunt.loadNpmTasks('grunt-contrib-uglify');
	grunt.loadNpmTasks('grunt-contrib-watch');

	// Default task(s).
	grunt.registerTask('default', ['stylus', 'cssmin', 'concat', 'uglify']);

}
