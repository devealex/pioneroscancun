ALTER TABLE torneos ADD COLUMN torneourl varchar(100) AFTER torneo;

CREATE TABLE goleo (
	idgoleo bigint primary key auto_increment,
    fktorneo bigint not null,
    nombre varchar(100),
    equipo varchar(100),
    goles int default 0,
    foreign key (fktorneo) references torneos(idTorneo) on delete cascade
);