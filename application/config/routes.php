<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/

$route['default_controller'] = 'pages/view';

/***********************FRONT************************/
//Escuelas
$route['escuelas'] = 'schools/traficFront/';
//Noticias
$route['noticias'] = 'news/traficFront/';
$route['noticias/(:any)'] = 'news/traficFront/$1';
$route['noticias/(:any)/(:any)'] = 'news/traficFront/$1/$2';
//Planteles
$route['planteles'] = 'planteles/index';
$route['plantel/(:any)'] = 'planteles/traficFront/$1';
//Palmares
$route['palmares'] = 'palmares/traficFront';
//Galerias
$route['galerias'] = 'galleries/traficFront';
//Videos
$route['videos'] = 'videos/traficFront';

/***********************LOGIN************************/
$route['manager'] = 'pages/login';
$route['manager/login'] = 'login/login/';
$route['manager/login/logout'] = 'login/logout/';

/***********************CRUDS************************/
//Noticias
$route['manager/noticias/crud/(:any)'] = 'news/crud/$1';
//Escuelas
$route['manager/escuelas/crud/(:any)'] = 'schools/crud/$1';
//Contacto
$route['manager/contact/contactus'] = 'contact/contactUs/';
//Subscriptions
$route['manager/subscriptions/subscribe'] = 'subscriptions/subscribe/';
$route['manager/subscriptions/document'] = 'subscriptions/downloadExcel/';
//Usuarios
$route['manager/usuarios/crud/(:any)'] = 'users/crud/$1';
//Categorias
$route['manager/categories/crud/(:any)'] = 'categories/crud/$1';
//Planteles
$route['manager/planteles/crud/(:any)'] = 'planteles/crud/$1';
//Jugadores
$route['manager/jugadores/listview/(:num)'] = 'players/listview/$1';
$route['manager/jugadores/create/(:num)'] = 'players/createView/$1';
$route['manager/jugadores/edit/(:num)/(:num)'] = 'players/editView/$1/$2';
$route['manager/jugadores/crud/(:any)'] = 'players/crud/$1';
//Palmares
$route['manager/palmares/crud/(:any)'] = 'palmares/crud/$1';
//Galleries
$route['manager/galerias/crud/(:any)'] = 'galleries/crud/$1';
$route['manager/imagenes/(:num)'] = 'galleries/images/$1';
$route['manager/imagenes/crud/(:any)'] = 'galleries/crudImage/$1';
//Videos
$route['manager/videos/crud/(:any)'] = 'videos/crud/$1';
//Sponsors
$route['manager/patrocinadores/crud/(:any)'] = 'sponsors/crud/$1';
//Torneos
$route['manager/torneos/crud/(:any)'] = 'torneos/crud/$1';
//Calendario
$route['manager/calendario/crud/(:any)'] = 'calendario/crud/$1';
//Equipos
$route['manager/equipos/crud/(:any)'] = 'equipos/crud/$1';
//Estadisticas
$route['manager/estadisticas/crud/(:any)'] = 'estadisticas/crud/$1';
//Goleo
$route['manager/goleo/crud/(:any)'] = 'goleo/crud/$1';

$route['manager/(:any)/(:any)'] = 'manager/trafic/$1/$2';
$route['manager/(:any)/edit/(:any)'] = 'manager/traficEdit/$1/$2';

/***********************ANY************************/
$route['(:any)'] = 'pages/view/$1';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;
