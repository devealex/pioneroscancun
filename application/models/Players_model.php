<?php
class Players_model extends CI_Model {
	public function __construct() {
		$this->load->database();
	}

	//Return team
	public function get_players_by_team($idteam) {

		$players = null;

		$this->db->where('playerAvailable', 1);
		$this->db->where('plantel', $idteam);
		$this->db->order_by('playerNumber', 'ASC');
		$this->db->order_by('playerFunction', 'ASC');
		$query = $this->db->get('players');

		if($query->num_rows() > 0) {
			$por = $def = $med = $del = $tec = array();
			
			foreach ($query->result() as $row) {
				
				//Porteros
				if($row->playerPosition == 1) {
					array_push($por, array('img' => $row->playerPhotos,
											'name' => $row->playerName,
											'number' => $row->playerNumber,
											'function' => $row->playerFunction,
											'position' => $row->playerPosition));
				}

				//Porteros
				if($row->playerPosition == 2) {
					array_push($def, array('img' => $row->playerPhotos,
											'name' => $row->playerName,											
											'number' => $row->playerNumber,
											'function' => $row->playerFunction,
											'position' => $row->playerPosition));
				}
				
				//Porteros
				if($row->playerPosition == 3) {
					array_push($med, array('img' => $row->playerPhotos,
											'name' => $row->playerName,
											'number' => $row->playerNumber,
											'function' => $row->playerFunction,
											'position' => $row->playerPosition));
				}

				//Porteros
				if($row->playerPosition == 4) {
					array_push($del, array('img' => $row->playerPhotos,
											'name' => $row->playerName,
											'number' => $row->playerNumber,
											'function' => $row->playerFunction,
											'position' => $row->playerPosition));
				}

				//Porteros
				if($row->playerPosition == 5) {
					array_push($tec, array('img' => $row->playerPhotos,
											'name' => $row->playerName,
											'number' => $row->playerNumber,
											'function' => $row->playerFunction,
											'position' => $row->playerPosition));
				}

			}

			$players = array('p' => $por, 'd' => $def, 'm' => $med,  'g' => $del, 't' => $tec);
		}

		return $players;
	}

    public function get_players($idPlantel = null, $idPlayer  = null, $slug = null, $state = null) {
        $this->db->select('p.*, pl.*');
        $this->db->from('players p');
        $this->db->join('planteles pl', 'p.plantel = pl.idPlantel');
        if ($idPlayer != null) {
            $this->db->where('idPlayer', $idPlayer);
        }
        if ($idPlantel != null) {
            $this->db->where('idPlantel', $idPlantel);
        }
		if ($slug != null) {
            $this->db->where('plantelSlug', $slug);
        }
		if ($state != null) {
            $this->db->where('playerAvailable', $state);
        }
        $this->db->order_by('idPlayer', 'desc');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function create_player($data) {
        try {
			$this->db->insert('players', $data);
			return json_encode([
				'msg' => 'El jugador se agrego con exito',
				'state' => 1
			]);
		} catch (Exception $e) {
			return json_encode([
				'msg' => 'Hubo un error al crear el jugador. (Intentelo mas tarde)',
				'state' => 0
			]);
		}
    }

    public function edit_player($data, $idPlayer) {
		try {
			$this->db->where('idPlayer', $idPlayer);
			$this->db->update('players', $data);
			return json_encode([
				'msg' => 'El jugador se modifico con exito',
				'state' => 1
			]);
		} catch (Exception $e) {
			return json_encode([
				'msg' => 'Hubo un error al editar el jugador. (Intentelo mas tarde)',
				'state' => 0
			]);
		}
	}

    public function delete_player($idPlayer) {
		try {
			$this->db->delete('players', array('idPlayer' => $idPlayer));
			return json_encode([
                'msg' => 'El jugador fue eliminado',
                'state' => 1
            ]);
		} catch (Exception $e) {
			return json_encode([
                'msg' => 'Hubo un error al eliminar el jugador. (Intentelo mas tarde)',
                'state' => 0
            ]);
		}
	}

}
