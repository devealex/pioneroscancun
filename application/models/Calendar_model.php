<?php
class Calendar_model extends CI_Model
{
	public function __construct()
	{
		$this->load->database();
	}

	public function get_calendario($idCalendario = null, $idTorneo = null, $jornadaRol = null, $torneourl = null, $limit = null)
	{
		$this->db->select('cl.*, tr.*, pl.*, eql.equipoName as locName, eql.equipoLogo as locLogo, eqv.equipoName as visName, eqv.equipoLogo as visLogo');
		$this->db->from('calendario cl');
		$this->db->join('torneos tr', 'cl.fkTorneoRol = tr.idTorneo');
		$this->db->join('planteles pl', 'tr.torneoPlantel = pl.idPlantel');
		$this->db->join('equipos eql', 'cl.localRol = eql.idEquipo');
		$this->db->join('equipos eqv', 'cl.visitaRol = eqv.idEquipo');
		if ($idCalendario != null) {
			$this->db->where('cl.idCalendario', $idCalendario);
		}
		if ($idTorneo != null) {
			$this->db->where('cl.fkTorneoRol', $idTorneo);
		}
		if ($jornadaRol != null) {
			$this->db->where('cl.jornadaRol', $jornadaRol);
		}
		if ($limit != null) {
			$this->db->limit($limit);
		}
		if ($torneourl != null) {
			$this->db->where('tr.torneourl', $torneourl);
			$this->db->order_by('cl.jornadaRol', 'asc');
		} else {
			$this->db->order_by('cl.playDay', 'desc');
		}
		$query = $this->db->get();
		return $query->result_array();
	}

	public function create_calendario($data)
	{
		try {
			$this->db->insert('calendario', $data);
			return json_encode([
				'msg' => 'El calendario se agrego con exito',
				'state' => 1
			]);
		} catch (Exception $e) {
			return json_encode([
				'msg' => 'Hubo un error al crear el calendario. (Intentelo mas tarde)',
				'state' => 0
			]);
		}
	}

	public function edit_calendario($data, $idCalendario)
	{
		try {
			$this->db->where('idCalendario', $idCalendario);
			$this->db->update('calendario', $data);
			return json_encode([
				'msg' => 'El calendario se modifico con exito',
				'state' => 1
			]);
		} catch (Exception $e) {
			return json_encode([
				'msg' => 'Hubo un error al editar el calendario. (Intentelo mas tarde)',
				'state' => 0
			]);
		}
	}

	public function delete_calendario($idCalendario)
	{
		try {
			$this->db->delete('calendario', array('idCalendario' => $idCalendario));
			return json_encode([
				'msg' => 'El calendario fue eliminado',
				'state' => 1
			]);
		} catch (Exception $e) {
			return json_encode([
				'msg' => 'Hubo un error al eliminar el calendario. (Intentelo mas tarde)',
				'state' => 0
			]);
		}
	}
}
