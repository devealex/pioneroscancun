<?php
class Palmares_model extends CI_Model {

	public function __construct() {
		$this->load->database();
	}

    public function get_palmares($idChampionship = null, $limit = null) {
        if ($idChampionship != null) {
            $this->db->where('idChampionship', $idChampionship);
		}
		if ($limit != null) {
			$this->db->limit($limit);
		}
		
		$this->db->order_by('championshipDate', 'DESC');
        $query = $this->db->get('championships');
        return $query->result_array();
    }

    public function delete_palmares($idChampionship){
		try {
			$this->db->delete('championships', array('idChampionship' => $idChampionship));
			return json_encode([
                'msg' => 'El campeonato fue eliminado',
                'state' => 1
            ]);
		} catch (Exception $e) {
			return json_encode([
                'msg' => 'Hubo un error al eliminar el campeonato. (Intentelo mas tarde)',
                'state' => 0
            ]);
		}
	}

    public function create_palmares($data) {
		try {
			$this->db->insert('championships', $data);
			return json_encode([
				'msg' => 'El campeonato se registro con exito',
				'state' => 1
			]);
		} catch (Exception $e) {
			return json_encode([
				'msg' => 'Hubo un error al registrar el campeonato. (Intentelo mas tarde)',
				'state' => 0
			]);
		}
	}

    public function edit_palmares($data, $idChampionship) {
		try {
			$this->db->where('idChampionship', $idChampionship);
			$this->db->update('championships', $data);
			return json_encode([
				'msg' => 'El campeonato se modifico con exito',
				'state' => 1
			]);
		} catch (Exception $e) {
			return json_encode([
				'msg' => 'Hubo un error al editar el campeonato. (Intentelo mas tarde)',
				'state' => 0
			]);
		}
	}

}
