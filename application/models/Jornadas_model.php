<?php
class Jornadas_model extends CI_Model {
	public function __construct() {
		$this->load->database();
	}

    public function get_jornadas($idJornada = null, $torneoJornada = null) {
		$this->db->select('j.*, tr.*');
        $this->db->from('jornadas j');
		$this->db->join('torneos tr', 'j.torneoJornada = tr.idTorneo');
        if ($idJornada != null) {
            $this->db->where('idJornada', $idJornada);
        }
        if ($torneoJornada != null) {
            $this->db->where('torneoJornada', $torneoJornada);
        }
        $this->db->order_by('idJornada', 'desc');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function create_jornada($data) {
        try {
			$this->db->insert('jornadas', $data);
			return json_encode([
				'msg' => 'La jornada se agrego con exito',
				'idJornada' => $this->db->insert_id(),
				'state' => 1
			]);
		} catch (Exception $e) {
			return json_encode([
				'msg' => 'Hubo un error al crear la jornada. (Intentelo mas tarde)',
				'state' => 0
			]);
		}
    }

    public function edit_jornada($data, $idJornada) {
		try {
			$this->db->where('idJornada', $idJornada);
			$this->db->update('jornadas', $data);
			return json_encode([
				'msg' => 'La jornada se modifico con exito',
				'state' => 1
			]);
		} catch (Exception $e) {
			return json_encode([
				'msg' => 'Hubo un error al editar la jornada. (Intentelo mas tarde)',
				'state' => 0
			]);
		}
	}

    public function delete_jornada($idJornada) {
		try {
			$this->db->delete('jornadas', array('idJornada' => $idJornada));
			return json_encode([
                'msg' => 'La jornada fue eliminado',
                'state' => 1
            ]);
		} catch (Exception $e) {
			return json_encode([
                'msg' => 'Hubo un error al eliminar la jornada. (Intentelo mas tarde)',
                'state' => 0
            ]);
		}
	}

}
