<?php

class Users_model extends CI_Model
{

    function __construct()
    {
        $this->load->database();
    }

    public function get_users($idUser = null) {
        if ($idUser != null) {
            $this->db->where('idUser', $idUser);
        }
        $query = $this->db->get('users');
        return $query->result_array();
    }

    public function delete_user($idUser){
		try {
			$this->db->delete('users', array('idUser' => $idUser));
			return json_encode([
                'msg' => 'El usuario fue eliminado.',
                'state' => 1
            ]);
		} catch (Exception $e) {
			return json_encode([
                'msg' => 'Error al eliminar al usuario. (Intentelo mas tarde)',
                'state' => 0
            ]);
		}
	}

    public function create_user($data) {
		try {
			$this->db->insert('users', $data);
			return json_encode([
				'msg' => 'Usuario Registrado',
				'state' => 1
			]);
		} catch (Exception $e) {
			return json_encode([
				'msg' => 'Error al registrar al usuario. (Intentelo mas tarde)',
				'state' => 0
			]);
		}
	}

    public function edit_user($data, $idUser) {
		try {
			$this->db->where('idUser', $idUser);
			$this->db->update('users', $data);
			return json_encode([
				'msg' => 'Usuario Modificado',
				'state' => 1
			]);
		} catch (Exception $e) {
			return json_encode([
				'msg' => 'Error al modificar al usuario. (Intentelo mas tarde)',
				'state' => 0
			]);
		}
	}

}
