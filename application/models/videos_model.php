<?php
class Videos_model extends CI_Model {
	public function __construct() {
		$this->load->database();
	}

    public function get_videos($idVideo = null, $videoPublish = null, $limit = null) {
        if ($idVideo != null) {
            $this->db->where('idVideo', $idVideo);
        }
        if ($videoPublish != null) {
            $this->db->where('videoPublish <=', $videoPublish);
		}
		if ($limit != null) {
			$this->db->limit($limit);
		}
		
		$this->db->order_by('videoPublish', 'DESC');
        $query = $this->db->get('videos');
        return $query->result_array();
	}
	
	//Return videos with limit
	public function get_videos_limit($no) {

		$limit = filter_var($no, FILTER_SANITIZE_NUMBER_INT);

		$this->db->order_by('videoPublish', 'DESC');
		$this->db->limit($limit);
		$query = $this->db->get('videos');

		return $query->result_array();
	}

	public function delete_video($idVideo) {
		try {
			$this->db->delete('videos', array('idVideo' => $idVideo));
			return json_encode([
                'msg' => 'El video fue eliminado',
                'state' => 1
            ]);
		} catch (Exception $e) {
			return json_encode([
                'msg' => 'Hubo un error al eliminar el video. (Intentelo mas tarde)',
                'state' => 0
            ]);
		}
	}

    public function create_video($data) {
		try {
			$this->db->insert('videos', $data);
			return json_encode([
				'msg' => 'El video se registro con exito',
				'state' => 1
			]);
		} catch (Exception $e) {
			return json_encode([
				'msg' => 'Hubo un error al registrar el video. (Intentelo mas tarde)',
				'state' => 0
			]);
		}
	}

    public function edit_video($data, $idVideo) {
		try {
			$this->db->where('idVideo', $idVideo);
			$this->db->update('videos', $data);
			return json_encode([
				'msg' => 'El video se modifico con exito',
				'state' => 1
			]);
		} catch (Exception $e) {
			return json_encode([
				'msg' => 'Hubo un error al editar el video. (Intentelo mas tarde)',
				'state' => 0
			]);
		}
	}

}
