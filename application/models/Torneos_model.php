<?php
class Torneos_model extends CI_Model {
	public function __construct() {
		$this->load->database();
	}

	public function get_ultimo_proximo_partido($isUltimo = true, $fecha) {
		if ($isUltimo == true) { //Ultimo Partido
			$this->db->where('playDay <=', $fecha);
			$this->db->order_by('playDay', 'DESC');
		}
		if ($isUltimo == false) { //Proximo Partido
			$this->db->where('playDay >', $fecha);
			$this->db->order_by('playDay', 'ASC');
		}
		$this->db->limit(1);
		$query = $this->db->get('calendario');
		$rs = [];
		foreach ($query->result_array() as $calendar) {
			$rs = [
				'jornada' => $calendar['jornadaRol'],
				'EQlocal' => $this->get_equipos(null, $calendar['localRol'])[0],
				'EQvisita' => $this->get_equipos(null, $calendar['visitaRol'])[0],
				'playDay' => $calendar['playDay'],
				'localGol' => $calendar['localGol'],
				'visitaGol' => $calendar['visitaGol'],
				'torneo' => $this->get_torneos($calendar['fkTorneoRol'])[0]
			];
		}
		return $rs;
	}

    public function get_torneos($idTorneo = null) {
        $this->db->select('t.*, pl.*');
        $this->db->from('torneos t');
        $this->db->join('planteles pl', 't.torneoPlantel = pl.idPlantel');
        if ($idTorneo != null) {
            $this->db->where('idTorneo', $idTorneo);
        }
        $this->db->order_by('idTorneo', 'desc');
        $query = $this->db->get();
        return $query->result_array();
	}
	
	public function get_equipos($idTorneo = null, $idEquipo = null) {
		$this->db->select('te.*, t.*, e.*');
        $this->db->from('torneo_equipos te');
		$this->db->join('torneos t', 'te.fkTorneo = t.idTorneo');
		$this->db->join('equipos e', 'te.fkEquipo = e.idEquipo');
        if ($idTorneo != null) {
            $this->db->where('te.fkTorneo', $idTorneo);
		}
		if ($idEquipo != null) {
            $this->db->where('te.fkEquipo', $idEquipo);
        }
		$this->db->order_by('te.idTorneoEquipo', 'desc');
        $query = $this->db->get();
        return $query->result_array();
	}

	public function create_equipos($data) {
		try {
			$this->db->insert('torneo_equipos', $data);
			return json_encode([
				'msg' => 'El equipo se agrego con exito',
				'idTorneoEquipo' => $this->db->insert_id(),
				'state' => 1
			]);
		} catch (Exception $e) {
			return json_encode([
				'msg' => 'Hubo un error al crear el equipo. (Intentelo mas tarde)',
				'state' => 0
			]);
		}
	}

	public function delete_equipos($idTorneo = null, $idEquipo = null) {
		try {
			if ($idTorneo != null && $idEquipo == null) {
				$this->db->delete('torneo_equipos', array('fkTorneo' => $idTorneo));
			}
			if ($idEquipo != null && $idTorneo == null) {
				$this->db->delete('torneo_equipos', array('fkEquipo' => $idEquipo));
			}
			if ($idTorneo != null && $idEquipo != null) {
				$this->db->delete('torneo_equipos', array('fkTorneo' => $idTorneo, 'fkEquipo' => $idEquipo));
			}
			return json_encode([
                'msg' => 'El equipo fue eliminado',
                'state' => 1
            ]);
		} catch (Exception $e) {
			return json_encode([
                'msg' => 'Hubo un error al eliminar el equipo. (Intentelo mas tarde)',
                'state' => 0
            ]);
		}
	}

    public function create_torneo($data) {
        try {
			$this->db->insert('torneos', $data);
			return json_encode([
				'msg' => 'El torneo se agrego con exito',
				'idTorneo' => $this->db->insert_id(),
				'state' => 1
			]);
		} catch (Exception $e) {
			return json_encode([
				'msg' => 'Hubo un error al crear el torneo. (Intentelo mas tarde)',
				'state' => 0
			]);
		}
    }

    public function edit_torneo($data, $idTorneo) {
		try {
			$this->db->where('idTorneo', $idTorneo);
			$this->db->update('torneos', $data);
			return json_encode([
				'msg' => 'El torneo se modifico con exito',
				'state' => 1
			]);
		} catch (Exception $e) {
			return json_encode([
				'msg' => 'Hubo un error al editar el torneo. (Intentelo mas tarde)',
				'state' => 0
			]);
		}
	}

    public function delete_torneo($idTorneo) {
		try {
			$this->db->delete('torneos', array('idTorneo' => $idTorneo));
			return json_encode([
                'msg' => 'El torneo fue eliminado',
                'state' => 1
            ]);
		} catch (Exception $e) {
			return json_encode([
                'msg' => 'Hubo un error al eliminar el torneo. (Intentelo mas tarde)',
                'state' => 0
            ]);
		}
	}

}
