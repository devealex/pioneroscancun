<?php
class Schools_model extends CI_Model {
	public function __construct() {
		$this->load->database();
	}

	public function get_schools($idSchool = null, $active = null) {
		if ($active != null) {
			$this->db->where('state', $active);
		}
		if ($idSchool != null) {
			$this->db->where('idSchool', $idSchool);
		}
		$this->db->order_by('registrationDate', 'desc');
		$query = $this->db->get('schools');
		return $query->result_array();
	}

	public function delete_school($idSchool){
		try {
			$this->db->delete('schools', array('idSchool' => $idSchool));
			return json_encode([
                'msg' => 'La Escuela fue eliminada',
                'state' => 1
            ]);
		} catch (Exception $e) {
			return json_encode([
                'msg' => 'Hubo un error al eliminar la Escuela. (Intentelo mas tarde)',
                'state' => 0
            ]);
		}
	}

	public function create_school($data) {
		try {
			$this->db->insert('schools', $data);
			return json_encode([
				'msg' => 'La Escuela se registro con exito',
				'state' => 1
			]);
		} catch (Exception $e) {
			return json_encode([
				'msg' => 'Hubo un error al registrar la Escuela. (Intentelo mas tarde)',
				'state' => 0
			]);
		}
	}

	public function edit_school($data, $idSchool) {
		try {
			$this->db->where('idSchool', $idSchool);
			$this->db->update('schools', $data);
			return json_encode([
				'msg' => 'La Escuela se modifico con exito',
				'state' => 1
			]);
		} catch (Exception $e) {
			return json_encode([
				'msg' => 'Hubo un error al editar la Escuela. (Intentelo mas tarde)',
				'state' => 0
			]);
		}
	}

}
