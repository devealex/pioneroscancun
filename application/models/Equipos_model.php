<?php
class Equipos_model extends CI_Model {
	public function __construct() {
		$this->load->database();
	}

    public function get_equipos($idEquipo = null) {
        if ($idEquipo != null) {
            $this->db->where('idEquipo', $idEquipo);
        }
        $this->db->order_by('idEquipo', 'desc');
        $query = $this->db->get('equipos');
        return $query->result_array();
    }

    public function create_equipo($data) {
        try {
			$this->db->insert('equipos', $data);
			return json_encode([
				'msg' => 'El equipo se agrego con exito',
				'state' => 1
			]);
		} catch (Exception $e) {
			return json_encode([
				'msg' => 'Hubo un error al crear el equipo. (Intentelo mas tarde)',
				'state' => 0
			]);
		}
    }

    public function edit_equipo($data, $idEquipo) {
		try {
			$this->db->where('idEquipo', $idEquipo);
			$this->db->update('equipos', $data);
			return json_encode([
				'msg' => 'El equipo se modifico con exito',
				'state' => 1
			]);
		} catch (Exception $e) {
			return json_encode([
				'msg' => 'Hubo un error al editar el equipo. (Intentelo mas tarde)',
				'state' => 0
			]);
		}
	}

    public function delete_equipo($idEquipo) {
		try {
			$this->db->delete('equipos', array('idEquipo' => $idEquipo));
			return json_encode([
                'msg' => 'El equipo fue eliminado',
                'state' => 1
            ]);
		} catch (Exception $e) {
			return json_encode([
                'msg' => 'Hubo un error al eliminar el equipo. (Intentelo mas tarde)',
                'state' => 0
            ]);
		}
	}

}
