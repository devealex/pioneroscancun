<?php
class Subscriptions_model extends CI_Model {
	public function __construct() {
		$this->load->database();
	}

	public function delete_subscription($email){
		try {
			$this->db->delete('subscriptions', array('email' => $email));
			return json_encode([
                'msg' => 'Te has desuscrito',
                'state' => 1
            ]);
		} catch (Exception $e) {
			return json_encode([
                'msg' => 'Hubo un error. (Intentelo mas tarde)',
                'state' => 0
            ]);
		}
	}

	public function create_subscription($data) {
		try {
			$this->db->insert('subscriptions', $data);
			return json_encode([
				'msg' => 'Suscripción Realizada',
				'state' => 1
			]);
		} catch (Exception $e) {
			return json_encode([
				'msg' => 'Hubo un error al suscribirte. (Intentelo mas tarde)',
				'state' => 0
			]);
		}
	}

	public function get_subscriptions($dateInitial = null, $dateFinal = null) {
		if ($dateInitial != null && $dateFinal != null) {
			$this->db->where('dateSubscription >=', date('Y-m-d', strtotime($dateInitial)));
			$this->db->where('dateSubscription <=', date('Y-m-d', strtotime($dateFinal)));
		}
		if ($dateInitial != null && $dateFinal == null) {
			$this->db->where('dateSubscription', date('Y-m-d', strtotime($dateInitial)));
		}
		$this->db->order_by('dateSubscription', 'DESC');
		$query = $this->db->get('subscriptions');
		return $query->result_array();
	}

}
