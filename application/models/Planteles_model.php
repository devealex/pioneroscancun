<?php
class Planteles_model extends CI_Model {
	public function __construct() {
		$this->load->database();
	}

    public function get_planteles($idPlantel = null, $slug = null) {
        if ($idPlantel != null) {
			$this->db->where('idPlantel', $idPlantel);
		}
		if ($slug != null) {
			$this->db->where('plantelSlug', $slug);
		}
        $query = $this->db->get('planteles');
        return $query->result_array();
    }

	public function delete_plantel($idPlantel) {
		try {
			$this->db->delete('planteles', array('idPlantel' => $idPlantel));
			return json_encode([
                'msg' => 'El plantel fue eliminado',
                'state' => 1
            ]);
		} catch (Exception $e) {
			return json_encode([
                'msg' => 'Hubo un error al eliminar el plantel. (Intentelo mas tarde)',
                'state' => 0
            ]);
		}
	}

	public function create_plantel($data) {
		try {
			$this->db->insert('planteles', $data);
			return json_encode([
				'msg' => 'El plantel se agrego con exito',
				'state' => 1
			]);
		} catch (Exception $e) {
			return json_encode([
				'msg' => 'Hubo un error al crear el plantel. (Intentelo mas tarde)',
				'state' => 0
			]);
		}
	}

	public function edit_plantel($data, $idPlantel) {
		try {
			$this->db->where('idPlantel', $idPlantel);
			$this->db->update('planteles', $data);
			return json_encode([
				'msg' => 'El plantel se modifico con exito',
				'state' => 1
			]);
		} catch (Exception $e) {
			return json_encode([
				'msg' => 'Hubo un error al editar el plantel. (Intentelo mas tarde)',
				'state' => 0
			]);
		}
	}

}
