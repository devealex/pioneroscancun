<?php
class Grupos_model extends CI_Model {
	public function __construct() {
		$this->load->database();
	}

    public function get_grupos($idGrupo = null) {
        $this->db->select('gr.*, eq.*, tr.*, pl.*');
        $this->db->from('grupos gr');
        $this->db->join('equipos eq', 'gr.fkEquipo = eq.idEquipo');
		$this->db->join('torneos tr', 'gr.fkTorneo = tr.idTorneo');
		$this->db->join('planteles pl', 'tr.torneoPlantel = pl.idPlantel');
        if ($idGrupo != null) {
            $this->db->where('gr.idGrupo', $idGrupo);
        }
        $this->db->order_by('gr.idGrupo', 'desc');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function create_grupo($data) {
        try {
			$this->db->insert('grupos', $data);
			return json_encode([
				'msg' => 'El grupo se agrego con exito',
				'state' => 1
			]);
		} catch (Exception $e) {
			return json_encode([
				'msg' => 'Hubo un error al crear el grupo. (Intentelo mas tarde)',
				'state' => 0
			]);
		}
    }

    public function edit_grupo($data, $idGrupo) {
		try {
			$this->db->where('idGrupo', $idGrupo);
			$this->db->update('grupos', $data);
			return json_encode([
				'msg' => 'El grupo se modifico con exito',
				'state' => 1
			]);
		} catch (Exception $e) {
			return json_encode([
				'msg' => 'Hubo un error al editar el grupo. (Intentelo mas tarde)',
				'state' => 0
			]);
		}
	}

    public function delete_grupo($idGrupo) {
		try {
			$this->db->delete('grupos', array('idGrupo' => $idGrupo));
			return json_encode([
                'msg' => 'El grupo fue eliminado',
                'state' => 1
            ]);
		} catch (Exception $e) {
			return json_encode([
                'msg' => 'Hubo un error al eliminar el grupo. (Intentelo mas tarde)',
                'state' => 0
            ]);
		}
	}

}
