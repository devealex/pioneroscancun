<?php
class Goleo_model extends CI_Model {
	public function __construct() {
		$this->load->database();
    }

    public function get_goleos($idTorneo = null, $torneourl = null) {
		$this->db->select('gl.*');
		$this->db->from('goleo gl');
		$this->db->join('torneos tr', 'gl.fktorneo = tr.idTorneo');
        if ($idTorneo != null) {
            $this->db->where('gl.fktorneo', $idTorneo);
		}
		if ($torneourl != null) {
            $this->db->where('tr.torneourl', $torneourl);
        }
        $this->db->order_by('goles', 'desc');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function add_goleo($data) {
        try {
			$this->db->insert('goleo', $data);
			return json_encode([
				'msg' => '',
				'state' => 1
			]);
		} catch (Exception $e) {
			return json_encode([
				'msg' => '',
				'state' => 0
			]);
		}
    }

    public function edit_goleo($idgoleo, $data) {
        try {
			$this->db->where('idgoleo', $idgoleo);
			$this->db->update('goleo', $data);
			return json_encode([
				'msg' => 'Lista Modificada',
				'state' => 1
			]);
		} catch (Exception $e) {
			return json_encode([
				'msg' => 'Error al modificar este registro',
				'state' => 0
			]);
		}
    }

    public function delete_goleo($idgoleo) {
        try {
			$this->db->delete('goleo', array('idgoleo' => $idgoleo));
			return json_encode([
                'msg' => '',
                'state' => 1
            ]);
		} catch (Exception $e) {
			return json_encode([
                'msg' => '',
                'state' => 0
            ]);
		}
    }

}