<?php
class Sponsors_model extends CI_Model {

	public function __construct() {
		$this->load->database();
	}

    public function get_sponsors($idSponsor = null, $order = 'ASC', $random = false, $limit = null, $sponsorOrder = null) {
        if ($idSponsor != null) {
            $this->db->where('idSponsor', $idSponsor);
		}

		if ($sponsorOrder != null) {
            $this->db->where('sponsorOrder', $sponsorOrder);
		}

		if ($random == true && $limit != null) {
			$this->db->order_by('RAND()');
    		$this->db->limit($limit);
		} else {
			$this->db->order_by('sponsorOrder', $order);
		}
		
        $query = $this->db->get('sponsors');
        return $query->result_array();
    }

    public function delete_sponsors($idSponsor){
		try {
			$this->db->delete('sponsors', array('idSponsor' => $idSponsor));
			return json_encode([
                'msg' => 'El Patrocinador fue eliminado',
                'state' => 1
            ]);
		} catch (Exception $e) {
			return json_encode([
                'msg' => 'Hubo un error al eliminar el Patrocinador. (Intentelo mas tarde)',
                'state' => 0
            ]);
		}
	}

    public function create_sponsors($data) {
		try {
			$this->db->insert('sponsors', $data);
			return json_encode([
				'msg' => 'El Patrocinador se registro con exito',
				'state' => 1
			]);
		} catch (Exception $e) {
			return json_encode([
				'msg' => 'Hubo un error al registrar el Patrocinador. (Intentelo mas tarde)',
				'state' => 0
			]);
		}
	}

}
