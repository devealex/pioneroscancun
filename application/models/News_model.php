<?php
class News_model extends CI_Model {
	public function __construct() {
		$this->load->database();
	}

	public function get_news($idNew = null, $slug = null, $urlNew = null, $published = null, $limit = null) {
		$this->db->select('n.*, nc.*');
		$this->db->from('news n');
		$this->db->join('newscategories nc', 'n.newsCategory = nc.idCategory');
		if ($slug != null) {
			$this->db->where('categorySlug', $slug);
		}
		if ($urlNew != null) {
			$this->db->where('newsUrl', $urlNew);
		}
		if ($idNew != null) {
			$this->db->where('idNew', $idNew);
		}
		if ($published != null) {
			$this->db->where('newsPublished <=', $published);
		}
		if ($limit != null) {
			$this->db->limit($limit);
		}
		$this->db->order_by('idNew', 'DESC');
		$query = $this->db->get();
		return $query->result_array();
    }

	public function delete_new($idNew){
		try {
			$this->db->delete('news', array('idNew' => $idNew));
			return json_encode([
                'msg' => 'La noticia fue eliminada',
                'state' => 1
            ]);
		} catch (Exception $e) {
			return json_encode([
                'msg' => 'Hubo un error al eliminar la noticia. (Intentelo mas tarde)',
                'state' => 0
            ]);
		}
	}

	public function create_new($data) {
		try {
			$this->db->insert('news', $data);
			return json_encode([
				'msg' => 'La noticia se agrego con exito',
				'state' => 1
			]);
		} catch (Exception $e) {
			return json_encode([
				'msg' => 'Hubo un error al crear la noticia. (Intentelo mas tarde)',
				'state' => 0
			]);
		}
	}

	public function edit_new($data, $idNew) {
		try {
			$this->db->where('idNew', $idNew);
			$this->db->update('news', $data);
			return json_encode([
				'msg' => 'La noticia se modifico con exito',
				'state' => 1
			]);
		} catch (Exception $e) {
			return json_encode([
				'msg' => 'Hubo un error al editar la noticia. (Intentelo mas tarde)',
				'state' => 0
			]);
		}
	}

	public function get_categories() {
		$query = $this->db->get('newscategories');
		return $query->result_array();
	}

}
