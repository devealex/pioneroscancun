<?php
class Estadisticas_model extends CI_Model {
	public function __construct() {
		$this->load->database();
	}
	
	public function get_estadisticas($idEstadistica = null , $fkTorneo = null, $fkEquipo = null, $url = null) {
        $this->db->select('est.*, te.*, tr.*, pl.*, eq.*');
        $this->db->from('estadisticas est');
		$this->db->join('torneo_equipos te', 'est.fkTorneoEquipos = te.idTorneoEquipo');
        $this->db->join('torneos tr', 'te.fkTorneo = tr.idTorneo');
        $this->db->join('planteles pl', 'tr.torneoPlantel = pl.idPlantel');
		$this->db->join('equipos eq', 'te.fkEquipo = eq.idEquipo');
        if ($idEstadistica != null) {
            $this->db->where('est.idEstadistica', $idEstadistica);
		}
		if ($fkTorneo != null) {
            $this->db->where('te.fkTorneo', $fkTorneo);
		}
		if ($fkEquipo != null) {
            $this->db->where('te.fkEquipo', $fkEquipo);
		}
		if ($url != null) {
            $this->db->where('tr.torneourl', $url);
        }
        $this->db->order_by('est.pts', 'desc');
        $query = $this->db->get();
        return $query->result_array();
    }

    public function create_estadistica($data) {
        try {
			$this->db->insert('estadisticas', $data);
			return json_encode([
				'msg' => 'Las estadisticas se agregaron con exito',
				'state' => 1
			]);
		} catch (Exception $e) {
			return json_encode([
				'msg' => 'Hubo un error al crear las estadisticas. (Intentelo mas tarde)',
				'state' => 0
			]);
		}
    }

    public function edit_estadistica($data, $idEstadistica) {
		try {
			$this->db->where('idEstadistica', $idEstadistica);
			$this->db->update('estadisticas', $data);
			return json_encode([
				'msg' => 'Las estadisticas se modificaron con exito',
				'state' => 1
			]);
		} catch (Exception $e) {
			return json_encode([
				'msg' => 'Hubo un error al editar las estadisticas. (Intentelo mas tarde)',
				'state' => 0
			]);
		}
	}

    public function delete_estadistica($idEstadistica) {
		try {
			$this->db->delete('estadisticas', array('idEstadistica' => $idEstadistica));
			return json_encode([
                'msg' => 'Las estadisticas fueron eliminadas',
                'state' => 1
            ]);
		} catch (Exception $e) {
			return json_encode([
                'msg' => 'Hubo un error al eliminar las estadisticas. (Intentelo mas tarde)',
                'state' => 0
            ]);
		}
	}

}
