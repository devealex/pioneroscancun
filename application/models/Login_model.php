<?php

class Login_model extends CI_Model
{

    function __construct()
    {
        $this->load->database();
    }

    public function verify_user($email, $pass) {
        $this->db->where('userMail', $email);
        $this->db->where('userPassword', $pass);
        $query = $this->db->get('users');
        return $query->result_array();
    }

}
