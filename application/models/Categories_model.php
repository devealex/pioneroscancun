<?php
class Categories_model extends CI_Model {
	public function __construct() {
		$this->load->database();
	}

	public function delete_category($idCategory){
		try {
			$this->db->delete('newscategories', array('idCategory' => $idCategory));
			return json_encode([
                'msg' => 'La categoria fue eliminada',
                'state' => 1
            ]);
		} catch (Exception $e) {
			return json_encode([
                'msg' => 'Hubo un error al eliminar la categoria. (Intentelo mas tarde)',
                'state' => 0
            ]);
		}
	}

	public function create_category($data) {
		try {
			$this->db->insert('newscategories', $data);
			return json_encode([
				'msg' => 'La categoria se agrego con exito',
				'state' => 1
			]);
		} catch (Exception $e) {
			return json_encode([
				'msg' => 'Hubo un error al crear la categoria. (Intentelo mas tarde)',
				'state' => 0
			]);
		}
	}

	public function edit_category($data, $idCategory) {
		try {
			$this->db->where('idCategory', $idCategory);
			$this->db->update('newscategories', $data);
			return json_encode([
				'msg' => 'La categoria se modifico con exito',
				'state' => 1
			]);
		} catch (Exception $e) {
			return json_encode([
				'msg' => 'Hubo un error al editar la categoria. (Intentelo mas tarde)',
				'state' => 0
			]);
		}
	}

}
