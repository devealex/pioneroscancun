<?php
class Gallery_model extends CI_Model {

	public function __construct() {
		$this->load->database();
	}

    //ALBUMS
    public function get_albums($idAlbum = null, $status = null, $limit = null) {
        if ($idAlbum != null) {
            $this->db->where('idAlbum', $idAlbum);
        }
        if ($status != null) {
            $this->db->where('albumPublish', $status);
		}
		if ($limit != null) {
			$this->db->limit($limit);
		}
		
		$this->db->order_by('albumRegister', 'DESC');
		$query = $this->db->get('albums');
		$rs = [];

		foreach ($query->result_array() as $album) {
			array_push($rs, [
				'idAlbum' => $album['idAlbum'],
				'albumName' => $album['albumName'],
				'albumAvatar' => $album['albumAvatar'],
				'albumRegister' => $album['albumRegister'],
				'albumPublish' => $album['albumPublish'],
				'count' => count($this->get_images(null, $album['idAlbum'])),
			]);
		}

        return $rs;
	}
	
	//Return albums with limit
	public function get_albums_limit($no) {

		$limit = filter_var($no, FILTER_SANITIZE_NUMBER_INT);

		$this->db->where('albumPublish', 1);
		$this->db->order_by('albumRegister', 'DESC');
		$this->db->limit($limit);
		$query = $this->db->get('albums');
		$rs = [];

		foreach ($query->result_array() as $album) {
			array_push($rs, [
				'idAlbum' => $album['idAlbum'],
				'albumName' => $album['albumName'],
				'albumAvatar' => $album['albumAvatar'],
				'albumRegister' => $album['albumRegister'],
				'albumPublish' => $album['albumPublish'],
				'count' => count($this->get_images(null, $album['idAlbum'])),
			]);
		}

        return $rs;
	}

    public function delete_albums($idAlbum){
		try {
			$this->db->delete('albums', array('idAlbum' => $idAlbum));
			return json_encode([
                'msg' => 'El album fue eliminado',
                'state' => 1
            ]);
		} catch (Exception $e) {
			return json_encode([
                'msg' => 'Hubo un error al eliminar el album. (Intentelo mas tarde)',
                'state' => 0
            ]);
		}
	}

    public function create_albums($data) {
		try {
			$this->db->insert('albums', $data);
			return json_encode([
				'msg' => 'El album se registro con exito',
				'state' => 1
			]);
		} catch (Exception $e) {
			return json_encode([
				'msg' => 'Hubo un error al registrar el album. (Intentelo mas tarde)',
				'state' => 0
			]);
		}
	}

    public function edit_albums($data, $idAlbum) {
		try {
			$this->db->where('idAlbum', $idAlbum);
			$this->db->update('albums', $data);
			return json_encode([
				'msg' => 'El album se modifico con exito',
				'state' => 1
			]);
		} catch (Exception $e) {
			return json_encode([
				'msg' => 'Hubo un error al editar el album. (Intentelo mas tarde)',
				'state' => 0
			]);
		}
	}
    //FIN ALBUMS

	//IMAGENES
	public function get_images($idGallery = null, $idAlbum = null) {
		if ($idGallery != null) {
            $this->db->where('idGallery', $idGallery);
        }
		if ($idAlbum != null) {
            $this->db->where('album', $idAlbum);
        }
        $query = $this->db->get('galleries');
        return $query->result_array();
	}

	public function create_image($data) {
		try {
			$this->db->insert('galleries', $data);
			return json_encode([
				'msg' => 'Imagen Agregada',
				'state' => 1
			]);
		} catch (Exception $e) {
			return json_encode([
				'msg' => 'Error al subir la imagen. (Intentelo mas tarde)',
				'state' => 0
			]);
		}
	}

	public function delete_image($idGallery){
		try {
			$this->db->delete('galleries', array('idGallery' => $idGallery));
			return json_encode([
                'msg' => 'La imagen fue eliminada',
                'state' => 1
            ]);
		} catch (Exception $e) {
			return json_encode([
                'msg' => 'Error al subir la imagen. (Intentelo mas tarde)',
                'state' => 0
            ]);
		}
	}

}
