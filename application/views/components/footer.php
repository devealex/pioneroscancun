
    </div>
    <section class="sponsors">
        <div class="container has-text-centered">

            <strong class="title">NUESTROS PATROCINADORES</strong>
            <?php
            if($sponsors <> null) {
                $indices = array('column' => 0, 'item' => 0);
                foreach($sponsors as $columns) { 
                    
                    if($indices['column'] == 0) {
                        echo '<div class="columns is-centered is-variable is-8">';
                        echo '<div class="column"><a href="'.$columns['sponsorSite'].'" target="_blank"><img src="/assets/img/sponsors/'.$columns['logotype'].'"></a></div>';

                        $indices['column'] = 1;
                    }
                    else {
                        echo '<div class="column"><a href="'.$columns['sponsorSite'].'" target="_blank"><img src="/assets/img/sponsors/'.$columns['logotype'].'"></a></div>';
                        if($indices['item'] == 6) {
                            echo '</div>';
                            $indices['column'] = 0;
                            $indices['item'] = -1;
                        }
                    }

                    
                    $indices['item']++;
                    
                }    
            }
            ?>
            
        </div>
    </section>
    <footer>
        <div class="container">
            <div class="columns is-centered is-variable is-5">

                <div class="column is-one-fifth logotipo"><a href="/"><img src="/assets/img/pioneros-cancun-fc.png" class="brand"></a></div>
                <div class="column newsletter">
                    <strong class="title">RECIBE NOTICIAS DE PIONEROS</strong>
                    <span class="subtitle">¡Suscríbete y participa en la comunidad Pioneros de Cancún!</span>
                    <form id="frm-suscription">
                        <input type="text" name="email">
                        <input type="submit" value="SUSCRIBIRME">
                    </form>
                    <div class="clr"></div>
                </div>
                <div class="column is-one-fifth social">
                    <a href="https://www.facebook.com/PionerosFC/" target="_blank" title="Síguenos en Facebook" class="social"><i class="fab fa-facebook-f"></i></a>
					<a href="https://twitter.com/PionerosFC" target="_blank" title="Síguenos en Twitter" class="social"><i class="fab fa-twitter"></i></a>
					<a href="https://www.instagram.com/pionerosfc/" target="_blank" title="Síguenos en Instagram" class="social"><i class="fab fa-instagram"></i></a>
					<a href="https://www.youtube.com/user/clubpionerosdecancun" target="_blank" title="Síguenos en Youtube" class="social"><i class="fab fa-youtube"></i></a>
					<a href="https://tunein.com/radio/Radio-Cultural-Ayuntamiento-1059-s91380/?lang=es" target="_blank" title="Síntonizanos a través de Radio" class="social"><i class="fas fa-broadcast-tower"></i></a>
                </div>

            </div>
        </div>

        <div class="container">
            <div class="columns links">

                <div class="column">
                    <strong>Noticias</strong>
                    <ul>
                        <?php foreach($page['categories'] as $liNewsCategory) { ?>
                        <li><a href="/<?php echo $liNewsCategory['categorySlug']; ?>"><i class="fas fa-chevron-right"></i> <?php echo $liNewsCategory['category']; ?></a></li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="column">
                    <strong>El Club</strong>
                    <ul>
                        <li><a href="/historia"><i class="fas fa-chevron-right"></i> Historia</a></li>
                        <li><a href="/palmares"><i class="fas fa-chevron-right"></i> Campeonatos</a></li>
                        <li><a href="/directiva"><i class="fas fa-chevron-right"></i> Directiva</a></li>
                        <li><a href="/nuestros-valores"><i class="fas fa-chevron-right"></i> Valores</a></li>
                    </ul>
                </div>
                <div class="column">
                    <strong>Plantel</strong>
                    <ul>
                        <?php foreach($page['planteles'] as $liPlantels) { ?>
                        <li><a href="/<?php echo $liPlantels['plantelSlug']; ?>"><i class="fas fa-chevron-right"></i> <?php echo $liPlantels['plantelName']; ?></a></li>
                        <?php } ?>
                    </ul>
                </div>
                <div class="column">
                    <strong>Estadísticas</strong>
                    <ul>
                        <li><a href="#"><i class="fas fa-chevron-right"></i> Calendario</a></li>
                        <li><a href="#"><i class="fas fa-chevron-right"></i> Tabla de Posiciones</a></li>
                    </ul>
                </div>
                <div class="column">
                    <strong>Escuelas</strong>
                    <ul>
                        <li><a href="/escuelas"><i class="fas fa-chevron-right"></i> Nuestras escuelas</a></li>
                        <li><a href="/escuelas-registro"><i class="fas fa-chevron-right"></i> Queremos ser parte</a></li>
                    </ul>
                </div>
                <div class="column">
                    <strong>Multimedia</strong>
                    <ul>
                        <li><a href="#"><i class="fas fa-chevron-right"></i> Videos</a></li>
                        <li><a href="#"><i class="fas fa-chevron-right"></i> Fotogalerías</a></li>
                    </ul>
                </div>
                <div class="column">
                    <ul class="black">
                        <li><a href="/aviso-privacidad">Aviso de Privacidad</a></li>
                        <!--<li><a href="#">Preguntas Frecuentes</a></li>-->
                        <li><a href="/contacto">Contacto</a></li>
                    </ul>
                </div>

            </div>
        </div>
    </footer>

    <!-- Global site tag (gtag.js) - Google Analytics -->
    <script async src="https://www.googletagmanager.com/gtag/js?id=UA-178742427-2"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());

    gtag('config', 'UA-178742427-2');
    </script>


    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>

</body>
</html>
