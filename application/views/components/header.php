<?php
defined('BASEPATH') or exit('No direct script access allowed');
?>
<!DOCTYPE html>
<html lang="en">

<head>
	<meta charset="utf-8">
	<title>Pioneros Cancún FC | Sitio Web Oficial <?php echo $page['title_anexo']; ?></title>

	<meta name="viewport" content="width=device-width, initial-scale=1.0">

	<link rel="shortcut icon" type="image/x-icon" href="/assets/img/favicon.ico">
	<link rel="icon" type="image/x-icon" href="/assets/img/favicon.ico">

	<link rel="stylesheet" href="/assets/css/bulma.min.css">
	<link rel="stylesheet" href="/assets/css/pioneros.min.css">
	<link rel="stylesheet" href="/assets/css/components.css">

	<link rel="stylesheet" href="/assets/manager/libs/quill/dist/quill.snow.css">

	<script type="text/javascript" src="/assets/js/jquery-3.4.1.min.js"></script>
	<script type="text/javascript" src="/assets/js/pioneros.js"></script>
	<script type="text/javascript" src="/assets/js/components.js"></script>

	<?php
	$src_image = 'pioneros-cancun-fc-banner.jpg';
	if (isset($page['content']['newsAvatar']) && trim($page['content']['newsAvatar']) != '') {
		$src_image = 'news/'.trim($page['content']['newsAvatar']);
	}
	?>
	<meta name="twitter:card" content="summary_large_image">
	<meta name="twitter:site" content="@">
	<meta name="twitter:creator" content="@">
	<meta name="twitter:title" content="Pioneros Cancún FC | Sitio Web Oficial <?= $page['title_anexo']; ?>">
	<meta name="twitter:description" content="">
	<meta name="twitter:image" content="<?= 'https://' . $_SERVER['HTTP_HOST'] . '/assets/img/' . $src_image; ?>">

	<meta property="og:title" content="Pioneros Cancún FC | Sitio Web Oficial <?= $page['title_anexo']; ?>" />
	<meta property="og:url" content="<?= 'https://' . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'] ?>" />
	<meta property="og:type" content="website" />
	<meta property="og:description" content="" />
	<meta property="og:image" content="<?= 'https://' . $_SERVER['HTTP_HOST'] . '/assets/img/' . $src_image; ?>" />
	<meta property="og:rating" content="5" />

</head>

<body>

	<header class="is-full-header">
		<div class="columns is-clearfix">

			<div class="column is-one-third-tablet is-one-quarter-desktop is-one-fifth-widescreen is-box-menu">
				<i class="fas fa-bars"></i>
				<span>MENU</span>
			</div>

			<div class="column is-box-brand"><a href="/"><img src="/assets/img/pioneros-cancun-fc.png"></a></div>

			<div class="column is-one-third-tablet is-one-quarter-desktop is-one-fifth-widescreen is-box-social">
				<a href="https://www.facebook.com/PionerosFC/" target="_blank" title="Síguenos en Facebook" class="social"><i class="fab fa-facebook-f"></i></a>
				<a href="https://twitter.com/PionerosFC" target="_blank" title="Síguenos en Twitter" class="social"><i class="fab fa-twitter"></i></a>
				<a href="https://www.instagram.com/pionerosfc/" target="_blank" title="Síguenos en Instagram" class="social"><i class="fab fa-instagram"></i></a>
				<a href="https://www.youtube.com/user/clubpionerosdecancun" target="_blank" title="Síguenos en Youtube" class="social"><i class="fab fa-youtube"></i></a>
				<a href="https://tunein.com/radio/Radio-Cultural-Ayuntamiento-1059-s91380/?lang=es" target="_blank" title="Síntonizanos a través de Radio" class="social"><i class="fas fa-broadcast-tower"></i></a>
			</div>

		</div>
	</header>

	<div class="is-full-menu">
		<span id="is-close-menu"><i class="far fa-times-circle"></i></span>
		<div class="container">
			<div class="columns is-multiline">

				<div class="column is-one-quarter">
					<a href="/" class="is-simply"><img src="/assets/img/pioneros-cancun-fc.png"> INICIO</a>

					<div class="is-group">
						<span><img src="/assets/img/pioneros-cancun-fc.png"> EL CLUB</span>
						<a href="/historia">Historia</a>
						<a href="/palmares">Campeonatos</a>
						<a href="/directiva">Directiva</a>
						<a href="/nuestros-valores">Valores</a>
					</div>

					<div class="is-group">
						<span><img src="/assets/img/pioneros-cancun-fc.png"> ESTADIOS</span>
						<a href="/cancun-86">Estadio 86</a>
						<a href="/andres-quintanaroo">Andrés Quintana Roo</a>
					</div>

					<a href="/contacto" class="is-simply"><img src="/assets/img/pioneros-cancun-fc.png"> CONTACTO</a>
				</div>

				<div class="column is-one-quarter">
					<div class="is-group">
						<span><img src="/assets/img/pioneros-cancun-fc.png"> NOTICIAS</span>
						<?php if ($page['categories'] != null) {
							foreach ($page['categories'] as $category) { ?>
								<a href="/<?= $category['categorySlug']; ?>"><?= $category['category']; ?></a>
						<?php }
						} ?>
					</div>

					<div class="is-group">
						<span><img src="/assets/img/pioneros-cancun-fc.png"> MULTIMEDIA</span>
						<a href="/videos">Videos</a>
						<a href="/galerias">Fotogalerías</a>
					</div>
				</div>

				<div class="column is-one-quarter">
					<div class="is-group">
						<span><img src="/assets/img/pioneros-cancun-fc.png"> PLANTELES</span>
						<?php if ($page['planteles'] != null) {
							foreach ($page['planteles'] as $plantel) { ?>
								<a href="/<?= $plantel['plantelSlug']; ?>"><?= $plantel['plantelName']; ?></a>
						<?php }
						} ?>
					</div>

					<div class="is-group">
						<span><img src="/assets/img/pioneros-cancun-fc.png"> ESCUELAS</span>
						<a href="/escuelas">Nuestras Escuelas</a>
						<a href="/escuelas-registro">Queremos ser parte</a>
					</div>
				</div>

				<div class="column is-one-quarter">
					<div class="is-group">
						<span><img src="/assets/img/pioneros-cancun-fc.png"> ESTADÍSTICAS</span>
						<a href="<?= (isset($torneos) && $torneos != null) ? '/calendario?torneo=' . $torneos[0]['torneourl'] : ''; ?>">Calendario</a>
						<a href="<?= (isset($torneos) && $torneos != null) ? '/estadisticas?torneo=' . $torneos[0]['torneourl'] : ''; ?>">Estadísticas</a>
					</div>
				</div>

				<div class="column is-full">
					<div class="is-social">
						<a href="https://www.facebook.com/PionerosFC/" target="_blank" title="Síguenos en Facebook" class="social"><i class="fab fa-facebook-f"></i></a>
						<a href="https://twitter.com/PionerosFC" target="_blank" title="Síguenos en Twitter" class="social"><i class="fab fa-twitter"></i></a>
						<a href="https://www.instagram.com/pionerosfc/" target="_blank" title="Síguenos en Instagram" class="social"><i class="fab fa-instagram"></i></a>
						<a href="https://www.youtube.com/user/clubpionerosdecancun" target="_blank" title="Síguenos en Youtube" class="social"><i class="fab fa-youtube"></i></a>
						<a href="https://tunein.com/radio/Radio-Cultural-Ayuntamiento-1059-s91380/?lang=es" target="_blank" title="Síntonizanos a través de Radio" class="social"><i class="fas fa-broadcast-tower"></i></a>
					</div>
				</div>

			</div>
		</div>
	</div>

	<div class="contenido">