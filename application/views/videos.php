<section class="videos">
    <input type="hidden" name="video_parm" id="video_parm" value="<?= (isset($_GET['video']) ? $_GET['video'] . '-button' : '') ?>">
    <br><br>
    <div class="container">
        <div class="columns is-variable is-2 is-multiline">
            <?php
            if ($page['videos'] != null) {
                $i = 1;
                foreach ($page['videos'] as $video) {
                    $size = 'is-one-quarter';
                    if ($i == 1 || $i == 5 || $i == 9) {
                        $size = 'is-half';
                        if ($i == 9) {
                            $i = 0;
                        }
                    }
                    $i++;
                    $linkTitleVideo = cleanText($video['videoTitle']); //Crear Link para abrir el video
            ?>
                    <!-- Video -->
                    <div class="column <?= $size ?>">
                        <div class="art-video" style="background: url('/assets/img/videos/<?= trim($video['videoAvatar']) ?>');">
                            <a href="" class="modal-button <?php echo $linkTitleVideo . '-button'; ?>" data-target="<?php echo $linkTitleVideo; ?>" aria-haspopup="true">
                                <div class="mask"><i class="far fa-play-circle"></i></div>
                                <div class="target">
                                    <h1><?= $video['videoTitle'] ?></h1>
                                </div>
                            </a>
                        </div>
                    </div>
                    <!-- MODAL -->
                    <div class="modal" id="<?php echo $linkTitleVideo; ?>">
                        <div class="modal-background"></div>
                        <div class="modal-card">
                            <header class="modal-card-head">
                                <p class="modal-card-title"><?= $video['videoTitle'] ?></p>
                                <button class="delete" aria-label="close"></button>
                            </header>
                            <section class="modal-card-body">
                                <div class="content-video">
                                    <?= $video['videoIframe'] ?>
                                </div>
                            </section>
                        </div>
                    </div>
            <?php
                }
            } else { echo '<div class="column is-full"><h1 style="text-align:center;">No hay videos disponibles</h1></div>'; }
            ?>
        </div>
    </div>
</section>