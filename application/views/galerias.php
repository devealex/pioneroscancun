<section class="galerias">
    <br><br>
    <div class="container">
        <div class="columns is-variable is-1 is-multiline">
            <?php
            if ($page['albums'] != null) {
                $i = 1;
                foreach ($page['albums'] as $album) {
                    $size = 'is-one-quarter';
                    if ($i == 1 || $i == 5 || $i == 9) {
                        $size = 'is-half';
                        if ($i == 9) {
                            $i = 0;
                        }
                    }
                    $i++;
                    $linkAlbumName = cleanText(trim($album['albumName']));
            ?>
                    <div class="column <?= $size ?>">
                        <div class="artc-album" style="background: url('/assets/img/galerias/albums/<?= trim($album['albumAvatar']) ?>');">
                            <a href="/assets/img/galerias/albums/<?= trim($album['albumAvatar']) ?>" data-fancybox="<?php echo $linkAlbumName; ?>" data-caption="<?= $album['albumName'] ?>" class="fancyslideshow">
                                <div class="mask"></div>
                                <div class="target">
                                    <h1><?= trim($album['albumName']) ?></h1>
                                    <h3><?php $date = date_create($album['albumRegister']);
                                        echo date_format($date, 'd/m/Y'); ?></h3>
                                </div>
                            </a>
                        </div>
                    </div>
                    <div class="gallery-images">
                        <?php
                        foreach ($page['images'] as $image) {
                            if ($image['album'] == $album['idAlbum']) {
                        ?>
                                <a href="/assets/img/galerias/images/<?= trim($image['galleryImage']) ?>" data-fancybox="<?php echo $linkAlbumName; ?>" data-caption="<?= $album['albumName'] ?>" class="fancyslideshow"></a>
                        <?php
                            }
                        }
                        ?>
                    </div>
            <?php
                }
            } else {
                echo '<div class="column is-full"><h1 style="text-align:center;">No hay Imagenes disponibles</h1></div>';
            }
            ?>
        </div>
    </div>
</section>

<script type="text/javascript">
    $(document).ready(function() {

        $(".fancyslideshow").fancybox({
            transitionEffect: 'zoom-in-out'
        });

    });
</script>