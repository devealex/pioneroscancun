<section class="escuelas">
    <div class="columns is-gapless familia">

        <div class="column is-one-third efecto">
            <h1 class="title is-size-4 is-title-red">SÉ UN PIONERO</h1>
            <ul>
                <li><i class="fas fa-check"></i> Torneos oficiales.</li>
                <li><i class="fas fa-check"></i> Visorías del primer equipo.</li>
                <li><i class="fas fa-check"></i> Labores sociales.</li>
                <li><i class="fas fa-check"></i> Crecimiento y seguimiento.</li>
            </ul>
            <img src="/assets/img/registro-escuelas-pioneros.jpg">
            <div class="mask"></div>
        </div>
        <div class="column">
            <div class="registro">
            <h2 class="is-title-red is-size-5">REGISTRA TU ESCUELA</h2>
            <Para>Forma parte de esta gran familia Pionera, te invitamos a enviarnos tu propuesta para sumar tu escuela a  los Centro de Formación.<br>
            Para más información mándanos tus dudas y preguntas al correo direccionpionerosfc@hotmail.com</p>

            <form id="registro-escuela" action="" method="post">

                <div class="columns">
                    <div class="column">
                        <div class="field">
                            <div class="control has-icons-left">
                                <input class="input is-medium" name="nameSchool" type="text" placeholder="Escuela">
                                <span class="icon is-small is-left">
                                <i class="fas fa-futbol fa-xs"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="field">
                            <div class="control has-icons-left">
                                <input class="input is-medium" name="representative" type="text" placeholder="Representante">
                                <span class="icon is-small is-left">
                                <i class="fas fa-user-tie fa-xs"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="columns">
                    <div class="column">
                        <div class="field">
                            <div class="control has-icons-left">
                                <input class="input is-medium" name="contactNumber" type="number" placeholder="Número Telefónico">
                                <span class="icon is-small is-left">
                                <i class="fas fa-phone fa-xs"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                    <div class="column">
                        <div class="field">
                            <div class="control has-icons-left">
                                <input class="input is-medium" name="email" type="email" placeholder="Correo Electrónico">
                                <span class="icon is-small is-left">
                                <i class="fas fa-envelope fa-xs"></i>
                                </span>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="field">
                    <div class="control">
                        <textarea class="textarea is-medium" name="address" placeholder="Dirección" rows="2"></textarea>
                    </div>
                </div>

                <div class="field">
                    <button type="submit" name="btn-school" class="button is-medium is-link">
                        <span class="icon is-small">
                            <i class="fas fa-clipboard-check"></i>
                        </span>
                        <span>Registrar Escuela</span>
                    </button>
                </div>

            </form>
            </div>

        </div>
    </div>



</section>
