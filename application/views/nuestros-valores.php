<section class="valores-pioneros">
    
    <img src="/assets/img/pioneros-cancun-valores.jpg">
    <div class="mask"></div>
    
    <div class="text">
        <div class="container has-text-centered">
            <h1 class="title is-size-4">NUESTROS VALORES</h1>
            <p>El Club Pioneros de Cancún durante toda su historia ha fomentado los valores dentro y fuera del terreno de juego, debido a que creemos rotundamente que la practica constante de los valores en la vida diaria y el deporte nos llevarán a formar mejor seres humanos reconstruyendo así el tejido social.</p>
            <p>Además al ser un club social, nuestro compromiso y responsabilidad es permear las buenas acciones dentro de esta gran familia pionera que es conformada por niñas, niños, jóvenes, entrenadores, personal de mantenimiento, personal administrativo, jugadores profesionales, patrocinadores y directivos.</p>
        </div>
    </div>

</section>

<section class="valores-pioneros container">
    <div class="columns has-text-centered items">

        <div class="column">
            <img src="/assets/img/pioneros-cancun-disciplina.jpg">
            <h3>Disciplina</h3>
            <p>Reconocemos que la disciplina y el máximo esfuerzo son indispensables para el logro de nuestras metas.</p>
        </div>
        <div class="column">    
            <img src="/assets/img/pioneros-cancun-respeto.jpg">
            <h3>Respeto</h3>
            <p>Aplicamos la tolerancia, con el trato digno para todos nuestros compañeros, jugadores, entrenadores y cada uno de los que conforman Pioneros de Cancún.</p>
        </div>
        <div class="column">
            <img src="/assets/img/pioneros-cancun-integridad.jpg">
            <h3>Integridad</h3>
            <p>Restamos los principios del ser humano dentro y fuera del terreno de juego.</p>
        </div>
        <div class="column">
            <img src="/assets/img/pioneros-cancun-transparencia.jpg">
            <h3>Transparencia</h3>
            <p>Promovemos la honestidad, transparencia ante cada acto a realizar dentro y fuera del terreno de juego.</p>
        </div>
        <div class="column">
            <img src="/assets/img/pioneros-cancun-trabajo-en-equipo.jpg">
            <h3>Trabajo en Equipo</h3>
            <p>Fomentar la solidaridad, unidad y el trabajo en equipo dentro y fuera de la cancha.</p>
        </div>

    </div>
</section>
