<section class="container is-teams">
    <div class="columns is-multiline is-marginless">
        <?php 
        if($page['planteles'] != null) { 
            foreach($page['planteles'] as $plantel) {
        ?>
        <div class="column is-full is-team-card">
            <img src="/assets/img/planteles/<?=$plantel['plantelImage'];?>">
            <div class="is-info">
                <h1><?=$plantel['plantelName'];?></h1>
                <a href="<?=$plantel['plantelSlug'];?>">Conocer la plantilla de jugadores</a>
            </div>
        </div>
        <?php 
            }
        } ?>
    </div>
</section>
