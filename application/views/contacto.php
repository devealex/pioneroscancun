<section class="contacto">

    <div class="informacion">
        <div class="container">

            <div class="columns is-marginless is-multiline">
                <div class="column is-one-quarter">
                    <h3 class="is-title-red">DIRECCIÓN GENERAL</h3>
                    <strong>José Casellas Peón</strong>
                    <span><strong><small>Tel.</small></strong> 888 2976</span>
                    <span><strong><small>Email.</small></strong> asocpionersfc@gmail.com</span>
                </div>

                <div class="column is-one-quarter">
                    <h3 class="is-title-red">DIRECCIÓN ADMINISTRATIVA</h3>
                    <strong>Gabriela Martínez Collí</strong>
                    <span><strong><small>Tel.</small></strong> 888 2976</span>
                    <span><strong><small>Email.</small></strong> facturaspionerosfc@hotmail.com</span>
                </div>

                <div class="column is-one-quarter">
                    <h3 class="is-title-red">DIRECCIÓN OPERATIVA</h3>
                    <strong>Martha Robles del Valle</strong>
                    <span><strong><small>Tel.</small></strong> 888 2976</span>
                    <span><strong><small>Email.</small></strong> clubpioneroscancun@hotmail.com</span>
                </div>

                <div class="column is-one-quarter">
                    <h3 class="is-title-red">MEDIOS</h3>
                    <strong>Efraín Grajeda</strong>
                    <span><strong><small>Tel.</small></strong> 888 2976</span>
                    <span><strong><small>Email.</small></strong> medios@pioneroscancunfc.com</span>
                </div>

                <div class="column is-one-quarter">
                    <h3 class="is-title-red">CENTROS DE FORMACION</h3>
                    <strong>Enrique Braga Tuz</strong>
                    <span><strong><small>Tel.</small></strong> 888 2976</span>
                    <span><strong><small>Email.</small></strong> cefors@pioneroscancunfc.com</span>
                </div>

                <div class="column is-one-quarter">
                    <h3 class="is-title-red">PATROCINADORES</h3>
                    <!--<strong>Efraín Grajeda</strong>-->
                    <span><strong><small>Tel.</small></strong> 888 2976</span>
                    <span><strong><small>Email.</small></strong> comercial@pioneroscancunfc.com</span>
                </div>

                <!--<div class="column">
                    <a href="https://www.facebook.com/PionerosFC/" target="_blank" title="Síguenos en Facebook" class="social"><i class="fab fa-facebook-f"></i></a>
                    <a href="https://twitter.com/PionerosFC" target="_blank" title="Síguenos en Twitter" class="social"><i class="fab fa-twitter"></i></a>
                    <a href="https://www.instagram.com/pionerosfc/" target="_blank" title="Síguenos en Instagram" class="social"><i class="fab fa-instagram"></i></a>
                    <a href="https://www.youtube.com/user/clubpionerosdecancun" target="_blank" title="Síguenos en Youtube" class="social"><i class="fab fa-youtube"></i></a>
                    <a href="https://tunein.com/radio/Radio-Cultural-Ayuntamiento-1059-s91380/?lang=es" target="_blank" title="Síntonizanos a través de Radio" class="social"><i class="fas fa-broadcast-tower"></i></a>
                </div>-->
            </div>

        </div>
    </div>

    <div class="container is-form-contact">
        <div class="columns is-margin-bottom-f">

            <div class="is-two-thirds column">
                <h1 class="is-title-red title is-size-4">CONTACTO</h1>
                <p class="subtitle">Si tienes algún tema de contacto, duda, sugerencia relacionado con el Club por favor contactános por cualquiera de los medios proporcionados en todo nuestro website, y un representate del club se comunicará contigo tan pronto como sea posible.</p>
            
                <form id="contacto-form" action="" method="post">
                    <div class="columns">
                        <div class="column">
                            <div class="field">
                                <div class="control has-icons-left">
                                    <input class="input is-medium" type="text" name="contactName" placeholder="Nombre">
                                    <span class="icon is-small is-left">
                                    <i class="fas fa-user-tie fa-xs"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="column">
                            <div class="field">
                                <div class="control has-icons-left">
                                    <input class="input is-medium" type="number" name="contactNumber" placeholder="Número Telefónico">
                                    <span class="icon is-small is-left">
                                    <i class="fas fa-phone fa-xs"></i>
                                    </span>
                                </div>
                            </div>
                        </div>                   
                    </div>

                    <div class="columns">
                        <div class="column">
                            <div class="field">
                                <div class="control has-icons-left">
                                    <input class="input is-medium" type="email" name="email" placeholder="Correo Electrónico">
                                    <span class="icon is-small is-left">
                                    <i class="fas fa-envelope fa-xs"></i>
                                    </span>
                                </div>
                            </div>
                        </div>
                        <div class="column">
                            <div class="field">
                                <div class="control has-icons-left" style="width:100%;">
                                    <div class="select is-medium" style="width: 100%;">
                                    <select style="width:100%;" name="contactSubject">
                                        <option selected>Dirección</option>
                                        <option>Equipos</option>
                                        <option>Escuelas</option>
                                        <option>Administración</option>
                                        <option>Otros</option>
                                    </select>
                                    </div>
                                    <div class="icon is-small is-left">
                                    <i class="fas fa-list"></i>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="field">
                        <div class="control">
                            <textarea class="textarea is-medium" name="address" placeholder="Dirección" rows="2"></textarea>
                        </div>
                    </div>

                    <div class="field">
                        <button type="submit" class="button is-medium is-link">
                            <span class="icon is-small">
                                <i class="fas fa-paper-plane"></i>
                            </span>
                            <span>Contactar</span>
                        </button>
                    </div>

                </form>

            </div>

            <div class="column">
                <div class="general">
                    <h2 class="is-title-blue">GENERALES</h2>
                    <p>Prolongación Av. Tulum S/N Manzana 89,  No Disponible,  Tequila
                    <br>Quintana Roo,  C.P. 77500</p>

                    <p><strong>Telefonos</strong><br>
                    888 2976<br>

                    <p><strong>Email</strong><br>
                    <small>info@pioneroscancunfc.com.mx</small><br>
                    <small>prensa@pioneroscancunfc.com.mx</small><br>
                    <small>medios@pioneroscancunfc.com.mx</small><br>
                    <small>cefors@pioneroscancunfc.com.mx</small><br>
                    <small>comercial@pioneroscancunfc.com.mx</small></p>
                </div>
            </div>

        </div>
    </div>

        
       

    

</section>

