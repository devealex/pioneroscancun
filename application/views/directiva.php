<section class="container directiva is-first-margin">
    <h1 class="title is-title-red is-size-4 is-centered">DIRECTIVOS</h1>
    <div class="columns is-marginless directivos">
        <div class="column">
            <div class="columns">
                <div class="column is-one-third"><img src="/assets/img/directiva/GUILLERMO-PAEZ.png"></div>
                <div class="column">
                    <strong>VICEPRESIDENTE</strong>
                    <h3 class="is-size-5 is-title-red">Arq. Guillermo Páez González</h3>
                    <p>Dirige, autoriza y coordina las operaciones, logística de las actividades deportivas y administrativas de la Asociación.</p>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="columns">
                <div class="column is-one-third"><img src="/assets/img/directiva/JOSE-CASELLAS.png"></div>
                <div class="column">
                    <strong>DIRECTOR GENERAL</strong>
                    <h3 class="is-size-5 is-title-red">José Casellas Peón</h3>
                    <p>Administra y ejecuta las propuestas, políticas, acuerdos, planes y programas aprobados por el Consejo Directivo de la Asociación.</p> 
                </div>
            </div>
        </div>
    </div>
    <div class="columns is-marginless directivos">
        <div class="column">
            <div class="columns">
                <div class="column is-one-third"><img src="/assets/img/directiva/JOSE-DANIEL-MOGUEL.png"></div>
                <div class="column">
                    <strong>DIRECTOR DEPORTIVO</strong>
                    <h3 class="is-size-5 is-title-red">José Daniel Moguel Hernández</h3>
                    <p>Dirige, controla, administra los recursos y personal deportivo.</p>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="columns">
                <div class="column is-one-third"><img src="/assets/img/directiva/GABRIELA-MARTINEZ.png"></div>
                <div class="column">
                    <strong>DIRECTORA ADMINISTRATIVA</strong>
                    <h3 class="is-size-5 is-title-red">Gabriela Atalia Martínez Colli</h3>
                    <p>Administra los recursos Financieros, Humanos y Materiales de la Asociación.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="columns is-marginless directivos">
        <div class="column">
            <div class="columns">
                <div class="column is-one-third"><img src="/assets/img/directiva/GILDARDO-VAZQUEZ-MONCADA.png"></div>
                <div class="column">
                    <strong>DIRECTOR JURÍDICO</strong>
                    <h3 class="is-size-5 is-title-red">Gildardo Vázquez Moncada</h3>
                    <p>Administra y dirige las acciones legales de la Asociación. </p>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="columns">
                <div class="column is-one-third"><img src="/assets/img/directiva/MARTHA-ROBLES-DEL-VALLE.png"></div>
                <div class="column">
                    <strong>DIRECTORA OPERATIVA</strong>
                    <h3 class="is-size-5 is-title-red">Martha Robles del Valle</h3>
                    <p>Supervisa y planea la logistica deportiva de las ligas profesionales y semi profesionales de la Asociación.</p>
                </div>
            </div>
        </div>
    </div>
    <div class="columns is-marginless directivos">
        <div class="column">
            <div class="columns">
                <div class="column is-one-third"><img src="/assets/img/directiva/ENRIQUE-BRAGA.png"></div>
                <div class="column">
                    <strong>DIRECTOR CENTROS DE FORMACIÓN</strong>
                    <h3 class="is-size-5 is-title-red">Enrique Braga Tuz</h3>
                    <p>Supervisa la jornada laboral de los instructores e implementa la metodología de trabajo en fuerzas básicas, gestiona el registro de jugadores en todas las categorías.</p>
                </div>
            </div>
        </div>
        <div class="column">
            <div class="columns">
                <div class="column is-one-third"><img src="/assets/img/directiva/EFRAIN-GRAJEDA.png"></div>
                <div class="column">
                    <strong>JEFE DE PRENSA</strong>
                    <h3 class="is-size-5 is-title-red">Efraín Grajeda</h3>
                    <p>Coordina la información en los medios de comunicación y los espacios oficiales de comunicación de los diferentes equipos profesionales, semi-profesionales y fuerzas básicas de la Asociación.</p>
                </div>
            </div>
        </div>
    </div>
</section>
