<section class="container is-first-margin">
    <div class="columns is-marginless">
        <div class="column is-full is-bread">
            <h1 class="title is-title-red is-size-4">CALENDARIO</h1>

            <?php
            if (isset($torneos) && $torneos != null) {
                $limit = count($torneos) - 1;
                $get = '';
                if (isset($_GET['torneo']) && $_GET['torneo'] != null) {
                    $get = trim($_GET['torneo']);
                }
                foreach ($torneos as $i => $torneo) {
                    $item = '<a href="/calendario?torneo=' . trim($torneo['torneourl']) . '">' . trim($torneo['torneo']) . '</a>';
                    if (trim($torneo['torneourl']) == $get) {
                        $item = '<span>' . trim($torneo['torneo']) . '</span>';
                    }
                    if ($i != $limit) {
                        $item = $item . '<span>|</span>';
                    }
                    echo $item;
                }
            }
            ?>
        </div>
    </div>

    <div class="columns is-multiline is-calendar">

        <?php
        if (isset($calendario) && $calendario != null) {
            foreach ($calendario as $i => $cldr) {
                //$date = date_format(date_create($cldr['playDay']), 'd/m/Y');
                $date = ['local' => '&nbsp;', 'visita' => '&nbsp;'];
                if ($cldr['playDay'] <= date('Y-m-d')) {
                    $date['local'] = $cldr['localGol'];
                    $date['visita'] = $cldr['visitaGol'];
                }
        ?>
                <div class="column is-one-quarter">
                    <div class="is-card">
                        <div class="columns is-multiline">
                            <div class="column is-full"><strong class="is-rol">Jornada <?= $cldr['jornadaRol'] ?></strong></div>
                            <div class="column is-one-third">
                                <img src="/assets/img/equipos/<?= $cldr['locLogo'] ?>">
                                <strong><?= $date['local'] ?></strong>
                            </div>
                            <div class="column is-one-third"><small>vs</small></div>
                            <div class="column is-one-third">
                                <img src="/assets/img/equipos/<?= $cldr['visLogo'] ?>">
                                <strong><?= $date['visita'] ?></strong>
                            </div>
                            <div class="column is-full">
                                <span><?= humanDateFormat($cldr['playDay']) ?></br><?= strftime("%I:%M %p", strtotime($cldr['playTime'])); ?></span>
                            </div>
                        </div>
                    </div>
                </div>
        <?php
            }
        }
        ?>

    </div>

</section>