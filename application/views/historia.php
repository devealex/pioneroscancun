<section class="historia is-clearfix">
    <div class="img">
        <div class="flexslider">
          <ul class="slides">
            <li>
              <img src="/assets/img/historia/historia-pioneros-1.jpg" />
            </li>
            <li>
              <img src="/assets/img/historia/historia-pioneros-2.jpg" />
            </li>
            <li>
              <img src="/assets/img/historia/historia-pioneros-3.jpg" />
            </li>
            <li>
              <img src="/assets/img/historia/historia-pioneros-4.jpg" />
            </li>
          </ul>
        </div>

        <script type="text/javascript">
        $('.flexslider').flexslider({
            animation: "slide"
        });
        </script>
        <!-- <img src="/assets/img/pioneros-cancun-historia.jpg"> -->
    </div>
    <div class="text">
        <h1 class="title is-title-red is-size-4">NUESTRA HISTORIA</h1>
        <p>El 3 de Mayo de 1984, Cancún vio nacer al equipo que lo acompañaría durante toda su historia, debido a que en este año el Club Pioneros de Cancún comenzó su historia, teniendo como arranque la Tercera División Profesional del Fútbol Mexicano, para ese momento el club cancunenense comenzaría a jugar en el Estadio Municipal ‘Andrés Quintana Roo’.</p>
        <p>El primer torneo lo disputaron el 28 de Julio de 1984, en un partido que finalizó con empate a un gol. Posteriormente el equipo se mudaría al Estadio ‘Cancún 86’, ubicado sobre la prolongación Tulum, fue ahí donde se escribió gran parte de la historia de Pioneros de Cancún.</p>
        <p>Los años pasaron y Pioneros consiguió ser uno de los equipos protagonistas de la categoría, fue así como se consiguió el primer ascenso del club caribeño, esto el 28 de Mayo del 2014 cuando disputaron la final de ascenso ante Cañeros de Zacatepec en una final que terminó con marcador global de 2-1 a favor de Pioneros de Cancún, consiguiendo así el ascenso a la Liga Premier.</p>
        <p>Actualmente y luego de 34 años de historia, Pioneros disputa la Liga Premier en la Segunda División la cual es la antesala de la categoría de plata del futbol mexicano, ahora con una nueva imagen y colores los cuales se remontan a los inicios de este histórico club cancunense. </p>
    </div>
</section>

<section class="container">

    <div class="columns is-marginless">
        <div class="column">
            <h2 class="is-size-5 is-title-red">AÑOS QUE NOS MARCARON</h2>
        </div>
    </div>

    <div class="columns is-marginless">

        <div class="column">
            <div class="linea-destaca">
                <div class="destacado">
                    <span class="bubble"></span>
                    <strong class="title is-title-blue is-size-5">2019 <i class="fas fa-angle-double-right"></i> <small>35 años del Club</small></strong>
                    <p>Partido Conmemorativo a los 35 años del Club en el que participaron más de 130 jugadores de diferentes épocas de Pioneros.</p>
                    <p>8 de Mayo del 2019</p>
                </div>
                <div class="destacado">
                    <span class="bubble"></span>
                    <strong class="title is-title-blue is-size-5">2014 <i class="fas fa-angle-double-right"></i> <small>Ascenso de Pioneros de Cancún</small></strong>
                    <p>Ascenso de Pioneros de Cancún de la Liga Nuevos Talentos a la Liga Premier.</p>
                    <p>24 de mayo de 2014.</p>
                </div>
                <div class="destacado">
                    <span class="bubble"></span>
                    <strong class="title is-title-blue is-size-5">1986 <i class="fas fa-angle-double-right"></i> <small>El Primer Partido</small></strong>
                    <p>El primer partido en el Estadio Cancún 86 el 27 de diciembre de 1986, Pioneros de Cancún vs Coras FC.</p>
                </div>
                <div class="destacado">
                    <span class="bubble"></span>
                    <strong class="title is-title-blue is-size-5">1986 <i class="fas fa-angle-double-right"></i> <small>El Primer gol</small></strong>
                    <p>El Primer gol en la historia del Cancún 86 27 de diciembre de 1986 anotado por Fernando Cruz Pichardo.</p>
                </div>
                <div class="destacado">
                    <span class="bubble"></span>
                    <strong class="title is-title-blue is-size-5">1984 <i class="fas fa-angle-double-right"></i> <small>Torneo Profesional</small></strong>
                    <p>El primer partido de Pioneros en un torneo Profesional se jugó el 28 de julio de 1984.</p>
                </div>
                <div class="destacado">
                    <span class="bubble"></span>
                    <strong class="title is-title-blue is-size-5">1984 <i class="fas fa-angle-double-right"></i> <small>Fundación del Club</small></strong>
                    <p>Fundación del Club</p>
                    <p>3 de mayo de 1984</p>
                </div>
            </div>
        </div>

    </div>
</section>
