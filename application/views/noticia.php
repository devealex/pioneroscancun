<section class="detalle-noticia">
    <div class="container">
        <div class="bg-new" style="background:url('/assets/img/news/<?php if($page['content']['newsAvatar'] == '') {echo 'default.jpg';} else {echo trim($page['content']['newsAvatar']);} ?>');">
            <h1><?= $page['content']['newsTitle']; ?></h1>
        </div>
        <div class="data-new">
            <div class="is-breadcrumb">
                <a href="/">Pioneros Cancún</a>
                <i class="fas fa-arrow-right"></i>
                <a href="/noticias">Noticias</a>
                <i class="fas fa-arrow-right"></i>
                <a href="/<?= $page['content']['categorySlug']; ?>"><?= $page['content']['category']; ?></a>
                <i class="fas fa-arrow-right"></i>
                <span><?= $page['content']['newsTitle']; ?></span>
            </div>
            <small>Publicado el <?= humanDateFormat($page['content']['newsPublished']); ?></small>
        </div>
        
        <div class="columns is-gapless">
            <div class="content-new column is-three-quarters">
                <div class="ql-editor">
                    <?= $page['content']['newsContent']; ?>
                </div>
            </div>
            <div class="column">
                <!-- VIDEO -->
                <div class="is-video">
                <?php 
                if($interesting['video'] != null) {
                    echo $interesting['video'][0]['videoIframe'];
                }
                ?>
                </div>
                
                <!-- GALLERY -->
                <div class="is-album">
                <?php if($interesting['gallery'] != null) { $link = ($interesting['gallery'][0]['count'] > 0) ? cleanText($interesting['gallery'][0]['albumName']).'-1' : cleanText($interesting['gallery'][0]['albumName']); ?>
                <a href="/galerias#<?=$link;?>" target="_blank"><img src="/assets/img/galerias/albums/<?= $interesting['gallery'][0]['albumAvatar']; ?>"></a>
                <a href="/galerias#<?=$link;?>" target="_blank"><i class="far fa-images"></i> <?= $interesting['gallery'][0]['albumName']; ?></a>
                <?php } ?>
                </div>

            </div>
        </div>
    </div>
</section>
