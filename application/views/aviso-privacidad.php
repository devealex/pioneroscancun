<section class="container">
    <div class="columns">

        <div class="column is-full">

            <br><br><br><br>
            <h1 class="is-text-centered">ASOCIACIÓN DE FUTBOL PIONEROS A.C.</h1>

            <h2 class="is-text-centered">AVISO DE PRIVACIDAD SIMPLIFICADO PARA INGRESO A LOS CENTROS<br>DE FORMACIÓN (CEFOR)</h2>

            <p>En cumplimiento a la Ley General de Protección de Datos Personales en Posesión de Sujetos Obligados y de la Ley de Protección de Datos Personales en Posesión de Sujetos Obligados para el Estado de Quintana Roo.</p>
            
            <h3 class="is-text-centered">AVISO DE PRIVACIDAD</h3>

            <p>La Asociación de Fútbol Pioneros, A.C. Informa que es el responsable del tratamiento de Datos Personales sensibles que nos proporcionan, serán utilizados para registrar y dar seguimiento al proceso de formación e integración de postulantes a la Asociación y serán protegidos de conformidad a lo dispuesto por la Ley General de Protección de Datos Personales en Posesión de Sujetos Obligados y la Ley de Protección de Datos Personales en Posesión de Sujetos Obligados para el Estado de Quintana Roo y demás normatividad que resulte aplicable.</p>
            
            <p>Los datos personales recabados en el presente formato serán utilizados con la finalidad de realizar el trámite de la afiliación para registrar y dar seguimiento al proceso de formación e integración de postulantes a la Asociación de Futbol Pioneros, A.C. así como emisión y envió de constancias de participación en caso de existir, elaboración de formatos gráficos con fines estadísticos, así como contar con los datos de los participantes, asumiendo la obligación de cumplir con las medidas legales y de seguridad suficientes para proteger los Datos Personales que se hayan recabado.</p>
            
            <p class="is-text-centered"><strong>NO DESEO QUE MIS DATOS PERSONALES SEAN UTILIZADOS PARA FINES DISTINTOS A LOS QUE FUERON ESTABLECIDOS.</strong></p>

            <p class="is-text-centered"><strong>TRANSFERENCIA DE DATOS PERSONALES</strong></p>

            <p>Se realizarán transferencias de datos personales como: edad, sexo, y tramite prestado, a otras dependencias con fines de cumplimiento de las obligaciones de transferencia las que serán con la Unidad de Vinculación de Transparencia y Acceso a la Información Pública del Municipio de Benito Juárez, asimismo a la Federación Mexicana de Futbol para que se realícenlas cedulas correspondientes.</p>

            <p class="is-text-centered"><strong>CONSULTAR</strong></p>

            <p>Se le comunica que usted podrá consultar el aviso de privacidad integral en el sitio web: <a href="http://cancun.gob.mx/transparencia/privacidad/" target="_blank">http://cancun.gob.mx/transparencia/privacidad/</a> , o bien de manera personal en la carpeta de avisos de privacidad de Asociación de Futbol Pioneros A.C. del Municipio de Benito Juárez.</p>
            
            <p><strong>Nota: el presente formato es indispensable para llevar un registro de participantes.</strong></p>
            <br><br>

        </div>

    </div>
</section>
