<section class="plantel">
    <div class="group">
        <img src="/assets/img/planteles/<?= $page['content']['plantelImage'] ?>">
        <div class="mask"></div>

        <div class="text">
            <div class="container has-text-centered">
                <div class="columns">
                </div>
            </div>
        </div>

        <div class="patrocinio">
            <div class="columns">

                <?php
                if ($page['sponsors'] != null) {
                    foreach($page['sponsors'] as $sponsor) {
                        ?>
                        <div class="column"><a href="<?= $sponsor['sponsorSite'] ?>" target="_blank"><img src="/assets/img/sponsors/<?= $sponsor['logotype'] ?>"></a></div>
                        <?php
                    }
                }
                ?> 
            </div>
        </div>
    </div>


    <div class="container posicion">
    <?php 
    if($page['players'] != null) { 
        
        //PORTEROS
        if($page['players']['p'] != null) {   
        ?>
        <div class="columns is-multiline is-variable is-8 is-position">
            <h3 class="column is-full">PORTEROS</h3>
            <?php foreach($page['players']['p'] as $player) { ?>
            <div class="column is-full-mobile is-half-tablet is-one-quarter-desktop is-one-quarter-widescreen">
                <div class="jugador">
                    <?php $playerImg = json_decode($player['img']); ?>
                    <img src="/assets/img/players/<?= $playerImg->imgFrente; ?>" class="frente">
                    <img src="/assets/img/players/<?= $playerImg->imgDetras; ?>" class="numero">
                    <strong class="nombre"><?= $player['name'] ?></strong>
                    <span class="playera"><?= $player['number'] ?></span>
                </div>
            </div>
            <?php } ?>
        </div>
        <?php
        }

        //DEFENZAS
        if($page['players']['d'] != null) {   
        ?>
        <div class="columns is-multiline is-variable is-8 is-position">
            <h3 class="column is-full">DEFENZAS</h3>
            <?php foreach($page['players']['d'] as $player) { ?>
            <div class="column is-full-mobile is-half-tablet is-one-quarter-desktop is-one-quarter-widescreen">
                <div class="jugador">
                    <?php $playerImg = json_decode($player['img']); ?>
                    <img src="/assets/img/players/<?= $playerImg->imgFrente; ?>" class="frente">
                    <img src="/assets/img/players/<?= $playerImg->imgDetras; ?>" class="numero">
                    <strong class="nombre"><?= $player['name'] ?></strong>
                    <span class="playera"><?= $player['number'] ?></span>                    
                </div>
            </div>
            <?php } ?>
        </div>
        <?php
        }

        //MEDIOS
        if($page['players']['m'] != null) {   
        ?>
        <div class="columns is-multiline is-variable is-8 is-position">
            <h3 class="column is-full">MEDIOS</h3>
            <?php foreach($page['players']['m'] as $player) { ?>
            <div class="column is-full-mobile is-half-tablet is-one-quarter-desktop is-one-quarter-widescreen">
                <div class="jugador">
                    <?php $playerImg = json_decode($player['img']); ?>
                    <img src="/assets/img/players/<?= $playerImg->imgFrente; ?>" class="frente">
                    <img src="/assets/img/players/<?= $playerImg->imgDetras; ?>" class="numero">
                    <strong class="nombre"><?= $player['name'] ?></strong>
                    <span class="playera"><?= $player['number'] ?></span>
                </div>
            </div>
            <?php } ?>
        </div>
        <?php
        }

        //DELANTEROS
        if($page['players']['g'] != null) {   
        ?>
        <div class="columns is-multiline is-variable is-8 is-position">
            <h3 class="column is-full">DELANTEROS</h3>
            <?php foreach($page['players']['g'] as $player) { ?>
            <div class="column is-full-mobile is-half-tablet is-one-quarter-desktop is-one-quarter-widescreen">
                <div class="jugador">
                    <?php $playerImg = json_decode($player['img']); ?>
                    <img src="/assets/img/players/<?= $playerImg->imgFrente; ?>" class="frente">
                    <img src="/assets/img/players/<?= $playerImg->imgDetras; ?>" class="numero">
                    <strong class="nombre"><?= $player['name'] ?></strong>
                    <span class="playera"><?= $player['number'] ?></span>
                </div>
            </div>
            <?php } ?>
        </div>
        <?php
        }

        //CUERPO TECNICO
        if($page['players']['t'] != null) {   
        ?>
        <div class="columns is-multiline is-variable is-8 is-position">
            <h3 class="column is-full">CUERPO TÉCNICO</h3>
            <?php
            foreach($page['players']['t'] as $player) { ?>
            <div class="column is-full-mobile is-half-tablet is-one-quarter-desktop is-one-quarter-widescreen">
                <div class="jugador">
                    <?php $playerImg = json_decode($player['img']); ?>
                    <img src="/assets/img/players/<?= $playerImg->imgFrente; ?>" class="frente">
                    <img src="/assets/img/players/<?= $playerImg->imgDetras; ?>" class="numero">
                    <strong class="nombre"><?= $player['name'] ?></strong>
                    <small><?= $player['function'];?></small>
                </div>
            </div>
            <?php } ?>
        </div>
        <?php
        }
        
    } ?>
    </div>

</section>
