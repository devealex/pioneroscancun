<div class="container is-fluid articulos-noticias">
	<?php
	if ($page['content'] != null){
		$i = 1;
		foreach ($page['content'] as $new) {
			$categoryNew = $new['category'];
			$categoryNew = str_replace('á', 'a', $categoryNew);
			$categoryNew = str_replace('é', 'e', $categoryNew);
			$categoryNew = str_replace('í', 'i', $categoryNew);
			$categoryNew = str_replace('ó', 'o', $categoryNew);
			$categoryNew = str_replace('ú', 'u', $categoryNew);
			if ($i == 1) {
				?>
				<section class="columns is-gapless list-news">
					<article class="article column is-half" style="background:url('/assets/img/news/<?php if($new['newsAvatar'] == '') {echo 'default.jpg';} else {echo trim($new['newsAvatar']);} ?>');">
						<a href="<?= '/'.$new['newsUrl']; ?>" class="mask">
							<h2><?= strtoupper($new['newsTitle']); ?></h3>
								<h3><?= humanDateFormat($new['newsPublished']); ?></h3>
								<h4 class="categoryNew"><?php if ($page['section'] == 'Noticias'){echo strtoupper($categoryNew);} ?></h4>
							</a>
						</article>
				<?php
			} else if ($i == 3) {
				?>
					<article class="article column" style="background:url('/assets/img/news/<?php if($new['newsAvatar'] == '') {echo 'default.jpg';} else {echo trim($new['newsAvatar']);} ?>');">
						<a href="<?= '/'.$new['newsUrl']; ?>" class="mask">
							<h2><?= strtoupper($new['newsTitle']); ?></h3>
							<h3><?= humanDateFormat($new['newsPublished']); ?></h3>
							<h4 class="categoryNew"><?php if ($page['section'] == 'Noticias'){echo strtoupper($categoryNew);} ?></h4>
						</a>
					</article>
				</section>
				<section class="columns is-gapless list-news">
				<?php
			} else if ($i == 6) {
				?>
					<article class="article column is-half" style="background:url('/assets/img/news/<?php if($new['newsAvatar'] == '') {echo 'default.jpg';} else {echo trim($new['newsAvatar']);} ?>');">
						<a href="<?= '/'.$new['newsUrl']; ?>" class="mask">
							<h2><?= strtoupper($new['newsTitle']); ?></h3>
							<h3><?= humanDateFormat($new['newsPublished']); ?></h3>
							<h4 class="categoryNew"><?php if ($page['section'] == 'Noticias'){echo strtoupper($categoryNew);} ?></h4>
						</a>
				  	</article>
				</section>
				<?php
				$i = 0;
			} else if ($i == count($page['content'])) {
				?>
					<article class="article column" style="background:url('/assets/img/news/<?php if($new['newsAvatar'] == '') {echo 'default.jpg';} else {echo trim($new['newsAvatar']);} ?>');">
						<a href="<?= '/'.$new['newsUrl']; ?>" class="mask">
							<h2><?= strtoupper($new['newsTitle']); ?></h3>
							<h3><?= humanDateFormat($new['newsPublished']); ?></h3>
							<h4 class="categoryNew"><?php if ($page['section'] == 'Noticias'){echo strtoupper($categoryNew);} ?></h4>
						</a>
					</article>
				</section>
				<?php
			} else {
				?>
					<article class="article column" style="background:url('/assets/img/news/<?php if($new['newsAvatar'] == '') {echo 'default.jpg';} else {echo trim($new['newsAvatar']);} ?>');">
						<a href="<?= '/'.$new['newsUrl']; ?>" class="mask">
							<h2><?= strtoupper($new['newsTitle']); ?></h3>
							<h3><?= humanDateFormat($new['newsPublished']); ?></h3>
							<h4 class="categoryNew"><?php if ($page['section'] == 'Noticias'){echo strtoupper($categoryNew);} ?></h4>
						</a>
					</article>
				<?php
			}
			$i++;
		}
	} else {
		?>
		<h1 style="text-align:center;">No hay noticias disponibles</h1>
		<?php
	}
	?>
</div>
