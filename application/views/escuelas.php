<section class="escuelas">
    <div class="columns is-gapless">

        <div class="column is half">
            <div class="introduccion">
                <h1 class="title is-title-red is-size-4">NUESTRAS ESCUELAS</h1>
                <p>Actualmente los Pioneros de Cancún cuenta con un gran semillero conformado por más de mil niñas, niños y jóvenes que son parte de las Escuelitas de Pioneros, estos Centros de Formación se distinguen por ser gratuitos para todos los niños. </p>
                <p>Para este 2020 Pioneros de Cancún cuenta con once Centros de Formación ubicados en diferentes puntos de la ciudad los cuales son:</p>
            </div>
        </div>
        <div class="column is-half">
            <div class="flexslider">
              <ul class="slides">
                <li>
                  <img src="/assets/img/escuelas-pioneros.jpg" />
                </li>
                <li>
                  <img src="/assets/img/escuelas-pioneros-1.jpg" />
                </li>
                <li>
                  <img src="/assets/img/escuelas-pioneros-2.jpg" />
                </li>
                <li>
                  <img src="/assets/img/escuelas-pioneros-3.jpg" />
                </li>
              </ul>
            </div>

            <script type="text/javascript">
            $('.flexslider').flexslider({
                animation: "slide"
            });
            </script>
            <!-- <img src="/assets/img/escuelas-pioneros.jpg"> -->
        </div>

    </div>

    <div class="container lista">
        <div class="columns titulo-boton">
            <div class="column is-full">
                <h2 class="title is-title-red is-size-5">ESCUELAS CERTIFICADAS</h2>
                <a href="/escuelas-registro" class="is-flat-blue is-abs-right">Queremos ser Pioneros <i class="fas fa-long-arrow-alt-right"></i></a>
            </div>
        </div>

    <?php
        if ($page['content'] != null) {
            $i = 1;
    		foreach ($page['content'] as $school) {
    			if ($i == 1) {
                    ?>
                    <div class="columns is-variable is-5 grupo">
                        <div class="column certifica">
                            <strong class="title is-title-blue is-size-5"><?= $school['nameSchool']; ?></strong>
                            <h4 class="is-size-6"><?= $school['representative']; ?></h4>

                            <div class="contacto is-clearfix">
                                <i class="fas fa-directions"></i>
                                <span><?= $school['address']; ?></span>
                            </div>
                            <div class="contacto is-clearfix">
                                <i class="fas fa-mobile-alt"></i>
                                <span><?= $school['contactNumber']; ?></span>
                            </div>
                            <div class="contacto is-clearfix">
                                <i class="fas fa-at"></i>
                                <span><?= $school['email']; ?></span>
                            </div>
                            <div class="social">
                                <?php
                                if ($school['facebook'] != "") {
                                    ?>
                                    <a href="<?= $school['facebook']; ?>" target="_blank"><i class="fab fa-facebook-f"></i></a>
                                    <?php
                                }
                                if ($school['twitter'] != "") {
                                    ?>
                                    <a href="<?= $school['twitter']; ?>" target="_blank"><i class="fab fa-twitter"></i></a>
                                    <?php
                                }
                                if ($school['instagram'] != "") {
                                    ?>
                                    <a href="<?= $school['instagram']; ?>" target="_blank"><i class="fab fa-instagram"></i></a>
                                    <?php
                                }
                                if ($school['youtube'] != "") {
                                    ?>
                                    <a href="<?= $school['youtube']; ?>" target="_blank"><i class="fab fa-youtube"></i></a>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    <?php
    			} else if ($i == 4 || $i == count($page['content'])) {
                    ?>
                        <div class="column certifica">
                            <strong class="title is-title-blue is-size-5"><?= $school['nameSchool']; ?></strong>
                            <h4 class="is-size-6"><?= $school['representative']; ?></h4>

                            <div class="contacto is-clearfix">
                                <i class="fas fa-directions"></i>
                                <span><?= $school['address']; ?></span>
                            </div>
                            <div class="contacto is-clearfix">
                                <i class="fas fa-mobile-alt"></i>
                                <span><?= $school['contactNumber']; ?></span>
                            </div>
                            <div class="contacto is-clearfix">
                                <i class="fas fa-at"></i>
                                <span><?= $school['email']; ?></span>
                            </div>
                            <div class="social">
                                <?php
                                if ($school['facebook'] != "") {
                                    ?>
                                    <a href="<?= $school['facebook']; ?>" target="_blank"><i class="fab fa-facebook-f"></i></a>
                                    <?php
                                }
                                if ($school['twitter'] != "") {
                                    ?>
                                    <a href="<?= $school['twitter']; ?>" target="_blank"><i class="fab fa-twitter"></i></a>
                                    <?php
                                }
                                if ($school['instagram'] != "") {
                                    ?>
                                    <a href="<?= $school['instagram']; ?>" target="_blank"><i class="fab fa-instagram"></i></a>
                                    <?php
                                }
                                if ($school['youtube'] != "") {
                                    ?>
                                    <a href="<?= $school['youtube']; ?>" target="_blank"><i class="fab fa-youtube"></i></a>
                                    <?php
                                }
                                ?>
                            </div>
                        </div>
                    </div>
                    <?php
                    $i = 0;
    			} else {
                    ?>
                    <div class="column certifica">
                        <strong class="title is-title-blue is-size-5"><?= $school['nameSchool']; ?></strong>
                        <h4 class="is-size-6"><?= $school['representative']; ?></h4>

                        <div class="contacto is-clearfix">
                            <i class="fas fa-directions"></i>
                            <span><?= $school['address']; ?></span>
                        </div>
                        <div class="contacto is-clearfix">
                            <i class="fas fa-mobile-alt"></i>
                            <span><?= $school['contactNumber']; ?></span>
                        </div>
                        <div class="contacto is-clearfix">
                            <i class="fas fa-at"></i>
                            <span><?= $school['email']; ?></span>
                        </div>
                        <div class="social">
                            <?php
                            if ($school['facebook'] != "") {
                                ?>
                                <a href="<?= $school['facebook']; ?>" target="_blank"><i class="fab fa-facebook-f"></i></a>
                                <?php
                            }
                            if ($school['twitter'] != "") {
                                ?>
                                <a href="<?= $school['twitter']; ?>" target="_blank"><i class="fab fa-twitter"></i></a>
                                <?php
                            }
                            if ($school['instagram'] != "") {
                                ?>
                                <a href="<?= $school['instagram']; ?>" target="_blank"><i class="fab fa-instagram"></i></a>
                                <?php
                            }
                            if ($school['youtube'] != "") {
                                ?>
                                <a href="<?= $school['youtube']; ?>" target="_blank"><i class="fab fa-youtube"></i></a>
                                <?php
                            }
                            ?>
                        </div>
                    </div>
                    <?php
    			}
    			$i++;
    		}
        } else {
            ?>
            <h1 style="text-align:center;">No hay escuelas registradas</h1>
            <?php
        }
    ?>
    </div>

    <div class="container">
        <!--<div class="columns publicidad">
            <div class="column is-full">
                <img src="/assets/img/publicidad/fullwidth.jpg">
            </div>
        </div>-->

        <div class="columns"><div class="column is-full"><h3 class="title is-title-red is-size-5">NOTICIAS</h3></div></div>

        <div class="noticias columns" style="text-align:center;">
        <?php 
            //print_r($page['news']);
            if (count($page['news']) == 0) {
                ?><h1 style="text-align:center;">No hay noticias</h1><?php
            } else {
                $i = 1;
                foreach($page['news'] as $new) {
                    if ($i <= 4) {
                        //Title
                        $titleNew = $new['newsTitle'];
                        $titleNew = str_replace('á', 'a', $titleNew);
                        $titleNew = str_replace('é', 'e', $titleNew);
                        $titleNew = str_replace('í', 'i', $titleNew);
                        $titleNew = str_replace('ó', 'o', $titleNew);
                        $titleNew = str_replace('ú', 'u', $titleNew);

                        //Date
                        $date = date_create($new['newsPublished']);
                        $date = date_format($date, 'd/m/Y');
                    ?>
                    <div class="column is-one-quarter" style="background:url('/assets/img/news/<?php if($new['newsAvatar'] == '') {echo 'default.jpg';} else {echo trim($new['newsAvatar']);} ?>');">
                        <a href="<?= '/'.$new['newsUrl']; ?>">
                            <div class="mask"></div>
                            <div class="text">
                                <h4><?= strtoupper($titleNew); ?></h4>
                                <span><?= $date; ?></span>
                            </div>
                        </a>
                    </div>
                    <?php
                    }
                    $i++;
                }
            }
        ?>
        </div>
    </div>

</section>
