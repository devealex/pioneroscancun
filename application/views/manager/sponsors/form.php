<div class="row">
    <div class="col-md-12">
        <div class="card">

            <form class="form-horizontal" id="form-sponsor">
                <div class="card-body">
                    <div class="form-group row col-sm-12 float-left">
                        <label for="sponsorOrder" class="col-sm-12 text-left control-label col-form-label">Orden de Listado</label>
                        <div class="col-sm-12">
                            <input type="number" min="1" max="100" class="form-control" id="sponsorOrder" name="sponsorOrder" placeholder="Orden de Listado" value="<?php if(isset($sponsor_item)){echo $sponsor_item['sponsorOrder'];} ?>">
                        </div>
                    </div>
                    <div class="form-group row col-sm-12 float-left">
                        <label for="sponsorSite" class="col-sm-12 text-left control-label col-form-label">Sitio del Patrocinador</label>
                        <div class="col-sm-12">
                            <input type="text" data-isvalid="false" class="form-control" id="sponsorSite" name="sponsorSite" placeholder="Sitio del Patrocinador" value="<?php if(isset($sponsor_item)){echo $sponsor_item['sponsorSite'];} ?>">
                        </div>
                    </div>
                    <div class="form-group row col-sm-12">
                        <label class="col-sm-12 text-left control-label col-form-label">Imagen del Patrocinador (540 x 322)</label>
                        <div class="col-sm-12 div-preview form-control">
                            <input type="file" class="form-control file-img-preview" name="logotype" id="logotype" accept="image/*">
                            <img src="/assets/img/sponsors/<?php if(isset($sponsor_item) && $sponsor_item['logotype'] != ''){echo $sponsor_item['logotype'];}else{echo "default.jpg";} ?>" class="img-preview">
                        </div>
                    </div>
                    <input type="hidden" name="action" value="<?= $action ?>">
                    <input type="hidden" name="nameLogotype" value="<?php if (isset($sponsor_item)) {echo $sponsor_item['logotype'];} ?>">
                    <input type="hidden" name="idSponsor" value="<?php if(isset($sponsor_item)){echo $sponsor_item['idSponsor'];} ?>">
                </div>
                <div class="clear"></div>
                <div class="border-top">
                    <div class="card-body">
                        <button type="submit" class="btn btn-success">Guardar Patrocinador</button>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
