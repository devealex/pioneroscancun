<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="sponsors-table" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Logotipo</th>
                                <th>Orden</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach($sponsors as $sponsor) {
                                ?>
                                <tr>
                                    <td class="td-center"><img src="/assets/img/sponsors/<?= $sponsor['logotype'] ?>" alt="Logotype"></td>
                                    <td><?= $sponsor['sponsorOrder'] ?></td>
                                    <td class="td-center">
                                        <button type="button" class="btn btn-danger btn-sm del-sponsor" data-logotype="<?= $sponsor['logotype'] ?>" data-idsponsor="<?= $sponsor['idSponsor']; ?>"><i class="m-r-10 mdi mdi-delete"></i>Eliminar</button>
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Logotipo</th>
                                <th>Orden</th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#sponsors-table').DataTable();
    });
</script>
