<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="galerias-table" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Estado</th>
                                <th>Nombre</th>
                                <th>Fecha de Registro</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($albums as $album) {
                                ?>
                                <tr>
                                    <td class="td-center">
                                        <input type="checkbox" name="albumPublish" id="albumPublish-<?= $album['idAlbum'] ?>" data-idalbum="<?= $album['idAlbum']; ?>" class="chxbx-table" value="<?= $album['albumPublish']; ?>" <?php if($album['albumPublish'] == 1){echo 'checked';} ?>>
                                        <label for="albumPublish-<?= $album['idAlbum']; ?>" class="lbl-to-chxbx-table"></label>
                                    </td>
                                    <td><?= $album['albumName'] ?></td>
                                    <td><?php $date = date_create($album['albumRegister']); echo date_format($date, 'Y-m-d'); ?></td>
                                    <td>
                                        <a href="/manager/galerias/edit/<?= $album['idAlbum'] ?>"><button type="button" class="btn btn-primary btn-sm"><i class="m-r-10 mdi mdi-table-edit"></i>Editar</button></a>
                                        <a href="/manager/imagenes/<?= $album['idAlbum'] ?>"><button type="button" class="btn btn-success btn-sm" ><i class="m-r-10 mdi mdi-folder-multiple-image"></i>Imagenes</button></a>
                                        <button type="button" class="btn btn-danger btn-sm del-album" data-idalbum="<?= $album['idAlbum']; ?>" data-albumavatar="<?= $album['albumAvatar'] ?>" ><i class="m-r-10 mdi mdi-delete"></i>Eliminar</button>
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Estado</th>
                                <th>Nombre</th>
                                <th>Fecha de Registro</th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#galerias-table').DataTable();
    });
</script>
