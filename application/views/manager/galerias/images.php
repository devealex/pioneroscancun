<div class="row">
    <div class="col-md-12">
        <div class="card">
            <form class="form-horizontal" action="/manager/imagenes/crud/create" method="post" id="form-imagen">
                <div class="card-body">
                    <div class="form-group row col-sm-12">
                        <label class="col-sm-12 text-left control-label col-form-label">Image (1280 x 720)</label>
                        <input type="file" class="form-control" id="archivo[]" name="archivo[]" multiple accept="image/*">
                    </div>
                    <input type="hidden" name="idAlbum" value="<?php if(isset($idAlbum)){echo $idAlbum;} ?>">
                </div>
                <div class="border-top">
                    <div class="card-body">
                        <button type="submit" class="btn btn-success">Guardar Imagen</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>

<div class="row el-element-overlay">
    <?php
    foreach ($images as $image) {
        ?>
        <div class="col-lg-3 col-md-6">
            <div class="card">
                <div class="el-card-item">
                    <div class="el-card-avatar el-overlay-1"> <img src="/assets/img/galerias/images/<?= $image['galleryImage'] ?>" alt="user" />
                        <div class="el-overlay">
                            <ul class="list-style-none el-info">
                                <li class="el-item"><a class="btn default btn-outline image-popup-vertical-fit el-link" href="/assets/img/galerias/images/<?= $image['galleryImage'] ?>"><i class="mdi mdi-magnify-plus"></i></a></li>
                                <li class="el-item del-img" data-idimage="<?= $image['idGallery'] ?>" data-nameimage="<?= trim($image['galleryImage']) ?>" data-galleryimage="<?= trim($image['galleryImage']) ?>"><a class="btn default btn-outline el-link" href="javascript:void(0);"><i class="mdi mdi-delete"></i></a></li>
                            </ul>
                        </div>
                    </div>
                    <div class="el-card-content">
                        <h4 class="m-b-0"><?php $date = date_create($image['galleryRegister']); echo date_format($date, 'd-m-Y'); ?></h4>
                    </div>
                </div>
            </div>
        </div>
        <?php
    }
    ?>
</div>
