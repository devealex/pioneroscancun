<div class="row">
    <div class="col-md-12">
        <div class="card">

            <form class="form-horizontal" id="form-albums">
                <div class="card-body">
                    <div class="form-group row col-sm-12 float-left">
                        <label for="albumName" class="col-sm-12 text-left control-label col-form-label">Nombre del Galería</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="albumName" name="albumName" placeholder="Nombre del Álbum" value="<?php if(isset($album_item)){echo $album_item['albumName'];} ?>">
                        </div>
                    </div>
                    <div class="form-group row col-sm-12">
                        <label class="col-sm-12 text-left control-label col-form-label">Imagen del Galería (1280 x 720)</label>
                        <div class="col-sm-12 div-preview form-control">
                            <input type="file" class="form-control file-img-preview" name="albumAvatar" id="albumAvatar" accept="image/*">
                            <img src="/assets/img/galerias/albums/<?php if(isset($album_item) && $album_item['albumAvatar'] != ''){echo $album_item['albumAvatar'];}else{echo "default.jpg";} ?>" class="img-preview">
                        </div>
                    </div>
                    <input type="hidden" name="action" value="<?= $action ?>">
                    <input type="hidden" name="nameAvatar" value="<?php if (isset($album_item)) {echo $album_item['albumAvatar'];} ?>">
                    <input type="hidden" name="idAlbum" value="<?php if(isset($album_item)){echo $album_item['idAlbum'];} ?>">
                </div>
                <div class="clear"></div>
                <div class="border-top">
                    <div class="card-body">
                        <button type="submit" class="btn btn-success">Guardar Album</button>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
