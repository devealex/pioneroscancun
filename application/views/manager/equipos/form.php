<div class="row">
    <div class="col-md-12">
        <div class="card">

            <form class="form-horizontal" id="form-equipo">
                <div class="card-body">
                    <div class="form-group row col-sm-12 float-left">
                        <label for="equipoName" class="col-sm-12 text-left control-label col-form-label">Nombre del Equipo</label>
                        <div class="col-sm-12">
                            <input type="text" data-isvalid="false" class="form-control" id="equipoName" name="equipoName" placeholder="Nombre del Equipo" value="<?php if(isset($equipo_item)){echo $equipo_item['equipoName'];} ?>">
                        </div>
                    </div>
                    <div class="form-group row col-sm-12">
                        <label class="col-sm-12 text-left control-label col-form-label">Imagen del Equipo (350 x 350)</label>
                        <div class="col-sm-12 div-preview form-control">
                            <input type="file" class="form-control file-img-preview" name="equipoLogo" id="equipoLogo" accept="image/png">
                            <img src="/assets/img/equipos/<?php if(isset($equipo_item) && $equipo_item['equipoLogo'] != ''){echo $equipo_item['equipoLogo'];}else{echo "default.jpg";} ?>" class="img-preview">
                        </div>
                    </div>
                    <input type="hidden" name="action" value="<?= $action ?>">
                    <input type="hidden" name="nameLogotype" value="<?php if (isset($equipo_item)) {echo $equipo_item['equipoLogo'];} ?>">
                    <input type="hidden" name="idEquipo" value="<?php if(isset($equipo_item)){echo $equipo_item['idEquipo'];} ?>">
                </div>
                <div class="clear"></div>
                <div class="border-top">
                    <div class="card-body">
                        <button type="submit" class="btn btn-success">Guardar Equipo</button>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
