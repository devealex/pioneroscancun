<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="equipos-table" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Nombre del Equipo</th>
                                <th>Logotipo</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($equipos as $equipo) {
                                ?>
                                <tr>
                                    <td><?= $equipo['equipoName'] ?></td>
                                    <td class="td-center">
                                        <img src="/assets/img/equipos/<?= trim($equipo['equipoLogo']); ?>" alt="">
                                    </td>
                                    <td>
                                        <a href="/manager/equipos/edit/<?= $equipo['idEquipo'] ?>"><button type="button" class="btn btn-primary btn-sm"><i class="m-r-10 mdi mdi-table-edit"></i>Editar</button></a>
                                        <button type="button" class="btn btn-danger btn-sm del-equipo" data-equipologo="<?= trim($equipo['equipoLogo']); ?>" data-idequipo="<?= $equipo['idEquipo']; ?>"><i class="m-r-10 mdi mdi-delete"></i>Eliminar</button>
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Nombre del Equipo</th>
                                <th>Logotipo</th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#equipos-table').DataTable();
    });
</script>
