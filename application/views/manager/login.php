<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?><!DOCTYPE html>
<html lang="en" style="height:100%;">
<head>
	<meta charset="utf-8">
	<title>Pioneros Cancún FC | Sitio Web Oficial | Acceso Administración</title>

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="shortcut icon" type="image/x-icon" href="/assets/img/favicon.ico">
	<link rel="icon" type="image/x-icon" href="/assets/img/favicon.ico">

	<link rel="stylesheet" href="/assets/css/bulma.min.css">
    <link rel="stylesheet" href="/assets/css/pioneros.min.css">
    <link rel="stylesheet" href="/assets/manager/css/manager.min.css">

	<script type="text/javascript" src="/assets/js/jquery-3.4.1.min.js"></script>
	<script type="text/javascript" src="/assets/js/pioneros.js"></script>
	<script type="text/javascript" src="/assets/js/components.js"></script>
    <!-- All Jquery -->
    <script src="/assets/manager/libs/jquery/dist/jquery.min.js"></script>
    <!-- Manager JS -->
    <script src="/assets/manager/js/components.js"></script>

</head>
<body class="login">

    <div class="container">
        <div class="columns">

            <div class="column is-4 is-offset-4">
                <form name="frmLogin" id="frmLogin" action="" method="post">
                    <img src="/assets/img/pioneros-cancun-fc.png">

                    <div class="control has-icons-left">
                        <input class="input is-medium" type="email" name="email" placeholder="Correo Electronico">
                        <span class="icon is-left">
                            <i class="fas fa-user-circle"></i>
                        </span>
                    </div>

                    <div class="control has-icons-left">
                        <input class="input is-medium" type="password" name="pass" placeholder="Contraseña">
                        <span class="icon is-left">
                            <i class="fas fa-lock"></i>
                        </span>
                    </div>

                    <div class="control">
                        <button class="button is-info is-medium">INGRESAR</button>
                    </div>
                </form>
            </div>
        </div>
    </div>

    <script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js"></script>

</body>
</html>
