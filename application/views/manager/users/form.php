<div class="row">
    <div class="col-md-12">
        <div class="card">
            <form class="form-horizontal" id="form-user">
                <div class="card-body">
                    <div class="form-group row col-md-6 col-sm-12 float-left">
                        <label for="userName" class="col-sm-12 text-left control-label col-form-label">Nombre de Usuario</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="userName" name="userName" placeholder="Nombre de Usuario" value="<?php if(isset($user_item)){echo $user_item['userName'];} ?>">
                        </div>
                    </div>
                    <div class="form-group row col-md-6 col-sm-12 float-left">
                        <label for="userRol" class="col-sm-12 text-left control-label col-form-label">Rol del Usuario</label>
                        <div class="col-sm-12">
                            <select class="form-control custom-select" name="userRol">
                                <option value="1" <?php if(isset($user_item) && $user_item['userRol']==1){echo 'selected';} ?>>Administrador</option>
                                <option value="2" <?php if(isset($user_item) && $user_item['userRol']==2){echo 'selected';} ?>>Editor Multimedia</option>
                                <option value="3" <?php if(isset($user_item) && $user_item['userRol']==3){echo 'selected';} ?>>Escuelas</option>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row col-sm-12 col-md-6 float-left">
                        <label for="userMail" class="col-sm-12 text-left control-label col-form-label">Correo Electrónico</label>
                        <div class="col-sm-12">
                            <input type="email" class="form-control" id="userMail" name="userMail" placeholder="Correo Electrónico" value="<?php if(isset($user_item)){echo $user_item['userMail'];} ?>">
                        </div>
                    </div>
                    <div class="form-group row col-sm-12 col-md-6 float-left">
                        <label for="userPassword" class="col-sm-12 text-left control-label col-form-label">Contraseña</label>
                        <div class="col-sm-12">
                            <input type="password" class="form-control" id="userPassword" name="userPassword" placeholder="Contraseña" value="<?php if(isset($user_item)){echo $user_item['userPassword'];} ?>">
                        </div>
                    </div>

                    <input type="hidden" name="action" value="<?= $action ?>">
                    <input type="hidden" name="lastPass" value="<?php if(isset($user_item)){echo $user_item['userPassword'];} ?>">
                    <input type="hidden" name="idUser" value="<?php if(isset($user_item)){echo $user_item['idUser'];} ?>">
                </div>
                <div class="clear"></div>
                <div class="border-top">
                    <div class="card-body">
                        <button type="submit" class="btn btn-success">Guardar Usuario</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
