<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="users-table" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Nombre de usuario</th>
                                <th>Correo Electrónico</th>
                                <th>Rol del usuario</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($users as $user) {
                                ?>
                                <tr>
                                    <td><?= $user['userName']; ?></td>
                                    <td><?= $user['userMail']; ?></td>
                                    <td><?php if($user['userRol'] == 1){echo 'Administrador';}else if($user['userRol'] == 2){echo 'Multimedia';}else if($user['userRol'] == 3){echo 'Escuelas';} ?></td>
                                    <td>
                                        <?php if ($user['permanentUser'] == FALSE && $_SESSION['idUser'] != $user['idUser']){ ?>
                                            <a href="/manager/usuarios/edit/<?= $user['idUser']; ?>"><button type="button" class="btn btn-primary btn-sm" title="Editar"><i class="m-r-10 mdi mdi-table-edit"></i>Editar</button></a>
                                            <button type="button" class="btn btn-danger btn-sm del-user" data-iduser="<?= $user['idUser']; ?>" title="Eliminar"><i class="m-r-10 mdi mdi-delete"></i>Eliminar</button>
                                        <?php } ?>
                                    </td>
                                </tr>
                                <?php
                            }
                             ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Nombre de usuario</th>
                                <th>Correo Electrónico</th>
                                <th>Rol del usuario</th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#users-table').DataTable();
    });
</script>
