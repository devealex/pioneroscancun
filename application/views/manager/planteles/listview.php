<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="planteles-table" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Estado</th>
                                <th>Nombre del plantel</th>
                                <th>Fecha de registro</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($planteles as $plantel) {
                                ?>
                                <tr>
                                    <td class="td-center">
                                        <input type="checkbox" name="PlantelAvailable" id="PlantelAvailable-<?= $plantel['idPlantel'] ?>" data-idplantel="<?= $plantel['idPlantel']; ?>" class="chxbx-table" value="<?= $plantel['PlantelAvailable']; ?>" <?php if($plantel['PlantelAvailable'] == 1){echo 'checked';} ?>>
                                        <label for="PlantelAvailable-<?= $plantel['idPlantel']; ?>" class="lbl-to-chxbx-table"></label>
                                    </td>
                                    <td><?= $plantel['plantelName'] ?></td>
                                    <td><?php $date = date_create($plantel['plantelRegister']); echo date_format($date, 'Y-m-d'); ?></td>
                                    <td>
                                        <a href="/manager/planteles/edit/<?= $plantel['idPlantel'] ?>"><button type="button" class="btn btn-primary btn-sm"><i class="m-r-10 mdi mdi-table-edit"></i>Editar</button></a>
                                        <button type="button" class="btn btn-danger btn-sm del-plantel" data-plantelimage="<?= trim($plantel['plantelImage']); ?>" data-idplantel="<?= $plantel['idPlantel']; ?>"><i class="m-r-10 mdi mdi-delete"></i>Eliminar</button>
                                        <a href="/manager/jugadores/listview/<?= $plantel['idPlantel'] ?>"><button type="button" class="btn btn-primary btn-sm"><i class="m-r-10 mdi mdi-soccer"></i>Jugadores</button></a>
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Estado</th>
                                <th>Nombre del plantel</th>
                                <th>Fecha de registro</th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#planteles-table').DataTable();
    });
</script>
