<div class="row">
    <div class="col-md-12">
        <div class="card">
            <form class="form-horizontal" id="form-plantel">
                <div class="card-body">
                    <div class="form-group row col-md-6 col-sm-12 float-left">
                            <label for="plantelName" class="col-sm-12 text-left control-label col-form-label">Nombre del plantel</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="plantelName" name="plantelName" maxlength="100" placeholder="Titulo" value="<?php if(isset($plantel_item)){echo $plantel_item['plantelName'];} ?>">
                        </div>
                    </div>
                    <div class="form-group row col-sm-12 col-md-6 float-left">
                        <label for="plantelSlug" class="col-sm-12 text-left control-label col-form-label">URL</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="plantelSlug" name="plantelSlug" placeholder="URL" disabled="true" value="<?php if(isset($plantel_item)){echo $plantel_item['plantelSlug'];} ?>">
                        </div>
                    </div>
                    <div class="form-group row col-sm-12">
                        <label class="col-sm-12 text-left control-label col-form-label">Image (1920 x 740)</label>
                        <div class="col-sm-12 div-preview form-control">
                            <input type="file" class="form-control file-img-preview" name="plantelImage" id="plantelImage" accept="image/*">
                            <img src="/assets/img/planteles/<?php if(isset($plantel_item) && $plantel_item['plantelImage'] != ''){echo $plantel_item['plantelImage'];}else{echo "default.jpg";} ?>" class="img-preview">
                        </div>
                    </div>
                    <input type="hidden" name="action" value="<?= $action ?>">
                    <input type="hidden" name="idPlantel" value="<?php if(isset($plantel_item)){echo $plantel_item['idPlantel'];} ?>">
                    <input type="hidden" name="plantelImageName" value="<?php if(isset($plantel_item)){echo $plantel_item['plantelImage'];} ?>">
                </div>
                <div class="border-top">
                    <div class="card-body">
                        <button type="submit" class="btn btn-success">Guardar Plantel</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
