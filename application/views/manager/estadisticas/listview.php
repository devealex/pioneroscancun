<?php
if (isset($idTorneo) && $idTorneo != null) {
    ?>
    <div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="estadisticas-table" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Lugar</th>
                                <th>Equipo</th>
                                <th>JJ</th>
                                <th>JG</th>
                                <th>JE</th>
                                <th>JP</th>
                                <th>GF</th>
                                <th>GC</th>
                                <th>DIF</th>
                                <th>PTS</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            $i = 1;
                            foreach ($estadisticas as $estadistica) {
                                ?>
                                <tr>
                                    <td class="t-a-center">
                                        <?= $i ?>
                                    </td>
                                    <td class="td-center t-a-center">
                                        <img src="/assets/img/equipos/<?= trim($estadistica['equipoLogo']) ?>" alt="">
                                    </td>
                                    <td class="t-a-center">
                                        <input class="inp-estadisticas" data-estype="jj" data-id="<?= $estadistica['idEstadistica'] ?>" type="number" min="0" max="150" value="<?= $estadistica['jj'] ?>">
                                    </td>
                                    <td class="t-a-center">
                                        <input class="inp-estadisticas" data-estype="jg" data-id="<?= $estadistica['idEstadistica'] ?>" type="number" min="0" max="150" value="<?= $estadistica['jg'] ?>">
                                    </td>
                                    <td class="t-a-center">
                                        <input class="inp-estadisticas" data-estype="je" data-id="<?= $estadistica['idEstadistica'] ?>" type="number" min="0" max="150" value="<?= $estadistica['je'] ?>">
                                    </td>
                                    <td class="t-a-center">
                                        <input class="inp-estadisticas" data-estype="jp" data-id="<?= $estadistica['idEstadistica'] ?>" type="number" min="0" max="150" value="<?= $estadistica['jp'] ?>">
                                    </td>
                                    <td class="t-a-center">
                                        <input class="inp-estadisticas" data-estype="gf" data-id="<?= $estadistica['idEstadistica'] ?>" type="number" min="0" max="150" value="<?= $estadistica['gf'] ?>">
                                    </td>
                                    <td class="t-a-center">
                                        <input class="inp-estadisticas" data-estype="gc" data-id="<?= $estadistica['idEstadistica'] ?>" type="number" min="0" max="150" value="<?= $estadistica['gc'] ?>">
                                    </td>
                                    <td class="t-a-center">
                                        <input class="inp-estadisticas" data-estype="dif" data-id="<?= $estadistica['idEstadistica'] ?>" type="number" min="0" max="150" value="<?= $estadistica['dif'] ?>">
                                    </td>
                                    <td class="t-a-center">
                                        <input class="inp-estadisticas" data-estype="pts" data-id="<?= $estadistica['idEstadistica'] ?>" type="number" min="0" max="150" value="<?= $estadistica['pts'] ?>">
                                    </td>
                                </tr>
                                <?php
                                $i++;
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Lugar</th>
                                <th>Equipo</th>
                                <th>JJ</th>
                                <th>JG</th>
                                <th>JE</th>
                                <th>JP</th>
                                <th>GF</th>
                                <th>GC</th>
                                <th>DIF</th>
                                <th>PTS</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>

<!--<span class="notificacion success">
    <p>Mensaje de prueba</p>
</span>-->

<script type="text/javascript">
    $(document).ready(function(){
        $('#estadisticas-table').DataTable();
    });
</script>
    <?php
} else {
    echo '<h1>SELECCIONE ALGUN TORNEO</h1>';
}
?>