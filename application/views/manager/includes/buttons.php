<?php
    $route = $_SERVER['REQUEST_URI'];
    //News Table
    if ($route == '/manager/noticias/listview') {
        ?>
        <div class="ml-auto">
            <a href="/manager/noticias/create"><button type="button" class="btn btn-success btn-sm"><i class="m-r-10 mdi mdi-creation"></i>Crear Noticia</button></a>
        </div>
        <?php
    }
    //Users Table
    if ($route == '/manager/usuarios/listview') {
        ?>
        <div class="ml-auto">
            <a href="/manager/usuarios/create"><button type="button" class="btn btn-success btn-sm"><i class="m-r-10 mdi mdi-creation"></i>Crear Usuario</button></a>
        </div>
        <?php
    }
    //Planteles Table
    if ($route == '/manager/planteles/listview') {
        ?>
        <div class="ml-auto">
            <a href="/manager/planteles/create"><button type="button" class="btn btn-success btn-sm"><i class="m-r-10 mdi mdi-creation"></i>Crear Plantel</button></a>
        </div>
        <?php
    }
    //Players table
    if (isset($idPlantel)) {
        if ($route == '/manager/jugadores/listview/'.$idPlantel) {
            ?>
            <div class="ml-auto">
                <a href="/manager/jugadores/create/<?= $idPlantel ?>"><button type="button" class="btn btn-success btn-sm"><i class="m-r-10 mdi mdi-creation"></i>Registrar Jugador</button></a>
            </div>
            <?php
        }
    }
    //Palmares table
    if ($route == '/manager/palmares/listview') {
        ?>
        <div class="ml-auto">
            <a href="/manager/palmares/create"><button type="button" class="btn btn-success btn-sm"><i class="m-r-10 mdi mdi-creation"></i>Registrar Campeonato</button></a>
        </div>
        <?php
    }
    //Galleries table
    if ($route == '/manager/galerias/listview') {
        ?>
        <div class="ml-auto">
            <a href="/manager/galerias/create"><button type="button" class="btn btn-success btn-sm"><i class="m-r-10 mdi mdi-creation"></i>Agregar Galeria</button></a>
        </div>
        <?php
    }
    //Videos table
    if ($route == '/manager/videos/listview') {
        ?>
        <div class="ml-auto">
            <a href="/manager/videos/create"><button type="button" class="btn btn-success btn-sm"><i class="m-r-10 mdi mdi-creation"></i>Agregar Video</button></a>
        </div>
        <?php
    }
    //Sponsors table
    if ($route == '/manager/patrocinadores/listview') {
        ?>
        <div class="ml-auto">
            <a href="/manager/patrocinadores/create"><button type="button" class="btn btn-success btn-sm"><i class="m-r-10 mdi mdi-creation"></i>Agregar Patrocinador</button></a>
        </div>
        <?php
    }
    //Suscriptions table
    if ($route == '/manager/suscripciones/listview') {
        ?>
        <div class="ml-auto">
            <form action="/manager/subscriptions/document" method="POST">
                <input name="dateInitial" type="hidden" value="<?php if(isset($dateInitial)){echo $dateInitial;} ?>">
                <input name="dateFinal" type="hidden" value="<?php if(isset($dateFinal)){echo $dateFinal;} ?>">
                <button type="submit" class="btn btn-success btn-sm"><i class="m-r-10 mdi mdi-file-excel"></i>Generar Excel</button>
            </form>
        </div>
        <?php
    }

    //Torneos table
    if ($route == '/manager/torneos/listview') {
        ?>
        <div class="ml-auto">
            <a href="/manager/equipos/listview"><button type="button" class="btn btn-primary btn-sm"><i class="m-r-10 mdi mdi-account-multiple"></i>Equipos</button></a>
            <a href="/manager/torneos/create"><button type="button" class="btn btn-success btn-sm"><i class="m-r-10 mdi mdi-creation"></i>Agregar Torneo</button></a>
        </div>
        <?php
    }

    //Torneos table
    if ($route == '/manager/equipos/listview') {
        ?>
        <div class="ml-auto">
            <a href="/manager/equipos/create"><button type="button" class="btn btn-success btn-sm"><i class="m-r-10 mdi mdi-creation"></i>Agregar Equipo</button></a>
        </div>
        <?php
    }

    //Estadisticas
    if (isset($_GET['t']) && $route == '/manager/estadisticas/listview?t='.$_GET['t']) {
        ?>
        <div class="ml-auto">
            <a href="/manager/goleo/listview?t=<?= $_GET['t'] ?>"><button type="button" class="btn btn-success btn-sm"><i class="m-r-10 mdi mdi-creation"></i>Goleadores</button></a>
        </div>
        <?php
    }

?>
