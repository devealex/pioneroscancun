<?php
    $route = $_SERVER['REQUEST_URI'];
    //News Table
    if ($route == '/manager/noticias/listview') {
        ?>
        <li class="nav-item dropdown">
            <a class="nav-link dropdown-toggle waves-effect waves-dark" href="" id="1" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"> <i class="font-12 mdi mdi-creation"></i> Categorias
            </a>
            <div class="dropdown-menu dropdown-menu-right mailbox animated bounceInDown" aria-labelledby="1">
                <ul class="list-style-none">
                    <li>
                        <div>
                            <?php
                                foreach ($categories as $category) {
                                    ?>
                                    <div class="link border-top">
                                        <div class="d-flex no-block align-items-center p-10">
                                            <input type="text" class="inp-category" data-id="<?= $category['idCategory'] ?>" name="category-<?= $category['idCategory'] ?>" value="<?= $category['category'] ?>" disabled>
                                            <input type="button" data-id="<?= $category['idCategory'] ?>" class="btn btn-success btn-sm btn-edit-category" value="Editar">
                                            <input type="button" data-id="<?= $category['idCategory'] ?>" class="btn btn-danger btn-sm btn-delete-category" value="Eliminar">
                                        </div>
                                    </div>
                                    <?php
                                }
                            ?>
                            <div class="link border-top">
                                <div class="d-flex no-block align-items-center p-10">
                                    <input type="text" class="inp-new-category" name="inp-new-category" value="">
                                    <input type="button" class="btn btn-success btn-sm" name="add-category" value="Agregar Categoria">
                                </div>
                            </div>
                        </div>
                    </li>
                </ul>
            </div>
        </li>
        <?php
    }
?>
