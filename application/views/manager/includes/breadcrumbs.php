<?php
$route = $_SERVER['REQUEST_URI'];
//noticias
if (($route == '/manager/noticias/create') || (isset($new_item) && $route == '/manager/noticias/edit/'.$new_item['idNew'])) {
    ?>
    <div class="row">
        <div class="mr-auto col-12">
             <nav aria-label="breadcrumb">
                 <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="/manager/noticias/listview">Noticias</a></li>
                     <?php if (isset($new_item)) {
                         ?>
                         <li class="breadcrumb-item active"><?= $new_item['newsTitle']; ?></li>
                         <?php
                     } ?>
                     <li class="breadcrumb-item active" aria-current="page"><strong><?= $section; ?></strong></li>
                 </ol>
             </nav>
         </div>
    </div>
    <?php
}
//planteles
if (
    ($route == '/manager/planteles/create') || (isset($plantel_item) && $route == '/manager/planteles/edit/'.$plantel_item['idPlantel']) || (isset($idPlantel) && $route == '/manager/jugadores/listview/'.$idPlantel) ) {
    ?>
    <div class="row">
        <div class="mr-auto col-12">
             <nav aria-label="breadcrumb">
                 <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="/manager/planteles/listview">Planteles</a></li>
                     <?php if (isset($plantelName)) { ?>
                     <li class="breadcrumb-item active"><?= $plantelName; ?></li>
                    <?php } ?>
                    <?php if (isset($plantel_item)) {
                        ?>
                        <li class="breadcrumb-item active"><?= $plantel_item['plantelName']; ?></li>
                        <?php
                    } ?>
                     <li class="breadcrumb-item active" aria-current="page"><strong><?= $section; ?></strong></li>
                 </ol>
             </nav>
         </div>
    </div>
    <?php
}
//jugadores
if ( (isset($idPlantel) && $route == '/manager/jugadores/create/'.$idPlantel) || (isset($player_item) && $route == '/manager/jugadores/edit/'.$player_item['idPlayer'].'/'.$idPlantel) ) {
    ?>
    <div class="row">
        <div class="mr-auto col-12">
             <nav aria-label="breadcrumb">
                 <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="/manager/planteles/listview">Planteles</a></li>
                     <?php if (isset($plantelName)) { ?>
                     <li class="breadcrumb-item active"><?= $plantelName; ?></li>
                    <?php } ?>
                     <li class="breadcrumb-item"><a href="/manager/jugadores/listview/<?= $idPlantel ?>">Jugadores</a></li>
                     <li class="breadcrumb-item active" aria-current="page"><strong><?= $section; ?></strong></li>
                 </ol>
             </nav>
         </div>
    </div>
    <?php
}
//escuelas
if (($route == '/manager/escuelas/create') || (isset($school_item) && $route == '/manager/escuelas/edit/'.$school_item['idSchool'])) {
    ?>
    <div class="row">
        <div class="mr-auto col-12">
             <nav aria-label="breadcrumb">
                 <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="/manager/escuelas/listview">Escuelas</a></li>
                     <?php if (isset($school_item)) {
                         ?>
                         <li class="breadcrumb-item active"><?= $school_item['nameSchool']; ?></li>
                         <?php
                     } ?>
                     <li class="breadcrumb-item active" aria-current="page"><strong><?= $section; ?></strong></li>
                 </ol>
             </nav>
         </div>
    </div>
    <?php
}
//usuarios
if (($route == '/manager/usuarios/create') || (isset($user_item) && $route == '/manager/usuarios/edit/'.$user_item['idUser'])) {
    ?>
    <div class="row">
        <div class="mr-auto col-12">
             <nav aria-label="breadcrumb">
                 <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="/manager/usuarios/listview">Usuarios</a></li>
                     <?php if (isset($user_item)) {
                         ?>
                         <li class="breadcrumb-item active"><?= $user_item['userName']; ?></li>
                         <?php
                     } ?>
                     <li class="breadcrumb-item active" aria-current="page"><strong><?= $section; ?></strong></li>
                 </ol>
             </nav>
         </div>
    </div>
    <?php
}
//palmares
if (($route == '/manager/palmares/create') || (isset($campeonato_item) && $route == '/manager/palmares/edit/'.$campeonato_item['idChampionship'])) {
    ?>
    <div class="row">
        <div class="mr-auto col-12">
             <nav aria-label="breadcrumb">
                 <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="/manager/palmares/listview">Palmares</a></li>
                     <?php if (isset($campeonato_item)) {
                         ?>
                         <li class="breadcrumb-item active"><?= $campeonato_item['cupName']; ?></li>
                         <?php
                     } ?>
                     <li class="breadcrumb-item active" aria-current="page"><strong><?= $section; ?></strong></li>
                 </ol>
             </nav>
         </div>
    </div>
    <?php
}
//galleries
if (($route == '/manager/galerias/create') || (isset($album_item) && $route == '/manager/galerias/edit/'.$album_item['idAlbum'])) {
    ?>
    <div class="row">
        <div class="mr-auto col-12">
             <nav aria-label="breadcrumb">
                 <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="/manager/galerias/listview">Galerias</a></li>
                     <?php if (isset($album_item)) {
                         ?>
                         <li class="breadcrumb-item active"><?= $album_item['albumName']; ?></li>
                         <?php
                     } ?>
                     <li class="breadcrumb-item active" aria-current="page"><strong><?= $section; ?></strong></li>
                 </ol>
             </nav>
         </div>
    </div>
    <?php
}
//imagenes
if ((isset($idAlbum) && $route == '/manager/imagenes/'.$idAlbum)) {
    ?>
    <div class="row">
        <div class="mr-auto col-12">
             <nav aria-label="breadcrumb">
                 <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="/manager/galerias/listview">Galerias</a></li>
                     <?php if (isset($album)) {
                         ?>
                         <li class="breadcrumb-item active"><?= $album['albumName']; ?></li>
                         <?php
                     } ?>
                     <li class="breadcrumb-item active" aria-current="page"><strong><?= $section; ?></strong></li>
                 </ol>
             </nav>
         </div>
    </div>
    <?php
}
//Sponsors
if ($route == '/manager/patrocinadores/create') {
    ?>
    <div class="row">
        <div class="mr-auto col-12">
             <nav aria-label="breadcrumb">
                 <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="/manager/patrocinadores/listview">Patrocinadores</a></li>
                     <li class="breadcrumb-item active" aria-current="page"><strong><?= $section; ?></strong></li>
                 </ol>
             </nav>
         </div>
    </div>
    <?php
}
//Videos
if ($route == '/manager/videos/create' || ( isset($video_item) && $route == '/manager/videos/edit/' . $video_item['idVideo'] )) {
    ?>
    <div class="row">
        <div class="mr-auto col-12">
             <nav aria-label="breadcrumb">
                 <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="/manager/videos/listview">Videos</a></li>
                        <?php if (isset($video_item)) { ?><li class="breadcrumb-item active" aria-current="page"><?= $video_item['videoTitle']; ?></li><?php } ?>
                     <li class="breadcrumb-item active" aria-current="page"><strong><?= $section; ?></strong></li>
                 </ol>
             </nav>
         </div>
    </div>
    <?php
}
//Torneos
if ($route == '/manager/torneos/create' || ( isset($torneo_item) && $route == '/manager/torneos/edit/' . $torneo_item['idTorneo'] ) || ( isset($calendario[0]) && $route == '/manager/calendario/listview?t=' . $calendario[0]['fkTorneoRol'] ) ) {
    ?>
    <div class="row">
        <div class="mr-auto col-12">
             <nav aria-label="breadcrumb">
                 <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="/manager/torneos/listview">Torneos</a></li>
                     <?php if (isset($calendario)) { ?><li class="breadcrumb-item active" aria-current="page"><?= $calendario[0]['torneo']; ?></li><?php } ?>
                     <?php if (isset($torneo_item)) { ?><li class="breadcrumb-item active" aria-current="page"><?= $torneo_item['torneo']; ?></li><?php } ?>
                     <li class="breadcrumb-item active" aria-current="page"><strong><?= $section; ?></strong></li>
                 </ol>
             </nav>
         </div>
    </div>
    <?php
}
if ($route == '/manager/equipos/listview') {
    ?>
    <div class="row">
        <div class="mr-auto col-12">
             <nav aria-label="breadcrumb">
                 <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="/manager/torneos/listview">Torneos</a></li>
                     <li class="breadcrumb-item active" aria-current="page"><strong><?= $section; ?></strong></li>
                 </ol>
             </nav>
         </div>
    </div>
    <?php
}
if ($route == '/manager/equipos/create' || (isset($equipo_item) && $route == '/manager/equipos/edit/'.$equipo_item['idEquipo'])) {
    ?>
    <div class="row">
        <div class="mr-auto col-12">
             <nav aria-label="breadcrumb">
                 <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="/manager/torneos/listview">Torneos</a></li>
                     <li class="breadcrumb-item"><a href="/manager/equipos/listview">Equipos</a></li>
                     <?php if(isset($equipo_item)){ ?><li class="breadcrumb-item active" aria-current="page"><?= $equipo_item['equipoName']; ?></li><?php } ?>
                     <li class="breadcrumb-item active" aria-current="page"><strong><?= $section; ?></strong></li>
                 </ol>
             </nav>
         </div>
    </div>
    <?php
}
if (isset($_GET['t']) && $route == '/manager/jornadas/listview?t='.$_GET['t']) {
    ?>
    <div class="row">
        <div class="mr-auto col-12">
             <nav aria-label="breadcrumb">
                 <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="/manager/torneos/listview">Torneos</a></li>
                     <li class="breadcrumb-item active" aria-current="page"><?= (isset($jornadas[0]['torneo'])) ? $jornadas[0]['torneo'] : ''; ?></li>
                     <li class="breadcrumb-item active" aria-current="page"><strong><?= $section; ?></strong></li>
                 </ol>
             </nav>
         </div>
    </div>
    <?php
}
if ( ( isset($_GET['t']) && (isset($_GET['j'])) ) && ( $route == '/manager/calendario/listview?t='.$_GET['t'].'&j='.$_GET['j'] || $route == '/manager/calendario/listview?j='.$_GET['j'].'&t='.$_GET['t'] ) ) {
    ?>
    <div class="row">
        <div class="mr-auto col-12">
             <nav aria-label="breadcrumb">
                 <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="/manager/torneos/listview">Torneos</a></li>
                     <li class="breadcrumb-item active" aria-current="page"><?= (isset($torneo['torneo'])) ? $torneo['torneo'] : ''; ?></li>
                     <li class="breadcrumb-item"><a href="/manager/jornadas/listview?t=<?= $_GET['t'] ?>">Jornadas</a></li>
                     <li class="breadcrumb-item active" aria-current="page">Jornada <?= $_GET['j'] ?></li>
                     <li class="breadcrumb-item active" aria-current="page"><strong><?= $section; ?></strong></li>
                 </ol>
             </nav>
         </div>
    </div>
    <?Php
}
if (isset($_GET['t']) && $route == '/manager/estadisticas/listview?t='.$_GET['t']) {
    ?>
    <div class="row">
        <div class="mr-auto col-12">
             <nav aria-label="breadcrumb">
                 <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="/manager/torneos/listview">Torneos</a></li>
                     <li class="breadcrumb-item active" aria-current="page"><?= (isset($torneo['torneo'])) ? $torneo['torneo'] : ''; ?></li>
                     <li class="breadcrumb-item active" aria-current="page"><strong><?= $section; ?></strong></li>
                 </ol>
             </nav>
         </div>
    </div>
    <?php
}

if (isset($_GET['t']) && $route == '/manager/goleo/listview?t='.$_GET['t']) {
    ?>
    <div class="row">
        <div class="mr-auto col-12">
             <nav aria-label="breadcrumb">
                 <ol class="breadcrumb">
                     <li class="breadcrumb-item"><a href="/manager/torneos/listview">Torneos</a></li>
                     <li class="breadcrumb-item active" aria-current="page"><?= (isset($torneo['torneo'])) ? $torneo['torneo'] : ''; ?></li>
                     <li class="breadcrumb-item"><a href="/manager/estadisticas/listview?t=<?= $_GET['t'] ?>">Torneos</a></li>
                     <li class="breadcrumb-item active" aria-current="page"><strong><?= $section; ?></strong></li>
                 </ol>
             </nav>
         </div>
    </div>
    <?php
}

?>
