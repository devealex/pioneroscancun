<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="goleo-table" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>#</th>
                                <th>Nombre</th>
                                <th>Equipo</th>
                                <th>Goles</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach($goleos as $i => $goleo) {
                            ?>
                            <tr>
                                <td><?= $i + 1 ?></td>
                                <td class="t-a-center"><input type="text" name="nombre" placeholder="Nombre Completo" class="input-goleo" data-campo="nombre" data-id="<?= $goleo['idgoleo'] ?>" value="<?= $goleo['nombre'] ?>"></td>
                                <td class="t-a-center"><select name="equipo" class="input-goleo" data-campo="equipo" data-id="<?= $goleo['idgoleo'] ?>">
                            <?php 
                                $options = '<option value="">Sin equipo</option>';
                                foreach($equipos as $equipo){
                                    $options .= '<option value="' . $equipo['equipoName'] . '" ' . (($goleo['equipo'] == $equipo['equipoName']) ? 'selected' : '') . '>' . $equipo['equipoName'] . '</option>';
                                }
                                echo $options;
                            ?>
                                </select></td>
                                <td class="t-a-center"><input type="number" name="goles" class="input-goleo" data-campo="goles" data-id="<?= $goleo['idgoleo'] ?>" value="<?= $goleo['goles'] ?>"></td>
                            </tr>
                            <?php
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Nombre</th>
                                <th>Equipo</th>
                                <th>Goles</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#goleo-table').DataTable();
    });
</script>
