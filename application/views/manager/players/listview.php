<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="players-table" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Estado</th>
                                <th>Nombre del jugador</th>
                                <th>Nombre del plantel</th>
                                <th>Posición</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php

                            foreach ($players as $player) {
                                ?>
                                <tr>
                                    <td class="td-center">
                                        <input type="checkbox" name="playerAvailable" id="playerAvailable-<?= $player['idPlayer'] ?>" data-idplayer="<?= $player['idPlayer']; ?>" class="chxbx-table" value="<?= $player['playerAvailable']; ?>" <?php if($player['playerAvailable'] == 1){echo 'checked';} ?>>
                                        <label for="playerAvailable-<?= $player['idPlayer']; ?>" class="lbl-to-chxbx-table"></label>
                                    </td>
                                    <td>
                                        <?= $player['playerName'] ?>
                                    </td>
                                    <td>
                                        <?= $player['plantelName'] ?>
                                    </td>
                                    <td>
                                        <?php
                                        if ($player['playerPosition'] == 1) {
                                            echo 'Portero';
                                        }
                                        if ($player['playerPosition'] == 2) {
                                            echo 'Defensa';
                                        }
                                        if ($player['playerPosition'] == 3) {
                                            echo 'Medio';
                                        }
                                        if ($player['playerPosition'] == 4) {
                                            echo 'Delantero';
                                        }
                                        if ($player['playerPosition'] == 5) {
                                            echo 'Cuerpo Técnico';
                                        }
                                        ?>
                                    </td>
                                    <td>
                                        <a href="/manager/jugadores/edit/<?= $player['idPlayer'] ?>/<?= $idPlantel ?>"><button type="button" class="btn btn-primary btn-sm"><i class="m-r-10 mdi mdi-table-edit"></i>Editar</button></a>
                                        <button type="button" class="btn btn-danger btn-sm del-player" data-imgfrente="<?php $img = json_decode($player['playerPhotos'], true);echo $img['imgFrente']; ?>" data-imgdetras="<?php $img = json_decode($player['playerPhotos'], true);echo $img['imgDetras']; ?>" data-idplayer="<?= $player['idPlayer']; ?>"><i class="m-r-10 mdi mdi-delete"></i>Eliminar</button>
                                    </td>
                                </tr>
                                <?php
                            }

                             ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Estado</th>
                                <th>Nombre del jugador</th>
                                <th>Nombre del plantel</th>
                                <th>Posición</th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#players-table').DataTable();
    });
</script>
