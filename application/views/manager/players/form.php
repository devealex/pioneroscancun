<div class="row">
    <div class="col-md-12">
        <div class="card">
            <form class="form-horizontal" id="form-player">

                <div class="card-body" style="overflow:hidden;">
                    <div class="form-group row col-md-6 col-sm-12 float-left">
                            <label for="playerName" class="col-sm-12 text-left control-label col-form-label">Nombre del jugador</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="playerName" name="playerName" placeholder="Nombre del jugador" value="<?php if(isset($player_item)){echo $player_item['playerName'];} ?>">
                        </div>
                    </div>
                    <div class="form-group row col-md-6 col-sm-12 float-left">
                        <div class="col-sm-6 float-left">
                            <label for="playerHeight" class="col-sm-12 text-left control-label col-form-label">Estatura</label>
                            <div class="col-sm-12">
                                <input type="number" step="0.01" class="form-control" id="playerHeight" name="playerHeight" placeholder="Estatura" value="<?php if(isset($player_item)){echo $player_item['playerHeight'];} ?>">
                            </div>
                        </div>
                        <div class="col-sm-6 float-left">
                            <label for="playerHeight" class="col-sm-12 text-left control-label col-form-label">Numero de Jugador</label>
                            <div class="col-sm-12">
                                <input type="number" class="form-control" id="playerNumber" name="playerNumber" placeholder="Numero de Jugador" value="<?php if(isset($player_item)){echo $player_item['playerNumber'];} ?>">
                            </div>
                        </div>
                    </div>
                    <div class="form-group row col-md-6 col-sm-12 float-left">
                            <label for="playerOrigin" class="col-sm-12 text-left control-label col-form-label">Origen del jugador</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="playerOrigin" name="playerOrigin" placeholder="Ciudad, Pais" value="<?php if(isset($player_item)){echo $player_item['playerOrigin'];} ?>">
                        </div>
                    </div>
                    <div class="form-group row col-md-6 col-sm-12 float-left">
                            <label for="previousTeam" class="col-sm-12 text-left control-label col-form-label">Equipo Anterior</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="previousTeam" name="previousTeam" placeholder="Equipo Anterior" value="<?php if(isset($player_item)){echo $player_item['previousTeam'];} ?>">
                        </div>
                    </div>
                    <div class="form-group row col-sm-12 col-md-6 float-left">
                        <label for="plantel" class="col-sm-12 text-left control-label col-form-label">Plantel</label>
                        <div class="col-sm-12">
                            <select class="form-control custom-select" name="plantel">
                                <?php
                                foreach ($planteles as $plantel) {
                                    ?>
                                    <option value="<?= $plantel['idPlantel'] ?>" <?php if((isset($player_item) && $player_item['idPlantel'] == $plantel['idPlantel']) || ($plantel['idPlantel'] == $idPlantel)){echo 'selected';} ?>><?= $plantel['plantelName'] ?></option>
                                    <?php
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row col-md-6 col-sm-12 float-left">
                        <label for="playerPosition" class="col-sm-12 text-left control-label col-form-label">Posición</label>
                        <div class="col-sm-12">
                            <select class="form-control custom-select" name="playerPosition">
                                <option value="1" <?php if(isset($player_item) && $player_item['playerPosition'] == 1){echo 'selected';} ?>>Portero</option>
                                <option value="2" <?php if(isset($player_item) && $player_item['playerPosition'] == 2){echo 'selected';} ?>>Defensa</option>
                                <option value="3" <?php if(isset($player_item) && $player_item['playerPosition'] == 3){echo 'selected';} ?>>Medio</option>
                                <option value="4" <?php if(isset($player_item) && $player_item['playerPosition'] == 4){echo 'selected';} ?>>Delantero</option>
                                <option value="5" <?php if(isset($player_item) && $player_item['playerPosition'] == 5){echo 'selected';} ?>>Cuerpo Técnico</option>
                            </select>
                        </div>
                        <br>
                        <label for="playerFunction" class="playerFunction-label col-sm-12 text-left control-label col-form-label" style="<?php if(isset($player_item) && $player_item['playerPosition'] == 5){echo 'display: initial;';} else {echo 'display: none;';} ?>">Función</label>
                        <div class="col-sm-12">
                            <input type="text" name="playerFunction" id="playerFunction" style="<?php if(isset($player_item) && $player_item['playerPosition'] == 5){echo 'display: initial;';} else {echo 'display: none;';} ?>" class="form-control" value="<?php if(isset($player_item) && ($player_item['playerFunction'] != null || $player_item['playerFunction'] != '')){echo $player_item['playerFunction'];} ?>">
                        </div>
                    </div>
                    <div class="form-group row col-sm-12 col-md-6 float-left">
                        <label class="col-sm-12 col-md-10 text-left control-label col-form-label">Imagen Frente 1 (350 x 395)</label>
                        <div class="col-sm-12 col-md-10 div-preview form-control">
                            <input type="file" class="form-control file-img-preview" name="imgFrente" id="imgFrente" accept="image/*">
                            <img id="prev-frente" src="/assets/img/players/<?php if(isset($player_item) && $player_item['playerPhotos'] != ''){$img = json_decode($player_item['playerPhotos'], true);echo $img['imgFrente'];}else{echo 'default.jpg';} ?>" class="img-preview">
                        </div>
                    </div>
                    <div class="form-group row col-sm-12 col-md-6 float-left">
                        <label class="col-sm-12 col-md-10 text-left control-label col-form-label">Imagen Detras 2 (350 x 395)</label>
                        <div class="col-sm-12 col-md-10 div-preview form-control">
                            <input type="file" class="form-control file-img-preview" name="imgDetras" id="imgDetras" accept="image/*">
                            <img id="prev-detras" src="/assets/img/players/<?php if(isset($player_item) && $player_item['playerPhotos'] != ''){$img = json_decode($player_item['playerPhotos'], true);echo $img['imgDetras'];}else{echo 'default.jpg';} ?>" class="img-preview">
                        </div>
                    </div>

                    <input type="hidden" name="action" value="<?= $action ?>">
                    <input type="hidden" name="idPlayer" value="<?php if(isset($player_item)){echo $player_item['idPlayer'];} ?>">
                    <input type="hidden" name="idPlantel" value="<?= $idPlantel ?>">
                    <input type="hidden" name="imgFrenteName" value="<?php if(isset($player_item) && $player_item['playerPhotos'] != ''){$img = json_decode($player_item['playerPhotos'], true);echo $img['imgFrente'];} ?>">
                    <input type="hidden" name="imgDetrasName" value="<?php if(isset($player_item) && $player_item['playerPhotos'] != ''){$img = json_decode($player_item['playerPhotos'], true);echo $img['imgDetras'];} ?>">


                </div>

                <div class="clr"></div>

                <div class="border-top">
                    <div class="card-body">
                        <button type="submit" class="btn btn-success">Guardar Jugador</button>
                    </div>
                </div>

                <div class="clr"></div>
            </form>
        </div>
    </div>
</div>
