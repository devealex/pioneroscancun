<?php
header('Content-type:application/xls');
header('Content-Disposition: attachment; filename=newsLetterList'. date('Y-m-d') .'.xls');
?>

<table>
    <tr>
        <th>Correo Electronico</th>
        <th>Fecha de Registro</th>
    </tr>
    <?php
    foreach($subscriptions as $subscription) {
        ?>
        <tr>
            <td><?= $subscription['email'] ?></td>
            <td><?= $subscription['dateSubscription'] ?></td>
        </tr>
        <?php
    }
    ?>
</table>