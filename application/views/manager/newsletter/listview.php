<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <form action="/manager/suscripciones/listview" id="frm-suscriptions" method="POST">
                    <input type="date" name="dateInitial" value="<?php if(isset($dateInitial)){echo $dateInitial;} ?>">
                    <input type="date" name="dateFinal" value="<?php if(isset($dateFinal)){echo $dateFinal;} ?>">
                    <button type="submit">Buscar</button>
                </form>
                <div class="table-responsive">
                    <table id="newsletter-table" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Correo Electronico</th>
                                <th>Fecha de Suscribción</th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach($subscriptions as $subscription) {
                                ?>
                                <tr>
                                    <td><?= $subscription['email'] ?></td>
                                    <td><?= date('d/m/Y', strtotime($subscription['dateSubscription'])) ?></td>
                                </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Correo Electronico</th>
                                <th>Fecha de Suscribción</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#newsletter-table').DataTable();
    });
</script>
