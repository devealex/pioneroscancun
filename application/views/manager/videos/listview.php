<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="videos-table" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Titulo</th>
                                <th>Fecha de Publicación</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($videos as $video) {
                                ?>
                                <tr>
                                    <td><?= $video['videoTitle'] ?></td>
                                    <td><?php $date = date_create($video['videoPublish']); echo date_format($date, 'd/m/Y'); ?></td>
                                    <td>
                                        <a href="/manager/videos/edit/<?= $video['idVideo'] ?>"><button type="button" class="btn btn-primary btn-sm"><i class="m-r-10 mdi mdi-table-edit"></i>Editar</button></a>
                                        <button type="button" class="btn btn-danger btn-sm del-video" data-idvideo="<?= $video['idVideo']; ?>" data-videoavatar="<?= trim($video['videoAvatar']) ?>"><i class="m-r-10 mdi mdi-delete"></i>Eliminar</button>
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Titulo</th>
                                <th>Fecha de Publicación</th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#videos-table').DataTable();
    });
</script>
