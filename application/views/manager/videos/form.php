<div class="row">
    <div class="col-md-12">
        <div class="card">

            <form class="form-horizontal" id="form-video">
                <div class="card-body">
                    <div class="form-group row col-sm-12 col-md-6 float-left">
                        <label for="videoTitle" class="col-sm-12 text-left control-label col-form-label">Titulo del Video</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="videoTitle" name="videoTitle" placeholder="Titulo del Video" value="<?php if(isset($video_item)){echo $video_item['videoTitle'];} ?>">
                        </div>
                    </div>
                    <div class="form-group row col-sm-12 col-md-6 float-left">
                        <label for="videoPublish" class="col-sm-12 text-left control-label col-form-label">Fecha de publicación</label>
                        <div class="col-sm-12">
                            <input type="date" class="form-control"  name="videoPublish" id="videoPublish" value="<?php if(isset($video_item)){echo $video_item['videoPublish'];} ?>">
                        </div>
                    </div>
                    <div class="form-group row col-sm-12 float-left">
                        <label for="videoIframe" class="col-sm-12 text-left control-label col-form-label">Iframe del video</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="videoIframe" name="videoIframe" placeholder="Iframe del video" value='<?php if(isset($video_item)){echo $video_item['videoIframe'];} ?>'>
                        </div>
                    </div>
                    <div class="form-group row col-sm-12">
                        <label class="col-sm-12 text-left control-label col-form-label">Imagen (1280 x 720)</label>
                        <div class="col-sm-12 div-preview form-control">
                            <input type="file" class="form-control file-img-preview" name="videoAvatar" id="videoAvatar" accept="image/*">
                            <img src="/assets/img/videos/<?php if(isset($video_item) && $video_item['videoAvatar'] != ''){echo $video_item['videoAvatar'];}else{echo "default.jpg";} ?>" class="img-preview">
                        </div>
                    </div>
                    <input type="hidden" name="action" value="<?= $action ?>">
                    <input type="hidden" name="nameAvatar" value="<?php if (isset($video_item)) {echo $video_item['videoAvatar'];} ?>">
                    <input type="hidden" name="idVideo" value="<?php if(isset($video_item)){echo $video_item['idVideo'];} ?>">
                </div>
                <div class="clear"></div>
                <div class="border-top">
                    <div class="card-body">
                        <button type="submit" class="btn btn-success">Guardar Video</button>
                    </div>
                </div>
            </form>

        </div>
    </div>
</div>
