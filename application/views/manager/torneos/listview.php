<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="torneos-table" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Disponible</th>
                                <th>Nombre de Torneo</th>
                                <th>Plantel</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($torneos as $torneo) {
                                ?>
                                <tr>
                                    <td class="td-center">
                                        <input type="checkbox" name="torneoAvailable" id="torneoAvailable-<?= $torneo['idTorneo'] ?>" data-idtorneo="<?= $torneo['idTorneo']; ?>" class="chxbx-table" value="<?= $torneo['torneoAvailable']; ?>" <?php if($torneo['torneoAvailable'] == 1){echo 'checked';} ?>>
                                        <label for="torneoAvailable-<?= $torneo['idTorneo']; ?>" class="lbl-to-chxbx-table"></label>
                                    </td>
                                    <td>
                                        <?= $torneo['torneo'] ?>
                                    </td>
                                    <td>
                                        <?= $torneo['plantelName'] ?>
                                    </td>
                                    <td>
                                        <a href="/manager/jornadas/listview?t=<?= $torneo['idTorneo'] ?>"><button type="button" class="btn btn-success btn-sm"><i class="m-r-10 mdi mdi-calendar"></i>Jornadas</button></a>
                                        <a href="/manager/estadisticas/listview?t=<?= $torneo['idTorneo'] ?>"><button type="button" class="btn btn-success btn-sm"><i class="m-r-10 mdi mdi-chart-bar"></i>Estadisticas</button></a>
                                        <a href="/manager/torneos/edit/<?= $torneo['idTorneo'] ?>"><button type="button" class="btn btn-primary btn-sm"><i class="m-r-10 mdi mdi-table-edit"></i>Editar</button></a>
                                        <button type="button" class="btn btn-danger btn-sm del-torneo" data-idtorneo="<?= $torneo['idTorneo']; ?>"><i class="m-r-10 mdi mdi-delete"></i>Eliminar</button>
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Disponible</th>
                                <th>Nombre de Torneo</th>
                                <th>Plantel</th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#torneos-table').DataTable();
    });
</script>
