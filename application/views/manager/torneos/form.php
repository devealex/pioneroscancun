<div class="row">
    <div class="col-md-12">
        <div class="card">
            <form class="form-horizontal" id="form-torneos">
                <div class="card-body">

                    <div class="form-group row col-md-6 col-sm-12 float-left">
                        <label for="torneo" class="col-sm-12 text-left control-label col-form-label">Nombre de Torneo</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="torneo" name="torneo" placeholder="Nombre de Torneo" value="<?php if(isset($torneo_item)){echo $torneo_item['torneo'];} ?>">
                        </div>
                    </div>

                    <div class="form-group row col-md-6 col-sm-12 float-left">
                        <label for="torneoPlantel" class="col-sm-12 text-left control-label col-form-label">Plantel</label>
                        <div class="col-sm-12 col-md-6">
                            <select class="selcat form-control custom-select" name="torneoPlantel">
                                <?php
                                foreach ($planteles as $plantel) {
                                    if (isset($torneo_item)) {
                                        if ($torneo_item['idPlantel'] == $plantel['idPlantel']) {
                                            ?>
                                            <option value="<?= $plantel['idPlantel'] ?>" selected><?= $plantel['plantelName'] ?></option>
                                            <?php
                                        } else {
                                            ?>
                                            <option value="<?= $plantel['idPlantel'] ?>"><?= $plantel['plantelName'] ?></option>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <option value="<?= $plantel['idPlantel'] ?>"><?= $plantel['plantelName'] ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row col-md-6 col-sm-12 float-left">
                        <label for="torneourl" class="col-sm-12 text-left control-label col-form-label">URL</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="torneourl" name="torneourl" readonly placeholder="torneo-url" value="<?php if(isset($torneo_item)){echo $torneo_item['torneourl'];} ?>">
                        </div>
                    </div>

                    <div class="form-group row col-md-6 col-sm-12 float-left">
                        <label for="torneo" class="col-sm-12 text-left control-label col-form-label">Jornadas</label>
                        <div class="col-sm-12">
                            <input type="number" class="form-control" id="jornadas" name="jornadas" placeholder="Jornadas" <?php if(isset($jornadas)){echo 'readOnly';} ?> value="<?php if(isset($jornadas)){echo $jornadas;} ?>">
                        </div>
                    </div>

                    <label class="col-sm-12 text-left control-label col-form-label">Equipos</label>
                    <div class="form-group row col-md-12 col-sm-12 float-left lst-equipos">
                        <?php
                        $arrayIdEquipo = [];
                        foreach($equipos as $equipo){
                            $isSelect = 0;
                            if (isset($misEquipos)) {
                                foreach ($misEquipos as $miEquipo) {
                                    if ($equipo['idEquipo'] == $miEquipo['fkEquipo']) {
                                        $isSelect = 1;
                                        array_push($arrayIdEquipo, $miEquipo['fkEquipo']);
                                    }
                                }
                            }
                            ?>
                            <div class="col-sm-6 col-md-2 col-6 float-left">
                                <article class="artEquipo">
                                    <span data-isselect="<?= $isSelect ?>" data-id="<?= $equipo['idEquipo'] ?>" class="mask select-team <?php if($isSelect == 1){echo 'selected';} ?>">
                                        <i class="mdi mdi-checkbox-marked-circle"></i>
                                    </span>
                                    <div class="cont-img">
                                        <img src="/assets/img/equipos/<?= $equipo['equipoLogo'] ?>" alt="">
                                    </div>
                                    <div class="cont-name">
                                        <?= $equipo['equipoName'] ?>
                                    </div>
                                </article>
                            </div>
                            <?php
                        }
                        ?>
                    </div>
                    <input type="hidden" name="equipos" value="<?= implode(',', $arrayIdEquipo) ?>">
                    <input type="hidden" name="equiposDel" value="">
                    <input type="hidden" name="action" value="<?= $action ?>">
                    <input type="hidden" name="idTorneo" value="<?php if(isset($torneo_item)){echo $torneo_item['idTorneo'];} ?>">
                <div class="clear"></div>
                <div class="border-top">
                    <div class="card-body">
                        <button type="submit" class="btn btn-success">Guardar Torneo</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
