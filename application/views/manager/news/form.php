<?php if ($categories != null) { ?>
<div class="row">
    <div class="col-md-12">
        <div class="card">
            <form class="form-horizontal" id="form-new">
                <div class="card-body">
                    <div class="form-group row col-md-6 col-sm-12 float-left">
                        <label for="newsTitle" class="col-sm-12 text-left control-label col-form-label">Titulo</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="newsTitle" maxlength="100" name="newsTitle" placeholder="Titulo" value="<?php if(isset($new_item)){echo $new_item['newsTitle'];} ?>">
                        </div>
                    </div>
                    <div class="form-group row col-md-6 col-sm-12 float-left">
                        <label for="newsTitle" class="col-sm-12 text-left control-label col-form-label">Categoria</label>
                        <div class="col-sm-12 col-md-6">
                            <select class="selcat form-control custom-select" name="newsCategory">
                                <?php
                                foreach ($categories as $category) {
                                    if (isset($new_item)) {
                                        if ($new_item['idCategory'] == $category['idCategory']) {
                                            ?>
                                            <option value="<?= $category['idCategory'] ?>" selected><?= $category['category'] ?></option>
                                            <?php
                                        } else {
                                            ?>
                                            <option value="<?= $category['idCategory'] ?>"><?= $category['category'] ?></option>
                                            <?php
                                        }
                                    } else {
                                        ?>
                                        <option value="<?= $category['idCategory'] ?>"><?= $category['category'] ?></option>
                                        <?php
                                    }
                                }
                                ?>
                            </select>
                        </div>
                    </div>
                    <div class="form-group row col-sm-12 col-md-6 float-left">
                        <label for="newsUrl" class="col-sm-12 text-left control-label col-form-label">URL</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="newsUrl" name="newsUrl" placeholder="URL" disabled="true" value="<?php if(isset($new_item)){echo $new_item['newsUrl'];} ?>">
                        </div>
                    </div>
                    <div class="form-group row col-sm-12 col-md-6 float-left">
                        <label for="" class="col-sm-12 text-left control-label col-form-label">Fecha de publicación</label>
                        <div class="col-sm-12">
                            <input type="date" class="form-control"  name="newsPublished" id="newsPublished" value="<?php if(isset($new_item)){echo $new_item['newsPublished'];} ?>">
                        </div>
                    </div>
                    <div class="form-group row col-sm-12">
                        <label class="col-sm-12 text-left control-label col-form-label">Image (1024 x 680)</label>
                        <div class="col-sm-12 div-preview form-control">
                            <input type="file" class="form-control file-img-preview" name="newsAvatar" id="newsAvatar" accept="image/*">
                            <img src="/assets/img/news/<?php if(isset($new_item) && $new_item['newsAvatar'] != ''){echo $new_item['newsAvatar'];}else{echo "default.jpg";} ?>" class="img-preview">
                        </div>
                    </div>
                    <div class="form-group row col-sm-12">
                        <label for="newsContent" class="col-sm-12 text-left control-label col-form-label">Contenido</label>
                        <div class="col-sm-12 form-control focus-content">
                            <div id="newsContent" class="quill-edit-content" style="height: 300px;">
                                <?php if(isset($new_item)){echo $new_item['newsContent'];} ?>
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="action" value="<?= $action ?>">
                    <input type="hidden" name="idNew" value="<?php if(isset($new_item)){echo $new_item['idNew'];} ?>">
                    <input type="hidden" name="nameAvatar" value="<?php if(isset($new_item)){echo $new_item['newsAvatar'];} ?>">
                </div>
                <div class="border-top">
                    <div class="card-body">
                        <button type="submit" class="btn btn-success">Guardar Noticia</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        // QUILL EDITOR
        var toolbarOptions =[
            [{ 'size': ['small', false, 'large', 'huge'] }],
            [{ 'header': [1, 2, 3, 4, 5, 6, false] }],
            [{ 'font': [] }],

            ['bold', 'italic', 'underline', 'strike'],

            [{ 'color': [] }, { 'background': [] }],

            [{ 'align': [] }],

            ['image', 'video'],
            // ['video'],

            ['clean']
        ];

        var quill = new Quill('#newsContent', {
          modules: {
            imageResize: {},
            toolbar: toolbarOptions,
          },
          theme: 'snow'
        });

        /*datwpicker*/
        jQuery('.mydatepicker').datepicker();
        jQuery('#datepicker-autoclose').datepicker({
            autoclose: true,
            todayHighlight: true
        });
    });
</script>
<?php } else { ?><h1 style="text-align:center;vertical-align:middle;">No hay categorias creadas</h1><?php } ?>
