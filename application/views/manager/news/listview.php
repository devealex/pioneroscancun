<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="news-table" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Title</th>
                                <th>Category</th>
                                <th>Created</th>
                                <th>Published</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach ($news as $new) {
                                    ?>
                            <tr>
                                <td><?= $new['newsTitle']; ?></td>
                                <td><?= $new['category']; ?></td>
                                <td><?php $date = date_create($new['newsCreated']); echo date_format($date, 'Y-m-d'); ?></td>
                                <td><?= $new['newsPublished']; ?></td>
                                <td>
                                    <a href="/manager/noticias/edit/<?= $new['idNew'] ?>"><button type="button" class="btn btn-primary btn-sm"><i class="m-r-10 mdi mdi-table-edit"></i>Editar</button></a>
                                    <button type="button" class="btn btn-danger btn-sm del-new" data-newsavatar="<?= trim($new['newsAvatar']); ?>" data-idnew="<?= $new['idNew']; ?>"><i class="m-r-10 mdi mdi-delete"></i>Eliminar</button>
                                </td>
                            </tr>
                                    <?php
                                }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Title</th>
                                <th>Category</th>
                                <th>Created</th>
                                <th>Published</th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#news-table').DataTable();
    });
</script>
