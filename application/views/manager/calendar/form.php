<div class="row">
    <div class="col-md-12">
        <div class="card">
            <form class="form-horizontal" id="form-calendario">
                <div class="card-body">

                    <div class="form-group row col-md-6 col-sm-12 float-left">
                        <label for="jornadaRol" class="col-sm-12 text-left control-label col-form-label">Jornada de Torneo</label>
                        <div class="col-sm-12">
                            <input type="number" min="1" max="100" class="form-control" id="jornadaRol" name="jornadaRol" placeholder="Jornada de Torneo" value="<?php if(isset($calendario_item)){echo $calendario_item['jornadaRol'];} ?>">
                        </div>
                    </div>

                    <div class="form-group row col-md-6 col-sm-12 float-left">
                        <label for="playDay" class="col-sm-12 text-left control-label col-form-label">Fecha del evento</label>
                        <div class="col-sm-12">
                            <input type="date" class="form-control" id="playDay" name="playDay" placeholder="Fecha del evento" value="<?php if(isset($calendario_item)){echo $calendario_item['playDay'];} ?>">
                        </div>
                    </div>

                    <div class="form-group row col-md-12 col-sm-12 float-left">
                        <label for="fkGrupoRol" class="col-sm-12 text-left control-label col-form-label">Grupo</label>
                        <div class="col-sm-12">
                            <select class="form-control custom-select" name="fkGrupoRol" id="fkGrupoRol">
                                <?php
                                    foreach ( $grupos as $grupo ) {
                                        ?>
                                        <option value="<?= $grupo['idGrupo'] ?>" <?php if ( isset($calendario_item) && $calendario_item['fkGrupoRol'] == $grupo['idGrupo'] ) { echo 'selected'; } ?> ><?= $grupo['gurpoName'] ?></option>
                                        <?php
                                    }
                                ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group row col-md-6 col-sm-12 float-left">
                        <label class="col-sm-12 text-left control-label col-form-label">EQUIPO 1</label>
                        
                        <label for="fkTorneoRol" class="col-sm-12 text-left control-label col-form-label">Equipo</label>
                        <div class="col-sm-12 col-md-12">
                            <select class="form-control custom-select" name="fkTorneoRol" id="fkTorneoRol">
                                <?php
                                    foreach ( $torneos as $torneo ) {
                                        ?>
                                        <option value="<?= $torneo['idTorneo'] ?>" <?php if ( isset($calendario_item) && $calendario_item['fkTorneoRol'] == $torneo['idTorneo'] ) { echo 'selected'; } ?> ><?= $torneo['plantelName'] ?></option>
                                        <?php
                                    }
                                ?>
                            </select>
                        </div>

                        <?php 
                            $team1 = null;
                            if ( isset($calendario_item) && $calendario_item['isLocal'] == 0 ) { $team1 = 'visitaGol'; } else { $team1 = 'localGol'; }
                        ?>
                        <label id="lbG1" for="<?= $team1; ?>" class="col-sm-12 text-left control-label col-form-label">Goles</label>
                        <div class="form-group row col-md-12 col-sm-12">
                            <div class="col-sm-12">
                                <input type="number" min="0" max="50" class="form-control" id="inpG1" name="<?= $team1; ?>" placeholder="Goles" value="<?php if(isset($calendario_item)){echo $calendario_item[$team1];} ?>">
                            </div>
                        </div>

                        <label for="isLocal" class="col-sm-12 text-left control-label col-form-label">¿Es Local?</label>
                        <div class="col-sm-12 col-md-12">
                            <select class="form-control custom-select" name="isLocal" id="isLocal">
                                <option value="1" <?php if ( isset($calendario_item) && $calendario_item['isLocal'] == 1 ) { echo 'selected'; } ?> >SI</option>
                                <option value="0" <?php if ( isset($calendario_item) && $calendario_item['isLocal'] == 0 ) { echo 'selected'; } ?> >NO</option>
                            </select>
                        </div>

                    </div>

                    <div class="form-group row col-md-6 col-sm-12 float-left">
                        <label class="col-sm-12 text-left control-label col-form-label">EQUIPO 2</label>
                        
                        <label for="fkEquipoRol" class="col-sm-12 text-left control-label col-form-label">Equipo</label>
                        
                        <div class="col-sm-12 col-md-12">
                            <select class="form-control custom-select" name="fkEquipoRol" id="fkEquipoRol">
                                <?php
                                    foreach ( $equipos as $equipo ) {
                                        ?>
                                        <option value="<?= $equipo['idEquipo'] ?>" <?php if ( isset($calendario_item) && $calendario_item['fkEquipoRol'] == $equipo['idEquipo'] ) { echo 'selected'; } ?> ><?= $equipo['equipoName'] ?></option>
                                        <?php
                                    }
                                ?>
                            </select>
                        </div>
                        
                        <?php 
                            $team2 = null;
                            if ( isset($calendario_item) && $calendario_item['isLocal'] == 0 ) { $team2 = 'localGol'; } else { $team2 = 'visitaGol'; }
                        ?>
                        <label id="lbG2" for="<?= $team2; ?>" class="col-sm-12 text-left control-label col-form-label">Goles</label>
                        <div class="form-group row col-md-12 col-sm-12">
                            <div class="col-sm-12">
                                <input type="number" min="0" max="50" class="form-control" id="inpG2" name="<?= $team2; ?>" placeholder="Goles" value="<?php if(isset($calendario_item)){echo $calendario_item[$team2];} ?>">
                            </div>
                        </div>
                        
                    </div>

                    <input type="hidden" name="action" value="<?= $action ?>">
                    <input type="hidden" name="idCalendario" value="<?php if(isset($calendario_item)){echo $calendario_item['idCalendario'];} ?>">
               
                <div class="clear"></div>
                <div class="border-top">
                    <div class="card-body">
                        <button type="submit" class="btn btn-success">Guardar Fecha</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
