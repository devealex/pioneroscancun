<?php
if (isset($idTorneo) && $idTorneo != null) {
?>
    <div class="row">
        <div class="col-12">

            <div class="col-md-12 col-sm-12 lst-equipos">
                <?php
                $arrayIdEquipo = [];
                foreach ($equipos as $equipo) {
                    $isSelect = 0;
                    if (isset($calendario)) {
                        foreach ($calendario as $cl) {
                            if ($equipo['idEquipo'] == $cl['localRol']) {
                                $isSelect = 1;
                                array_push($arrayIdEquipo, $cl['localRol']);
                            }
                            if ($equipo['idEquipo'] == $cl['visitaRol']) {
                                $isSelect = 1;
                                array_push($arrayIdEquipo, $cl['visitaRol']);
                            }
                        }
                    }
                ?>
                    <div class="col-sm-6 col-md-2 col-6 float-left cont-equipo-calendario">
                        <article class="artEquipo">
                            <span data-isselect="<?= $isSelect ?>" data-name="<?= $equipo['equipoName'] ?>" data-id="<?= $equipo['idEquipo'] ?>" class="mask select-team-calendar <?php if ($isSelect == 1) {
                                                                                                                                                                                        echo 'selected';
                                                                                                                                                                                    } ?>">
                                <i class="mdi mdi-checkbox-marked-circle"></i>
                            </span>
                            <div class="cont-img">
                                <img src="/assets/img/equipos/<?= $equipo['equipoLogo'] ?>" alt="">
                            </div>
                            <div class="cont-name">
                                <?= $equipo['equipoName'] ?>
                            </div>
                        </article>
                    </div>
                <?php
                }
                ?>
            </div>

            <div class="card">
                <div class="card-body">
                    <div class="table-responsive">
                        <table id="calendario-table" class="table table-striped table-bordered">
                            <thead>
                                <tr>
                                    <th>Equipo Local</th>
                                    <th>Equipo Visitante</th>
                                    <th>Fecha y Hora</th>
                                    <th></th>
                                </tr>
                            </thead>
                            <thead>
                                <tr>
                                    <td class="cont-form-table">
                                        <input type="text" name="namLocal" readOnly>
                                    </td>
                                    <td class="cont-form-table">
                                        <input type="text" name="namVisit" readOnly>
                                    </td>
                                    <td class="cont-form-table">
                                        <input type="date" name="dateCal">
                                        <input type="time" name="timeCal">
                                    </td>
                                    <td class="cont-form-table">
                                        <form id="form-calendar">
                                            <input type="hidden" name="localRol">
                                            <input type="hidden" name="jornadaRol" value="<?= $jornadaRol ?>">
                                            <input type="hidden" name="fkTorneoRol" value="<?= $idTorneo ?>">
                                            <input type="hidden" name="visitaRol">
                                            <input type="hidden" name="playDay">
                                            <input type="hidden" name="playTime">
                                            <button type="submit" class="btn btn-success btn-sm">Crear</button>
                                            <button type="button" id="clear" class="btn btn-primary btn-sm">Limpiar</button>
                                        </form>
                                    </td>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                foreach ($calendario as $cl) {
                                ?>
                                    <tr>
                                        <td class="t-a-center">
                                            <?= $cl['locName'] ?>
                                            <input class="inp-goles" data-goles="<?= $cl['localGol'] ?>" data-goltype="localGol" data-id="<?= $cl['idCalendario'] ?>" type="number" min="0" max="50" value="<?= $cl['localGol'] ?>">
                                        </td>
                                        <td class="t-a-center">
                                            <?= $cl['visName'] ?>
                                            <input class="inp-goles" data-goles="<?= $cl['visitaGol'] ?>" data-goltype="visitaGol" data-id="<?= $cl['idCalendario'] ?>" type="number" min="0" max="50" value="<?= $cl['visitaGol'] ?>">
                                        </td>
                                        <td class="t-a-center">
                                            <input class="inp-date-calendar" data-type="date" data-id="<?= $cl['idCalendario'] ?>" type="date" value="<?= $cl['playDay'] ?>">
                                            <input class="inp-time-calendar" data-type="time" data-id="<?= $cl['idCalendario'] ?>" type="time" value="<?= $cl['playTime'] ?>">
                                        </td>
                                        <td>
                                            <button type="button" class="btn btn-danger btn-sm del-calendario" data-idcalendario="<?= $cl['idCalendario']; ?>"><i class="m-r-10 mdi mdi-delete"></i>Eliminar</button>
                                        </td>
                                    </tr>
                                <?php
                                }
                                ?>
                            </tbody>
                            <tfoot>
                                <tr>
                                    <th>Equipo Local</th>
                                    <th>Equipo Visitante</th>
                                    <th>Fecha</th>
                                    <th></th>
                                </tr>
                            </tfoot>
                        </table>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <script type="text/javascript">
        $(document).ready(function() {
            $('#calendario-table').DataTable();
        });
    </script>
<?php
} else {
    echo '<h1>SELECCIONE ALGUN TORNEO</h1>';
}
?>