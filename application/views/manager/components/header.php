<!DOCTYPE html>
<html lang="en" dir="ltr">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="icon" type="image/x-icon" href="/assets/img/favicon.ico">

        <title><?= ucwords(strtolower($section)); ?> | Manager</title>

        <!-- TEMPLATE CSS -->
        <link rel="stylesheet" href="/assets/manager/css/style.css">
        <link rel="stylesheet" href="/assets/manager/css/manager.min.css">

        <!-- Forms CSS -->
        <link href="/assets/manager/libs/jquery-steps/jquery.steps.css" rel="stylesheet">
        <link href="/assets/manager/libs/jquery-steps/steps.css" rel="stylesheet">
        <link rel="stylesheet" type="text/css" href="/assets/manager/libs/select2/dist/css/select2.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/manager/libs/jquery-minicolors/jquery.minicolors.css">
        <link rel="stylesheet" type="text/css" href="/assets/manager/libs/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
        <link rel="stylesheet" type="text/css" href="/assets/manager/libs/quill/dist/quill.snow.css">

        <!-- Charts CSS -->
        <link href="/assets/manager/libs/flot/css/float-chart.css" rel="stylesheet">

        <!-- Gallery CSS -->
        <link href="/assets/manager/libs/magnific-popup/dist/magnific-popup.css" rel="stylesheet">

        <!-- Elements CSS -->
        <link href="/assets/manager/libs/toastr/build/toastr.min.css" rel="stylesheet">

        <!-- Tables CSS -->
        <link rel="stylesheet" type="text/css" href="/assets/manager/libs/multicheck/multicheck.css">
        <link href="/assets/manager/libs/datatables.net-bs4/css/dataTables.bootstrap4.css" rel="stylesheet">

        <!-- Calendar CSS -->
        <link href="/assets/manager/libs/fullcalendar/dist/fullcalendar.min.css" rel="stylesheet" />
        <link href="/assets/manager/libs/calendar/calendar.css" rel="stylesheet" />

        <!-- All Jquery -->
        <script src="/assets/manager/libs/jquery/dist/jquery.min.js"></script>
        <!-- Manager JS -->
        <script src="/assets/manager/js/components.js"></script>

    </head>
    <body>

        <!-- PRE LOADER -->
        <div class="preloader">
            <div class="lds-ripple">
                <div class="lds-pos"></div>
                <div class="lds-pos"></div>
            </div>
        </div>
        <!-- FIN PRE LOADER -->

        <!-- MAIN WHRAPER -->
        <div id="main-wrapper">
            <!-- HEADER -->
            <header class="topbar" data-navbarbg="skin5">
                <nav class="navbar top-navbar navbar-expand-md navbar-dark" style="background:#a22b2b;">
                    <div class="navbar-header" data-logobg="skin5">
                        <!-- TOGGLE WHICH IS VISIBLE ON MOBILE ONLY -->
                        <a class="nav-toggler waves-effect waves-light d-block d-md-none" href="javascript:void(0)"><i class="ti-menu ti-close"></i></a>

                        <!-- LOGO -->
                        <a class="navbar-brand" href="/" target="_blank">
                            <b class="logo-icon">
                                <img src="/assets/img/pioneros-cancun-fc.png" alt="Pioneros" class="light-logo" />
                            </b>
                        </a>
                        <!-- END LOGO -->

                        <!-- TOGGLE WHICH IS VISIBLE ON MOBILE ONLY -->
                        <a class="topbartoggler d-block d-md-none waves-effect waves-light" href="javascript:void(0)" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation"><i class="ti-more"></i></a>
                    </div>
                    <div class="navbar-collapse collapse" id="navbarSupportedContent" data-navbarbg="skin5">
                        <!-- NAV ITEMS -->
                        <ul class="navbar-nav float-left mr-auto">
                            <li class="nav-item d-none d-md-block"><a class="nav-link sidebartoggler waves-effect waves-light" href="javascript:void(0)" data-sidebartype="mini-sidebar"><i class="mdi mdi-menu font-24"></i></a></li>
                        </ul>
                        <!-- Right side toggle and nav items -->
                        <ul class="navbar-nav float-right">

                            <!-- HEADER ELEMENTS -->
                            <?php include ($_SERVER['DOCUMENT_ROOT'].'/application/views/manager/includes/headerElements.php'); ?>
                            <!-- END HEADER ELEMENTS -->

                            <!-- USER PROFILE AND SEARCH -->
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle text-muted waves-effect waves-dark pro-pic" href="" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><img src="/assets/img/pioneros-cancun-fc.png" alt="user" class="rounded-circle" width="31"></a>
                                <div class="dropdown-menu dropdown-menu-right user-dd animated">
                                    <a class="dropdown-item" href="javascript:void(0)"><i class="ti-user m-r-5 m-l-5"></i> <?= $_SESSION['userName']; ?></a>
                                    <div class="dropdown-divider"></div>
                                    <a class="dropdown-item" href="/manager/login/logout"><i class="fa fa-power-off m-r-5 m-l-5"></i> Logout</a>
                                    <div class="dropdown-divider"></div>
                                </div>
                            </li>
                            <!-- END USER PROFILE AND SEARCH -->

                        </ul>
                    </div>
                </nav>
            </header>
            <!-- END HEADER -->

            <!-- LEFT SIDEBAR -->
            <aside class="left-sidebar" data-sidebarbg="skin5">
                <!-- Sidebar scroll-->
                <div class="scroll-sidebar">
                    <!-- Sidebar navigation-->
                    <nav class="sidebar-nav">
                        <ul id="sidebarnav" class="p-t-30">
                            <?php
                            if ($userType == 1) {
                                ?>
                                <li class="sidebar-item <?php if (strtolower($section) == 'noticias') echo 'selected'; ?>"> <a class="sidebar-link waves-effect waves-dark sidebar-link <?php if (strtolower($section) == 'noticias') echo 'active'; ?>" href="/manager/noticias/listview" aria-expanded="false"><i class="mdi mdi-newspaper"></i><span class="hide-menu">Noticias</span></a></li>
                                <li class="sidebar-item <?php if (strtolower($section) == 'planteles') echo 'selected'; ?>"> <a class="sidebar-link waves-effect waves-dark sidebar-link <?php if (strtolower($section) == 'planteles') echo 'active'; ?>" href="/manager/planteles/listview" aria-expanded="false"><i class="mdi mdi-school"></i><span class="hide-menu">Planteles</span></a></li>
                                <li class="sidebar-item <?php if (strtolower($section) == 'videos') echo 'selected'; ?>"> <a class="sidebar-link waves-effect waves-dark sidebar-link <?php if (strtolower($section) == 'videos') echo 'active'; ?>" href="/manager/videos/listview" aria-expanded="false"><i class="mdi mdi-video"></i><span class="hide-menu">Videos</span></a></li>
                                <li class="sidebar-item <?php if (strtolower($section) == 'galerias') echo 'selected'; ?>"> <a class="sidebar-link waves-effect waves-dark sidebar-link <?php if (strtolower($section) == 'galerias') echo 'active'; ?>" href="/manager/galerias/listview" aria-expanded="false"><i class="mdi mdi-image-area"></i><span class="hide-menu">Galerias</span></a></li>
                                <li class="sidebar-item <?php if (strtolower($section) == 'palmares') echo 'selected'; ?>"> <a class="sidebar-link waves-effect waves-dark sidebar-link <?php if (strtolower($section) == 'palmares') echo 'active'; ?>" href="/manager/palmares/listview" aria-expanded="false"><i class="mdi mdi-flower"></i><span class="hide-menu">Palmares</span></a></li>
                                <li class="sidebar-item <?php if (strtolower($section) == 'escuelas') echo 'selected'; ?>"> <a class="sidebar-link waves-effect waves-dark sidebar-link <?php if (strtolower($section) == 'escuelas') echo 'active'; ?>" href="/manager/escuelas/listview" aria-expanded="false"><i class="mdi mdi-school"></i><span class="hide-menu">Escuelas</span></a></li>
                                <li class="sidebar-item <?php if (strtolower($section) == 'torneos') echo 'selected'; ?>"> <a class="sidebar-link waves-effect waves-dark sidebar-link <?php if (strtolower($section) == 'torneos') echo 'active'; ?>" href="/manager/torneos/listview" aria-expanded="false"><i class="mdi mdi-clock-start"></i><span class="hide-menu">Torneos</span></a></li>
                                <!--<li class="sidebar-item <?php if (strtolower($section) == 'calendario') echo 'selected'; ?>"> <a class="sidebar-link waves-effect waves-dark sidebar-link <?php if (strtolower($section) == 'calendario') echo 'active'; ?>" href="/manager/calendario/listview" aria-expanded="false"><i class="mdi mdi-calendar"></i><span class="hide-menu">Calendario</span></a></li>-->
                                <!--<li class="sidebar-item <?php if (strtolower($section) == 'estadisticas') echo 'selected'; ?>"> <a class="sidebar-link waves-effect waves-dark sidebar-link <?php if (strtolower($section) == 'estadisticas') echo 'active'; ?>" href="/manager/estadisticas/listview" aria-expanded="false"><i class="mdi mdi-chart-bar"></i><span class="hide-menu">Estadisticas</span></a></li>-->
                                <li class="sidebar-item <?php if (strtolower($section) == 'usuarios') echo 'selected'; ?>"> <a class="sidebar-link waves-effect waves-dark sidebar-link <?php if (strtolower($section) == 'usuarios') echo 'active'; ?>" href="/manager/usuarios/listview" aria-expanded="false"><i class="mdi mdi-account-box"></i><span class="hide-menu">Usuarios</span></a></li>
                                <li class="sidebar-item <?php if (strtolower($section) == 'patrocinadores') echo 'selected'; ?>"> <a class="sidebar-link waves-effect waves-dark sidebar-link <?php if (strtolower($section) == 'patrocinadores') echo 'active'; ?>" href="/manager/patrocinadores/listview" aria-expanded="false"><i class="mdi mdi-account-multiple"></i><span class="hide-menu">Patrocinadores</span></a></li>
                                <li class="sidebar-item <?php if (strtolower($section) == 'suscripciones') echo 'selected'; ?>"> <a class="sidebar-link waves-effect waves-dark sidebar-link <?php if (strtolower($section) == 'suscripciones') echo 'active'; ?>" href="/manager/suscripciones/listview" aria-expanded="false"><i class="mdi mdi-note-text"></i><span class="hide-menu">Suscripciones</span></a></li>
                                <?php
                            } else if ($userType == 2) {
                                ?>
                                <li class="sidebar-item <?php if (strtolower($section) == 'noticias') echo 'selected'; ?>"> <a class="sidebar-link waves-effect waves-dark sidebar-link <?php if (strtolower($section) == 'noticias') echo 'active'; ?>" href="/manager/noticias/listview" aria-expanded="false"><i class="mdi mdi-newspaper"></i><span class="hide-menu">Noticias</span></a></li>
                                <li class="sidebar-item <?php if (strtolower($section) == 'videos') echo 'selected'; ?>"> <a class="sidebar-link waves-effect waves-dark sidebar-link <?php if (strtolower($section) == 'videos') echo 'active'; ?>" href="/manager/videos/listview" aria-expanded="false"><i class="mdi mdi-video"></i><span class="hide-menu">Videos</span></a></li>
                                <li class="sidebar-item <?php if (strtolower($section) == 'galerias') echo 'selected'; ?>"> <a class="sidebar-link waves-effect waves-dark sidebar-link <?php if (strtolower($section) == 'galerias') echo 'active'; ?>" href="/manager/galerias/listview" aria-expanded="false"><i class="mdi mdi-image-area"></i><span class="hide-menu">Galerias</span></a></li>
                                <?php
                            } else if ($userType == 3) {
                                ?>
                                <li class="sidebar-item <?php if (strtolower($section) == 'escuelas') echo 'selected'; ?>"> <a class="sidebar-link waves-effect waves-dark sidebar-link <?php if (strtolower($section) == 'escuelas') echo 'active'; ?>" href="/manager/escuelas/listview" aria-expanded="false"><i class="mdi mdi-school"></i><span class="hide-menu">Escuelas</span></a></li>
                                <?php
                            }
                            ?>
                    </nav>
                    <!-- End Sidebar navigation -->
                </div>
                <!-- End Sidebar scroll-->
            </aside>
            <!-- END LEFT SIDEBAR -->

            <!-- PAGE WRAPPER -->
            <div class="page-wrapper">

                <!-- TITLE SITE -->
                <div class="page-breadcrumb">
                   <div class="row">
                       <div class="col-12 d-flex no-block align-items-center">
                           <h4 class="page-title"><?= $section; ?></h4>
                           <?php include ($_SERVER['DOCUMENT_ROOT'].'/application/views/manager/includes/buttons.php'); ?>
                       </div>
                   </div>
                   <?php include ($_SERVER['DOCUMENT_ROOT'].'/application/views/manager/includes/breadcrumbs.php'); ?>
               </div>
               <!-- END TITLE SITE -->

               <div class="container-fluid">
