                    <!-- FOOTER -->
                    <footer class="footer text-center">
                        <!-- Text -->
                    </footer>
                    <!-- END FOOTER -->
                </div>
                <!-- END CONTAINER FLUID -->
            </div>
            <!-- END PAGE WRAPPER -->
        </div>
        <!-- END MAIN WHRAPER -->

        <!-- Bootstrap tether Core JavaScript -->
        <script src="/assets/manager/libs/popper.js/dist/umd/popper.min.js"></script>
        <script src="/assets/manager/libs/bootstrap/dist/js/bootstrap.min.js"></script>
        <script src="/assets/manager/libs/perfect-scrollbar/dist/perfect-scrollbar.jquery.min.js"></script>
        <script src="/assets/manager/libs/sparkline/sparkline.js"></script>
        <!--Wave Effects -->
        <script src="/assets/manager/js/waves.js"></script>
        <!--Menu sidebar -->
        <script src="/assets/manager/js/sidebarmenu.js"></script>
        <!--Custom JavaScript -->
        <script src="/assets/manager/js/custom.min.js"></script>

        <!-- Forms -->
        <script src="/assets/manager/libs/jquery-steps/build/jquery.steps.min.js"></script>
        <script src="/assets/manager/libs/jquery-validation/dist/jquery.validate.min.js"></script>
        <script src="/assets/manager/libs/inputmask/dist/min/jquery.inputmask.bundle.min.js"></script>
        <script src="/assets/manager/js/pages/mask/mask.init.js"></script>
        <script src="/assets/manager/libs/select2/dist/js/select2.full.min.js"></script>
        <script src="/assets/manager/libs/select2/dist/js/select2.min.js"></script>
        <script src="/assets/manager/libs/jquery-asColor/dist/jquery-asColor.min.js"></script>
        <script src="/assets/manager/libs/jquery-asGradient/dist/jquery-asGradient.js"></script>
        <script src="/assets/manager/libs/jquery-asColorPicker/dist/jquery-asColorPicker.min.js"></script>
        <script src="/assets/manager/libs/jquery-minicolors/jquery.minicolors.min.js"></script>
        <script src="/assets/manager/libs/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
        <script src="/assets/manager/libs/quill/dist/quill.min.js"></script>
        <script src="/assets/manager/libs/quill/dist/image-resize.min.js"></script>

        <!-- Charts -->
        <script src="/assets/manager/libs/chart/matrix.interface.js"></script>
        <script src="/assets/manager/libs/chart/excanvas.min.js"></script>
        <script src="/assets/manager/libs/flot/jquery.flot.js"></script>
        <script src="/assets/manager/libs/flot/jquery.flot.pie.js"></script>
        <script src="/assets/manager/libs/flot/jquery.flot.time.js"></script>
        <script src="/assets/manager/libs/flot/jquery.flot.stack.js"></script>
        <script src="/assets/manager/libs/flot/jquery.flot.crosshair.js"></script>
        <script src="/assets/manager/libs/chart/jquery.peity.min.js"></script>
        <script src="/assets/manager/libs/chart/matrix.charts.js"></script>
        <script src="/assets/manager/libs/chart/jquery.flot.pie.min.js"></script>
        <script src="/assets/manager/libs/flot.tooltip/js/jquery.flot.tooltip.min.js"></script>
        <script src="/assets/manager/libs/chart/turning-series.js"></script>
        <script src="/assets/manager/js/pages/chart/chart-page-init.js"></script>

        <!-- Gallery -->
        <script src="/assets/manager/libs/magnific-popup/dist/jquery.magnific-popup.min.js"></script>
        <script src="/assets/manager/libs/magnific-popup/meg.init.js"></script>

        <!-- Elements -->
        <script src="/assets/manager/libs/toastr/build/toastr.min.js"></script>

        <!-- Tables -->
        <script src="/assets/manager/libs/multicheck/datatable-checkbox-init.js"></script>
        <script src="/assets/manager/libs/multicheck/jquery.multicheck.js"></script>
        <script src="/assets/manager/libs/datatables/datatables.min.js"></script>

        <!-- Calendar -->
        <script src="/assets/manager/libs/moment/min/moment.min.js"></script>
        <script src="/assets/manager/libs/fullcalendar/dist/fullcalendar.min.js"></script>
        <script src="/assets/manager/js/pages/calendar/cal-init.js"></script>

    </body>
</html>
