<div class="row">
    <div class="col-md-12">
        <div class="card">
            <form class="form-horizontal" id="form-palmares">
                <div class="card-body">
                    <div class="form-group row col-md-6 col-sm-12 float-left">
                        <label for="cupName" class="col-sm-12 text-left control-label col-form-label">Nombre de Campeonato</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="cupName" name="cupName" placeholder="Nombre de Campeonato" value="<?php if(isset($campeonato_item)){echo $campeonato_item['cupName'];} ?>">
                        </div>
                    </div>
                    <div class="form-group row col-md-6 col-sm-12 float-left">
                        <label for="championshipDate" class="col-sm-12 text-left control-label col-form-label">Fecha del Campeonato</label>
                        <div class="col-sm-12">
                            <input type="number" min="1900" max="<?php echo date('Y'); ?>" class="form-control" id="championshipDate" name="championshipDate" placeholder="Fecha del Campeonato" value="<?php if(isset($campeonato_item)){echo $campeonato_item['championshipDate'];} ?>">
                        </div>
                    </div>
                    <div class="form-group row col-sm-12">
                        <label class="col-sm-12 text-left control-label col-form-label">Imagen Copa (400 x 400)</label>
                        <div class="col-sm-12 div-preview form-control">
                            <input type="file" class="form-control file-img-preview" name="cupAvatar" id="cupAvatar" accept="image/png">
                            <img src="/assets/img/palmares/<?php if(isset($campeonato_item) && $campeonato_item['cupAvatar'] != ''){echo $campeonato_item['cupAvatar'];}else{echo "default.png";} ?>" class="img-preview">
                        </div>
                    </div>
                    <div class="form-group row col-sm-12 float-left">
                        <label for="championshipDescription" class="col-sm-12 text-left control-label col-form-label">Descripción del Campeonato</label>
                        <div class="col-sm-12">
                            <textarea name="championshipDescription" class="form-control" rows="8" cols="80"><?php if(isset($campeonato_item)){echo $campeonato_item['championshipDescription'];} ?></textarea>
                        </div>
                    </div>
                    <input type="hidden" name="action" value="<?= $action ?>">
                    <input type="hidden" name="nameAvatar" value="<?php if (isset($campeonato_item)) {echo $campeonato_item['cupAvatar'];} ?>">
                    <input type="hidden" name="idChampionship" value="<?php if(isset($campeonato_item)){echo $campeonato_item['idChampionship'];} ?>">
                <div class="clear"></div>
                <div class="border-top">
                    <div class="card-body">
                        <button type="submit" class="btn btn-success">Guardar Campeonato</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
