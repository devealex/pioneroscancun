<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="palmares-table" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Nombre de Campeonato</th>
                                <th>Año de Campeonato</th>
                                <th>Fecha de Registro</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($palmares as $campeonato) {
                                ?>
                                <tr>
                                    <td><?= $campeonato['cupName'] ?></td>
                                    <td><?= $campeonato['championshipDate'] ?></td>
                                    <td><?php $date = date_create($campeonato['championshipRegister']); echo date_format($date, 'Y-m-d'); ?></td>
                                    <td>
                                        <a href="/manager/palmares/edit/<?= $campeonato['idChampionship'] ?>"><button type="button" class="btn btn-primary btn-sm"><i class="m-r-10 mdi mdi-table-edit"></i>Editar</button></a>
                                        <button type="button" class="btn btn-danger btn-sm del-palmares" data-idchampionship="<?= $campeonato['idChampionship']; ?>" data-cupavatar="<?= trim($campeonato['cupAvatar']) ?>"><i class="m-r-10 mdi mdi-delete"></i>Eliminar</button>
                                    </td>
                                </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Nombre de Campeonato</th>
                                <th>Año de Campeonato</th>
                                <th>Fecha de Registro</th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#palmares-table').DataTable();
    });
</script>
