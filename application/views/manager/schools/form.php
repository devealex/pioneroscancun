<div class="row">
    <div class="col-md-12">
        <div class="card">
            <form class="form-horizontal" id="form-school">
                <div class="card-body">
                    <div class="form-group row col-md-6 col-sm-12 float-left">
                        <label for="nameSchool" class="col-sm-12 text-left control-label col-form-label">Escuela</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="nameSchool" name="nameSchool" placeholder="Escuela" value="<?php if(isset($school_item)){echo $school_item['nameSchool'];} ?>">
                        </div>
                    </div>
                    <div class="form-group row col-md-6 col-sm-12 float-left">
                        <label for="representative" class="col-sm-12 text-left control-label col-form-label">Representante</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="representative" name="representative" placeholder="Representante" value="<?php if(isset($school_item)){echo $school_item['representative'];} ?>">
                        </div>
                    </div>
                    <div class="form-group row col-sm-12 col-md-6 float-left">
                        <label for="contactNumber" class="col-sm-12 text-left control-label col-form-label">Número de contacto</label>
                        <div class="col-sm-12">
                            <input type="number" class="form-control" id="contactNumber" name="contactNumber" placeholder="Número de contacto" value="<?php if(isset($school_item)){echo $school_item['contactNumber'];} ?>">
                        </div>
                    </div>
                    <div class="form-group row col-sm-12 col-md-6 float-left">
                        <label for="email" class="col-sm-12 text-left control-label col-form-label">Correo Electrónico</label>
                        <div class="col-sm-12">
                            <input type="email" class="form-control" id="email" name="email" placeholder="Correo Electrónico" value="<?php if(isset($school_item)){echo $school_item['email'];} ?>">
                        </div>
                    </div>
                    <div class="form-group row col-sm-12">
                        <label for="representative" class="col-sm-12 text-left control-label col-form-label">Dirección</label>
                        <div class="col-sm-12">
                            <textarea name="address" class="form-control" rows="2" placeholder="Dirección"><?php if(isset($school_item)){echo $school_item['address'];} ?></textarea>
                        </div>
                    </div>
                    <div class="form-group row col-sm-12 col-md-6 float-left">
                        <label for="facebook" class="col-sm-12 text-left control-label col-form-label">Facebook</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="facebook" name="facebook" placeholder="Facebook" value="<?php if(isset($school_item)){echo $school_item['facebook'];} ?>">
                        </div>
                    </div>
                    <div class="form-group row col-sm-12 col-md-6 float-left">
                        <label for="twitter" class="col-sm-12 text-left control-label col-form-label">Twitter</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="twitter" name="twitter" placeholder="Twitter" value="<?php if(isset($school_item)){echo $school_item['twitter'];} ?>">
                        </div>
                    </div>
                    <div class="form-group row col-sm-12 col-md-6 float-left">
                        <label for="instagram" class="col-sm-12 text-left control-label col-form-label">Instagram</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="instagram" name="instagram" placeholder="Instagram" value="<?php if(isset($school_item)){echo $school_item['instagram'];} ?>">
                        </div>
                    </div>
                    <div class="form-group row col-sm-12 col-md-6 float-left">
                        <label for="youtube" class="col-sm-12 text-left control-label col-form-label">Youtube</label>
                        <div class="col-sm-12">
                            <input type="text" class="form-control" id="youtube" name="youtube" placeholder="Youtube" value="<?php if(isset($school_item)){echo $school_item['youtube'];} ?>">
                        </div>
                    </div>
                    <input type="hidden" name="action" value="<?= $action ?>">
                    <input type="hidden" name="idSchool" value="<?php if(isset($school_item)){echo $school_item['idSchool'];} ?>">
                </div>
                <div class="clear"></div>
                <div class="border-top">
                    <div class="card-body">
                        <button type="submit" class="btn btn-success">Guardar Escuela</button>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
