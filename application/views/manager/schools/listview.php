<div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="schools-table" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Estado</th>
                                <th>Escuela</th>
                                <th>Representante</th>
                                <th>Numero Contacto</th>
                                <th>Fecha de Registro</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                                foreach ($schools as $school) {
                                    ?>
                            <tr>
                                <td class="td-center">
                                    <input type="checkbox" name="state" id="state-<?= $school['idSchool']; ?>" data-idschool="<?= $school['idSchool']; ?>" class="chxbx-table" value="<?= $school['state']; ?>" <?php if($school['state'] == 1){echo 'checked';} ?>>
                                    <label for="state-<?= $school['idSchool']; ?>" class="lbl-to-chxbx-table"></label>
                                </td>
                                <td><?= $school['nameSchool']; ?></td>
                                <td><?= $school['representative']; ?></td>
                                <td><?= $school['contactNumber']; ?></td>
                                <td><?php $date = date_create($school['registrationDate']); echo date_format($date, 'Y-m-d'); ?></td>
                                <td>
                                    <a href="/manager/escuelas/edit/<?= $school['idSchool'] ?>"><button type="button" class="btn btn-primary btn-sm" title="Editar"><i class="m-r-10 mdi mdi-table-edit"></i>Editar</button></a>
                                    <button type="button" class="btn btn-danger btn-sm del-school" data-idschool="<?= $school['idSchool']; ?>" title="Eliminar"><i class="m-r-10 mdi mdi-delete"></i>Eliminar</button>
                                </td>
                            </tr>
                                    <?php
                                }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Estado</th>
                                <th>Escuela</th>
                                <th>Representante</th>
                                <th>Numero Contacto</th>
                                <th>Fecha de Registro</th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#schools-table').DataTable();
    });
</script>
