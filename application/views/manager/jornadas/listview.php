<?php
if (isset($idTorneo) && $idTorneo != null) {
    ?>
    <div class="row">
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <div class="table-responsive">
                    <table id="jornadas-table" class="table table-striped table-bordered">
                        <thead>
                            <tr>
                                <th>Jornada</th>
                                <th>Torneo</th>
                                <th></th>
                            </tr>
                        </thead>
                        <tbody>
                            <?php
                            foreach ($jornadas as $jornada) {
                                ?>
                                <tr>
                                    <td><?= $jornada['jornada'] ?></td>
                                    <td><?= $jornada['torneo'] ?></td>
                                    <td>
                                        <a href="/manager/calendario/listview?t=<?= $idTorneo ?>&j=<?= $jornada['jornada'] ?>"><button type="button" class="btn btn-success btn-sm"><i class="m-r-10 mdi mdi-calendar"></i>Calendario</button></a>
                                    </td>
                                    <!--<td>
                                        <a href="/manager/jornadas/edit/<?= $jornada['idCalendario'] ?>"><button type="button" class="btn btn-primary btn-sm"><i class="m-r-10 mdi mdi-table-edit"></i>Editar</button></a>
                                        <button type="button" class="btn btn-danger btn-sm del-jornadas" data-idcalendario="<?= $jornada['idCalendario']; ?>"><i class="m-r-10 mdi mdi-delete"></i>Eliminar</button>
                                    </td>-->
                                </tr>
                                <?php
                            }
                            ?>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Jornada</th>
                                <th>Torneo</th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
    $(document).ready(function(){
        $('#jornadas-table').DataTable();
    });
</script>
    <?php
} else {
    echo '<h1>SELECCIONE ALGUN TORNEO</h1>';
}
?>