<section class="estadio">
    <div class="columns is-gapless">

        <div class="column is-three-fifths">
            <div class="flexslider">
              <ul class="slides">
                <li>
                  <img src="/assets/img/estadios/estadio-andres-quintanaroo.jpg" />
                </li>
                <li>
                  <img src="/assets/img/estadios/estadio-andres-quintanaroo-1.jpg" />
                </li>
              </ul>
            </div>

            <script type="text/javascript">
            $('.flexslider').flexslider({
                animation: "slide"
            });
            </script>
        </div>
        
        <div class="column">
            <div class="introduccion">
                <h1 class="title is-title-red is-size-4">ESTADIO ANDRÉS QUINTANA ROO</h1>
                <p>El Estadio Olímpico Andrés Quintana Roo es un estadio de futbol con pista de atletismo situado en Cancún, Quintana Roo, México. Actualmente es una de las sedes de los Pioneros de Cancún en la Liga Premier. </p>
                
                <div class="contacto is-clearfix">
                    <i class="fas fa-mobile-alt"></i>
                    <span>848 2725</span>
                </div>
                <div class="contacto is-clearfix">
                    <i class="fas fa-at"></i>
                    <span>andresquintanaroo@pioneroscancunfc.com.mx</span>
                </div>
                <div class="contacto is-clearfix">
                    <i class="fas fa-directions"></i>
                    <span>Av. Mayapan Mz. 4, 21, 77500 Cancún, Q.R.</span>
                </div>

            </div>
        </div>

    </div>

    <div class="container">
        <div class="columns">
            <div class="column is-four-fifths texto-historia">

                <h2 class="title is-title-red is-size-5">HISTORIA</h2>
                
                <p>El Estadio Andrés Quintana Roo fue un inmueble que se construyó en el año de 1984  en lo que podría llamarse su primera etapa, en el año de 1987 comenzó a ser la casa de los Pioneros de Cancún.<br>
                Para el 2009 el “Andrés Quintana Roo” fue remodelado esto tras la llegada del Atlante y la Primera División Profesional a Cancún, teniendo la capacidad de más de 17 mil espectadores.</p>

                <p>Actualmente es la sede oficial de los Pioneros de Cancún quienes disputan el torneo de la Liga Premier Serie A.</p>

                <p>En el 2019 fue sede del primer Torneo Internacional de la Liga Premier que tuvo la participación de equipos como Paranaense, Salamanca, Red Star y el Antigua de Guatemala.</p>

            </div>
        </div>
    </div>

    <div class="columns is-gapless">
        <div class="column">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3721.042421957626!2d-86.84036268543726!3d21.150709985932473!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8f4c2bf234dea56d%3A0x894e7629f516837e!2sOl%C3%ADmpico+Andr%C3%A9s+Quintana+Roo!5e0!3m2!1ses!2smx!4v1563170630213!5m2!1ses!2smx" width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
    </div>
</section>
