<section class="container is-first-margin">
    <div class="columns is-marginless">
        <div class="column is-full is-bread">
            <h1 class="title is-title-red is-size-4">ESTADÍSTICAS</h1>

            <?php
            if (isset($torneos) && $torneos != null) {
                $limit = count($torneos) -1;
                $get = '';
                if (isset($_GET['torneo']) && $_GET['torneo'] != null) {
                    $get = trim($_GET['torneo']);
                }
                foreach ($torneos as $i => $torneo) {
                    $item = '<a href="/estadisticas?torneo=' . trim($torneo['torneourl']) . '">' . trim($torneo['torneo']) . '</a>';
                    if (trim($torneo['torneourl']) == $get) {
                        $item = '<span>' . trim($torneo['torneo']) . '</span>';
                    }
                    if ($i != $limit) {
                        $item = $item . '<span>|</span>';
                    }
                    echo $item;
                }
            }
            ?>
        </div>
    </div>

    <div class="columns is-multiline is-stadistics">

        <div class="column is-two-thirds">
            <div class="is-table">
                <strong>TABLA GENERAL</strong>
                <?php
                if (isset($estadisticas) && $estadisticas != null) {
                ?>
                <table class="estadisticas-tabla">
                    <tr class="is-options">
                        <td></td>
                        <td></td>
                        <td></td>
                        <td>JJ</td>
                        <td>JG</td>
                        <td>JE</td>
                        <td>JP</td>
                        <td>GF</td>
                        <td>GC</td>
                        <td>DG</td>
                        <td>PTS</td>
                    </tr>
                <?php
                    foreach ($estadisticas as $i => $estadistica) {
                ?>
                    <tr class="<?php if (stristr(strtolower($estadistica['equipoName']), 'pionero') == true) {echo 'is-own';} ?>">
                        <td data-title="Posición" class="is-pos"><?= $i + 1 ?></td>
                        <td class="is-image"><img src="/assets/img/equipos/<?= trim($estadistica['equipoLogo']) ?>"></td>
                        <td class="is-name"><?= $estadistica['equipoName'] ?></td>
                        <td data-title="JJ" ><?= $estadistica['jj'] ?></td>
                        <td data-title="JG" ><?= $estadistica['jg'] ?></td>
                        <td data-title="JE" ><?= $estadistica['je'] ?></td>
                        <td data-title="JP" ><?= $estadistica['jp'] ?></td>
                        <td data-title="GF" ><?= $estadistica['gf'] ?></td>
                        <td data-title="GC" ><?= $estadistica['gc'] ?></td>
                        <td data-title="DG" ><?= $estadistica['dif'] ?></td>
                        <td data-title="PTS" ><?= $estadistica['pts'] ?></td>
                    </tr>
                <?php   
                    }
                ?>
                </table>
                <?php
                }
                ?>
            </div>
        </div>

        <div class="column is-one-third">
            <div class="is-table">
                <strong>TABLA DE GOLEO</strong>

                <table class="is-goals">
                    <?php
                    if (isset($goleo) && $goleo != null) {
                        foreach($goleo as $i => $goleador) {
                            if ( ($goleador['nombre'] != '' || $goleador['nombre'] != null) && $goleador['goles'] > 0) {
                    ?>
                    <tr>
                        <td class="is-pos"><?= $i + 1 ?></td>
                        <td><?= $goleador['nombre'] ?><small><?= $goleador['equipo'] ?></small></td>
                        <td><?= $goleador['goles'] ?></td>
                    </tr>
                    <?php
                            }
                        }
                    } else {
                        echo "<br><br><br><br><br><br><br><br><br><br>";
                    }
                    ?>
                </table>
            </div>
        </div>

    </div>

    <div class="columns is-multiline is-calendar">
        <?php
        if (isset($calendario) && $calendario != null) {
            foreach ($calendario as $i => $cldr) {
                //$date = date_format(date_create($cldr['playDay']), 'd/m/Y');
                $date = ['local' => '&nbsp;', 'visita' => '&nbsp;'];
                if ($cldr['playDay'] <= date('Y-m-d')) {
                    $date['local'] = $cldr['localGol'];
                    $date['visita'] = $cldr['visitaGol'];
                }
        ?>
        <div class="column is-one-quarter">
            <div class="is-card">
                <div class="columns is-multiline">
                    <div class="column is-full"><strong class="is-rol">Jornada <?= $cldr['jornadaRol'] ?></strong></div>
                    <div class="column is-one-third">
                        <img src="/assets/img/equipos/<?= $cldr['locLogo'] ?>">
                        <strong><?= $date['local'] ?></strong>
                    </div>
                    <div class="column is-one-third"><small>vs</small></div>
                    <div class="column is-one-third">
                        <img src="/assets/img/equipos/<?= $cldr['visLogo'] ?>">
                        <strong><?= $date['visita'] ?></strong>
                    </div>
                    <div class="column is-full">
                        <span><?= humanDateFormat($cldr['playDay']) ?></br><?= strftime("%I:%M %p", strtotime($cldr['playTime'])); ?></span>
                    </div>
                </div>         
            </div>
        </div>
        <?php
            }
        }
        ?>

        <div class="column is-full">
            <a href="/calendario?torneo=<?= isset($_GET['torneo']) ? $_GET['torneo'] : ''; ?>" class="is-button-group">Todo los partidos</a>
        </div>

    </div>

</section>
