<section class="estadio">
    <div class="columns is-gapless">

        <div class="column is-three-fifths">
            <div class="flexslider">
              <ul class="slides">
                <li>
                  <img src="/assets/img/estadios/estadio-cancun-86.jpg" />
                </li>
                <li>
                  <img src="/assets/img/estadios/estadio-cancun-86-1.jpg" />
                </li>
              </ul>
            </div>

            <script type="text/javascript">
            $('.flexslider').flexslider({
                animation: "slide"
            });
            </script>
        </div>

        <div class="column">
            <div class="introduccion">
                <h1 class="title is-title-red is-size-4">ESTADIO CANCÚN 86</h1>
                <p>El estadio Cancún 86 se encuentra ubicado en la prolongación Av. Tulum muy cerca del centro de la ciudad de Cancún. Fue construido en la administración municipal 1984 – 1987 siendo así la casa de los Pioneros de Cancún.</p>

                <div class="contacto is-clearfix">
                    <i class="fas fa-mobile-alt"></i>
                    <span>888 2976</span>
                </div>
                <div class="contacto is-clearfix">
                    <i class="fas fa-at"></i>
                    <span>cancun86@pioneroscancunfc.com.mx</span>
                </div>
                <div class="contacto is-clearfix">
                    <i class="fas fa-directions"></i>
                    <span style="float:left;">Prolongación Av. Tulum s/n Manzana 89,  No Disponible,  Tequila,  Quintana Roo,  C.P. 77500</span>
                </div>
            </div>
        </div>

    </div>

    <div class="container">
        <div class="columns">
            <div class="column is-four-fifths texto-historia">

                <h2 class="title is-title-red is-size-5">HISTORIA</h2>
                
                <p>El Estadio “Cancún 86” casa de los Pioneros de Cancún, es uno de los inmuebles icónicos de Cancún se encuentra ubicado en la prolongación Av. Tulum. Este escenario fue construido en entre los años 1984– 1987, los encargados fueron el ingeniero Rafael Lara y el arquitecto Rodolfo Páez, finalizando su construcción cuando se jugaba la Copa del Mundo México 86, de ahí su nombre. El aforo es de seis mil aficionados a su máxima capacidad.</p>

                <p>El estadio ha recibido la visita de la Selección Mayor Mexicana, así como diferentes equipos de la Primera División de México, además de otros equipos internacionales.</p>

                <p>En el 2019 fue sede del primer Torneo Internacional de la Liga Premier que tuvo la participación de equipos como Paranaense, Salamanca, Red Star y el Antigua de Guatemala.</p>

            </div>
        </div>
    </div>

    <div class="columns is-gapless">
        <div class="column">
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3720.257668786675!2d-86.83292168506475!3d21.181920685915664!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x8f4c2c186a697917%3A0x6d269e9dcc9e096c!2sEstadio+Canc%C3%BAn+86!5e0!3m2!1ses!2smx!4v1563169585222!5m2!1ses!2smx" width="100%" height="300" frameborder="0" style="border:0" allowfullscreen></iframe>
        </div>
    </div>
</section>
