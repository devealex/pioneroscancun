<section class="palmares">
    <div class="container">
        <?php
        if (count($page['palmares']) == 0) {
            ?>
            <h1 style="text-align:center;">No hay campeonatos registrados</h1>
            <?php
        } else {
            $i = 1;
            foreach ($page['palmares'] as $campeonato) {
                if (count($page['palmares']) == 1) {
                    ?>
                    <div class="columns">
                        <div class="column is-one-quarter">
                            <div class="artc-palmares">
                                <figure>
                                    <img src="/assets/img/palmares/<?= $campeonato['cupAvatar'] ?>">
                                </figure>
                                <div class="info">
                                    <h1><?= $campeonato['cupName'] ?></h1>
                                    <h2><?= $campeonato['championshipDate'] ?></h2>
                                    <p><?= $campeonato['championshipDescription'] ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                } else if ($i == 1) {
                    ?>
                    <div class="columns">
                        <div class="column is-one-quarter">
                            <div class="artc-palmares">
                                <figure>
                                    <img src="/assets/img/palmares/<?= $campeonato['cupAvatar'] ?>">
                                </figure>
                                <div class="info">
                                    <h1><?= $campeonato['cupName'] ?></h1>
                                    <h2><?= $campeonato['championshipDate'] ?></h2>
                                    <p><?= $campeonato['championshipDescription'] ?></p>
                                </div>
                            </div>
                        </div>
                    <?php
                } else if ($i == 4 || $i == count($page['palmares'])) {
                    ?>
                        <div class="column is-one-quarter">
                            <div class="artc-palmares">
                                <figure>
                                    <img src="/assets/img/palmares/<?= $campeonato['cupAvatar'] ?>">
                                </figure>
                                <div class="info">
                                    <h1><?= $campeonato['cupName'] ?></h1>
                                    <h2><?= $campeonato['championshipDate'] ?></h2>
                                    <p><?= $campeonato['championshipDescription'] ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php
                    $i = 0;
                } else {
                    ?>
                    <div class="column is-one-quarter">
                        <div class="artc-palmares">
                            <figure>
                                <img src="/assets/img/palmares/<?= $campeonato['cupAvatar'] ?>">
                            </figure>
                            <div class="info">
                                <h1><?= $campeonato['cupName'] ?></h1>
                                <h2><?= $campeonato['championshipDate'] ?></h2>
                                <p><?= $campeonato['championshipDescription'] ?></p>
                            </div>
                        </div>
                    </div>
                    <?php
                }
                $i++;
            }
        }
        ?>
    </div>
</section>
