<section class="columns is-gapless grid-news">
	<div class="column is-half news" style="//background: url('/assets/img/news/<?php if (count($news) == 0 || $news[0]['newsAvatar'] == "") {
																					echo 'default.jpg';
																				} else {
																					echo trim($news[0]['newsAvatar']);
																				} ?>');">
		<div class="section">
			<h1>NOTICIAS</h1>
		</div>
		<div class="flexslider">
			<ul class="slides">
				<?php
				if (count($news) != 0) {
					$i = 1;
					foreach ($news as $new) {
						if ($i <= 5) {
							$categoryNew = $new['category'];
							$categoryNew = str_replace('á', 'a', $categoryNew);
							$categoryNew = str_replace('é', 'e', $categoryNew);
							$categoryNew = str_replace('í', 'i', $categoryNew);
							$categoryNew = str_replace('ó', 'o', $categoryNew);
							$categoryNew = str_replace('ú', 'u', $categoryNew);
				?>
							<li style="background: url('/assets/img/news/<?php if ($new['newsAvatar'] == "") {
																				echo 'default.jpg';
																			} else {
																				echo trim($new['newsAvatar']);
																			} ?>');">
								<a href="<?= $new['newsUrl'] ?>" class="mask">
									<div class="info">
										<h1><?php if (count($news) == 0) {
												echo '';
											} else {
												echo strtoupper($new['newsTitle']);
											} ?></h1>
										<h2><?php if (count($news) == 0) {
												echo '';
											} else {
												echo humanDateFormat($new['newsPublished']);
											} ?></h2>
										<h2><?php if (count($news) == 0) {
												echo '';
											} else {
												echo strtoupper($categoryNew);
											} ?></h2>
									</div>
								</a>
							</li>
					<?php
						}
						$i++;
					}
				} else {
					?>
					<li style="background: url('/assets/img/news/default.jpg">
						<a href="/noticias" class="mask"></a>
					</li>
				<?php
				}
				?>
			</ul>
		</div>
	</div>
	<div class="column is-half multimedia">
		<div class="video" style="background: url('/assets/img/videos/<?php if (count($videos) == 0) {
																			echo 'default.jpg';
																		} else {
																			echo trim($videos[0]['videoAvatar']);
																		} ?>');">
			<div class="section">
				<h1>VIDEOS</h1>
			</div>
			<a href="/videos<?php if (count($videos) == 0) {
								echo '';
							} else {
								echo '?video=' . cleanText($videos[0]['videoTitle']);
							} ?>" class="mask">
				<div class="info">
					<h1><?php if (count($videos) == 0) {
							echo '';
						} else {
							echo strtoupper($videos[0]['videoTitle']);
						} ?></h1>
					<h2><?php if (count($videos) == 0) {
							echo '';
						} else {/*$date = date_create($videos[0]['videoPublish']); echo date_format($date, 'd/m/Y');*/
							echo humanDateFormat($videos[0]['videoPublish']);
						} ?></h2>
				</div>
			</a>
		</div>
		<div class="gallery">
			<a href="/galerias<?php if (count($albums) > 0) {
									if ($albums[0]['count'] > 0) {
										echo '#' . cleanText($albums[0]['albumName']) . '-1';
									} else {
										echo '#' . cleanText($albums[0]['albumName']);
									}
								} ?>" class="btnGallery">VER GALERIA</a>
			<div class="flexslider">
				<ul class="slides">
					<img src="/assets/img/galerias/albums/<?php if (count($albums) > 0) {
																echo trim($albums[0]['albumAvatar']);
															} else {
																echo 'default.jpg';
															} ?>">
				</ul>
			</div>
		</div>
	</div>
	<div class="clr"></div>
</section>

<?php
if ($torneosPresentacion['ultimo'] != [] || $torneosPresentacion['proximo'] != []) { ?>
	<div class="is-plays">
		<div class="container">
			<div class="columns">
				<div class="column is-half">
					<h3>Ultimo Partido</h3>
					<?php
					if ($torneosPresentacion['ultimo'] != []) {
					?>
						<span>Jornada <?= $torneosPresentacion['ultimo']['jornada'] ?> / <small><?= humanDateFormat($torneosPresentacion['ultimo']['playDay']) ?></small></span>
						<div class="columns is-played">
							<div class="column is-one-third">
								<img src="/assets/img/equipos/<?= trim($torneosPresentacion['ultimo']['EQlocal']['equipoLogo']) ?>">
								<strong><?= $torneosPresentacion['ultimo']['EQlocal']['equipoName'] ?><br><small><?= $torneosPresentacion['ultimo']['torneo']['torneo'] ?></small></strong>
								<span><?= $torneosPresentacion['ultimo']['localGol'] ?></span>
							</div>
							<div class="column is-vs">VS</div>
							<div class="column is-one-third">
								<img src="/assets/img/equipos/<?= trim($torneosPresentacion['ultimo']['EQvisita']['equipoLogo']) ?>">
								<strong><?= $torneosPresentacion['ultimo']['EQvisita']['equipoName'] ?><br><small><?= $torneosPresentacion['ultimo']['torneo']['torneo'] ?></small></strong>
								<span><?= $torneosPresentacion['ultimo']['visitaGol'] ?></span>
							</div>
						</div>
					<?php
					}
					?>
				</div>

				<div class="column is-half">
					<h3>Próximo Partido</h3>
					<?php
					if ($torneosPresentacion['proximo'] != []) {
					?>
						<span>Jornada <?= $torneosPresentacion['proximo']['jornada'] ?> / <small><?= humanDateFormat($torneosPresentacion['proximo']['playDay']) ?></small></span>
						<div class="columns is-play">
							<div class="column is-one-third">
								<img src="/assets/img/equipos/<?= trim($torneosPresentacion['proximo']['EQlocal']['equipoLogo']) ?>">
								<strong><?= $torneosPresentacion['proximo']['EQlocal']['equipoName'] ?><br><small><?= $torneosPresentacion['proximo']['torneo']['torneo'] ?></small></strong>
							</div>
							<div class="column is-vs">VS</div>
							<div class="column is-one-third">
								<img src="/assets/img/equipos/<?= trim($torneosPresentacion['proximo']['EQvisita']['equipoLogo']) ?>">
								<strong><?= $torneosPresentacion['proximo']['EQvisita']['equipoName'] ?><br><small><?= $torneosPresentacion['proximo']['torneo']['torneo'] ?></small></strong>
							</div>
						</div>
					<?php
					}
					?>
				</div>

			</div>
		</div>
	</div>
<?php } ?>

<div class="container">
	<div class="columns is-multiline">

		<!--RADIO - PUB-->
		<div class="column is-half">
			<iframe src="https://tunein.com/embed/player/s91380/" style="width:100%; height:100px;" scrolling="no" frameborder="no"></iframe>
		</div>
		<div class="column is-half is-publicity">
			<span></span>
		</div>

		<!--NOTICIAS-->
		<div class="column is-full is-group-info">
			<h2>Noticias</h2>

			<div class="columns is-multiline is-group-news">
				<?php
				if ($news != null) {
					foreach ($news as $i => $new) {
						if ($i <= 3) {
				?>
							<div class="column is-one-quarter">
								<a href="<?= $new['newsUrl'] ?>"><span class="is-img" style="background: url(<?php if (trim($new['newsAvatar']) != null || trim($new['newsAvatar']) != '') {
																													echo '/assets/img/news/' . trim($new['newsAvatar']);
																												} else {
																													echo '/assets/img/news/default.jpg';
																												} ?>);"></span></a>
								<a href="<?= $new['categorySlug'] ?>" class="is-category"><?= $new['category'] ?></a>
								<a href="<?= $new['newsUrl'] ?>" class="is-title"><?= $new['newsTitle'] ?></a>
								<small><?= humanDateFormat($new['newsPublished']); ?></small>
								<p></p>
							</div>
				<?php
						}
					}
				}
				?>

				<div class="column is-full">
					<a href="/noticias" class="is-button-group">Más Noticias</a>
				</div>

			</div>
		</div>

		<!--VIDEOS-->
		<div class="column is-two-fifths is-group-info">
			<h2>Videos</h2>
			<div class="columns is-multiline is-group-videos">
				<?php
				if ($videos != null) {
					foreach ($videos as $i => $video) {
						if ($i <= 1) {
							$linkTitleVideo = cleanText($video['videoTitle']); //Construye el link al video
				?>
							<div class="column is-full">
								<a href="/videos?video=<?= $linkTitleVideo ?>" target="__blank">
									<span class="is-video" style="background: url(/assets/img/videos/<?php if (trim($video['videoAvatar']) == null || trim($video['videoAvatar']) == "") {
																											echo 'default.jpg';
																										} else {
																											echo trim($video['videoAvatar']);
																										} ?>);"></span>
								</a>
								<a href="/videos?video=<?= $linkTitleVideo ?>" class="is-icon"><i class="far fa-play-circle"></i></a>
								<a href="/videos?video=<?= $linkTitleVideo ?>" target="__blank" class="is-title"><?= $video['videoTitle'] ?></a>
							</div>
				<?php
						}
					}
				}
				?>

				<div class="column is-full">
					<a href="/videos" class="is-button-group">Más Videos</a>
				</div>
			</div>
		</div>

		<!--GALERIAS-->
		<div class="column is-three-fifths is-group-info">
			<h2>Galerias</h2>
			<div class="columns is-multiline is-group-album">
				<?php
				if ($albums != null) {
					foreach ($albums as $i => $album) {
						if ($i <= 3) {
				?>
							<div class="column is-half">
								<a href="/galerias#<?= trim(cleanText($album['albumName'])) ?><?= ($album['count'] > 0) ? '-1' : ''; ?>" target="__blank">
									<span class="is-video" style="background: url(/assets/img/galerias/albums/<?= trim($album['albumAvatar']) ?>);"></span>
								</a>
								<a href="/galerias#<?= trim(cleanText($album['albumName'])) ?><?= ($album['count'] > 0) ? '-1' : ''; ?>" class="is-icon"><i class="far fa-images"></i></a>
								<a href="/galerias#<?= trim(cleanText($album['albumName'])) ?><?= ($album['count'] > 0) ? '-1' : ''; ?>" target="__blank" class="is-title"><?= $album['albumName'] ?></a>
							</div>
				<?php
						}
					}
				}
				?>

				<div class="column is-full">
					<a href="/galerias" class="is-button-group">Más Galerías</a>
				</div>
			</div>
		</div>

	</div>
</div>

<!-- PALMARES -->
<div class="is-palmares">
	<div class="container">
		<h2>Palmarés</h2>
		<div class="columns">

			<?php
			if ($palmares != null) {
				foreach ($palmares as $i => $copa) {
					if ($i <= 4) {
			?>
						<div class="column is-one-fifth is-item">
							<span class="is-img">
								<img src="/assets/img/palmares/<?= $copa['cupAvatar'] ?>" alt="">
							</span>
							<strong class="is-title"><?= $copa['cupName'] ?></strong>
							<small class="is-date"><?= $copa['championshipDate'] ?></small>
						</div>
			<?php
					}
				}
			}
			?>

		</div>
	</div>
</div>

<script type="text/javascript">
	$('.flexslider').flexslider({
		animation: "slide"
	});
</script>

<style>
	.gallery .flexslider {
		width: 100%;
		height: 100%;
		display: block;
		overflow: hidden;
		margin: 0px;
	}

	.gallery .flexslider ul {
		width: 100%;
		height: 100%;
	}

	.gallery .flexslider img {
		height: 100%
	}
</style>