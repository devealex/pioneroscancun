<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//Generate URL parts for brands
if (!function_exists('cleanText')) {
    function cleanText($text) {
        $text = str_replace("|", "", $text);
        $text = str_replace(",", "", $text);
        $text = str_replace(".", "", $text);
        $text = str_replace("á", "a", $text);
        $text = str_replace("é", "e", $text);
        $text = str_replace("í", "i", $text);
        $text = str_replace("ó", "o", $text);
        $text = str_replace("ú", "u", $text);
        $text = str_replace("!", "", $text);
        $text = str_replace('"', "", $text);
        $text = str_replace('#', "", $text);
        $text = str_replace('$', "", $text);
        $text = str_replace('%', "", $text);
        $text = str_replace('&', "", $text);
        $text = str_replace('/', "", $text);
        $text = str_replace('(', "", $text);
        $text = str_replace(')', "", $text);
        $text = str_replace('=', "", $text);
        $text = str_replace('?', "", $text);
        $text = str_replace('¿', "", $text);
        $text = str_replace('¡', "", $text);
        $text = str_replace('{', "", $text);
        $text = str_replace('}', "", $text);
        $text = str_replace('*', "", $text);
        $text = str_replace('-', "", $text);
        $text = str_replace(':', "", $text);
        $text = str_replace(';', "", $text);
        $text = str_replace('+', "", $text);
        $text = str_replace('<', "", $text);
        $text = str_replace('>', "", $text);
        $text = str_replace('Ñ', "N", $text);
        $text = str_replace('ñ', "n", $text);
        $text = str_replace('°', "", $text);
        $text = str_replace('@', "", $text);
        $text = str_replace(" ", "-", $text);
        return strtolower($text);
    }
}

?>