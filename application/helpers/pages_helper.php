<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

//Generate URL parts for brands
if (!function_exists('createStringSection')) {
    function createStringSection($section) {

        if($section == 'index') { return null; exit; }
    
        $string = ' | ';
        $section_args = explode('-', $section);
    
        foreach($section_args as $key) {
            $string .= ucfirst($key).' ';
        }
    
        return $string;
    
    }
}


//Generate human dateformat 
if (!function_exists('humanDateFormat')) {
    function humanDateFormat($date) {

        $args = explode('-', $date);

        $month = null;

        switch($args[1]) {
            case '01':
                $month = 'Enero';
                break;
            case '02':
                $month = 'Febrero';
                break;
            case '03':
                $month = 'Marzo';
                break;
            case '04':
                $month = 'Abril';
                break;
            case '05':
                $month = 'Mayo';
                break;
            case '06':
                $month = 'Junio';
                break;
            case '07':
                $month = 'Julio';
                break;
            case '08':
                $month = 'Agosto';
                break;
            case '09':
                $month = 'Septiembre';
                break;
            case '10':
                $month = 'Octubre';
                break;
            case '11':
                $month = 'Noviembre';
                break;
            case '12':
                $month = 'Diciembre';
                break;
        }
        
        return $args[2].' de '.$month.' del '.$args[0];

    }
}

?>