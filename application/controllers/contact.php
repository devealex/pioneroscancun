<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Contact extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->helper(array('url','pages'));
    }

    public function contactUs() {
        $data = [
            'contactName' => $this->input->post('contactName'),
            'contactNumber' => $this->input->post('contactNumber'),
            'email' => $this->input->post('email'),
            'contactSubject' => $this->input->post('contactSubject'),
            'address' => $this->input->post('address'),
            'dateContact' => date('Y-m-d H:i:s')
        ];
        $this->sendMail($data);
        echo json_encode([
            'msg' => 'Tu mensaje fue enviado',
            'state' => 1
        ]);
    }

    public function sendMail($data) {
        $body = '
        <div style="box-sizing:border-box;padding:20px;background:#fff;overflow:hidden;width:100%;font-family:sans-serif;">
            <div style="width:100%;box-sizing:border-box;padding:16px;background:#b23232;overflow:hidden;">
                <img src="https://upload.wikimedia.org/wikipedia/en/thumb/d/d7/Pioneros_Logo.png/200px-Pioneros_Logo.png" alt="pioneroscancunfc" style="max-height:150px;float:left;margin-right:100px;">
                <h1 style="float:left;font-size:30px;color:#fff;">GRACIAS POR CONTACTARTE CON NOSTOTROS</h1>
                <h2 style="float:left;font-size:20px;color:#fff;">Un Encargado se pondra en contacto con usted<h2>
            </div>
            <div style="width:100%;box-sizing:border-box;padding:20px;background:#fff;overflow:hidden;">
                <br>
                <strong>NOMBRE: </strong><p>'.$data['contactName'].'</p>
                <br>
                <strong>NUMERO DE CONTACTO: </strong><p>'.$data['contactNumber'].'</p>
                <br>
                <strong>CORREO ELECTRONICO: </strong><p>'.$data['email'].'</p>
                <br>
                <strong>DIRECCIÓN: </strong><p>'.$data['address'].'</p>
                <br>
                <strong>TEMA: </strong><p>'.$data['contactSubject'].'</p>
                <br>
                <strong>FECHA DE CONTACTO: </strong><p>'.$data['dateContact'].'</p>
            </div>
            <div style="width:100%;box-sizing:border-box;padding:16px;background:#b23232;overflow:hidden;">

            </div>
        </div>
        ';
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'ssl://mail.pioneroscancunfc.com';
        $config['smtp_port'] = 465;
        $config['smtp_timeout'] = '7';
        $config['smtp_user'] = 'hola@pioneroscancunfc.com';
        $config['smtp_pass'] = 'd3v3l0p3r';
        $config['mailtype'] = 'html';
        $config['charset'] = 'utf-8';
        $config['validation'] = FALSE;

        $this->load->library('email');
        $this->email->initialize($config);
        $this->email->from('hola@pioneroscancunfc.com', 'Administrador');
        $this->email->to($data['email']);
        $this->email->cc('hola@pioneroscancunfc.com');
        $this->email->subject('Contacto');
        $this->email->message($body);
        $this->email->send();
    }

}
