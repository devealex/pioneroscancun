<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Torneos extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('torneos_model');
        $this->load->model('jornadas_model');
        $this->load->model('estadisticas_model');
        $this->load->model('goleo_model');
        $this->load->helper(array('url','pages'));
    }

    // CRUDS
    public function crud($action) {
        switch ($action) {
            case 'create':
                $data = [
                    'torneo' => $this->input->post('torneo'),
                    'torneourl' => $this->input->post('torneourl'),
                    'torneoPlantel' => $this->input->post('torneoPlantel')
                ];
                $insert = $this->torneos_model->create_torneo($data);
                $id = json_decode($insert)->idTorneo;
                $jornadas = $this->input->post('jornadas');
                for ($i = 1; $i <= $jornadas; $i++) {
                    $dtj = [
                        'jornada' => $i,
                        'torneoJornada' => $id
                    ];
                    $this->jornadas_model->create_jornada($dtj);
                }
                $equipos = explode( ',', $this->input->post('equipos') );
                for($i = 0;$i <= count($equipos)-1;$i++) {
                    if ($equipos[$i] != null || $equipos[$i] != "") {
                        $dtte = [
                            'fkTorneo' => $id,
                            'fkEquipo' => $equipos[$i],
                        ];
                        $torneo_equipo = $this->torneos_model->create_equipos($dtte);
                        $idTorneoEquipo = json_decode($torneo_equipo)->idTorneoEquipo;
                        $dttes = [
                            'fkTorneoEquipos' => $idTorneoEquipo
                        ];
                        $this->estadisticas_model->create_estadistica($dttes);
                    }
                }
                echo $insert;
                break;
            case 'edit':
                $data = [
                    'torneo' => $this->input->post('torneo'),
                    'torneourl' => $this->input->post('torneourl'),
                    'torneoPlantel' => $this->input->post('torneoPlantel')
                ];
                $equiposDel = explode( ',', $this->input->post('equiposDel') );
                if (count($equiposDel) != 0) {
                    for ($i = 0; $i <= count($equiposDel)-1; $i++) {
                        $this->torneos_model->delete_equipos($this->input->post('idTorneo'), $equiposDel[$i]);
                    }
                }
                $equipos = explode( ',', $this->input->post('equipos') );
                if (count($equipos) != 0) {
                    for ($i = 0; $i <= count($equipos)-1; $i++) {
                        if ($equipos[$i] != null || $equipos[$i] != "") {
                            $equipo = $this->torneos_model->get_equipos($this->input->post('idTorneo'), $equipos[$i]);
                            if ($equipo == 0 || $equipo == null) {
                                $dtte = [
                                    'fkTorneo' => $this->input->post('idTorneo'),
                                    'fkEquipo' => $equipos[$i],
                                ];
                                $torneo_equipo = $this->torneos_model->create_equipos($dtte);
                                $idTorneoEquipo = json_decode($torneo_equipo)->idTorneoEquipo;
                                $dttes = [
                                    'fkTorneoEquipos' => $idTorneoEquipo
                                ];
                                $this->estadisticas_model->create_estadistica($dttes);
                            }
                        }
                    }
                }
                //$this->torneos_model->delete_equipos($this->input->post('idTorneo')); //Borra los equipos existentes
                //Actualiza los equipos nuevos
                /*for($i = 0;$i <= count($equipos)-1;$i++) {
                    if ($equipos[$i] != null || $equipos[$i] != "") {
                        $dtte = [
                            'fkTorneo' => $this->input->post('idTorneo'),
                            'fkEquipo' => $equipos[$i],
                        ];
                        $torneo_equipo = $this->torneos_model->create_equipos($dtte);
                        $idTorneoEquipo = json_decode($torneo_equipo)->idTorneoEquipo;
                        $dttes = [
                            'fkTorneoEquipos' => $idTorneoEquipo
                        ];
                        $this->estadisticas_model->create_estadistica($dttes);
                    }
                }*/
                echo $this->torneos_model->edit_torneo($data, $this->input->post('idTorneo'));
                /*echo json_encode([
                    'msg' => $equiposDel[0],
                    'state' => 1
                ]);*/
                break;
            case 'delete':
                $idTorneo = $this->input->post('idTorneo');
                echo $this->torneos_model->delete_torneo($idTorneo);
                break;
            case 'state':
                $data = [
                    'torneoAvailable' => $this->input->post('torneoAvailable')
                ];
                echo $this->torneos_model->edit_torneo($data, $this->input->post('idTorneo'));
                break;
        }
    }

}
