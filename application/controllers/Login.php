<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/**
 *
 */
class Login extends CI_Controller
{

    function __construct()
    {
        parent::__construct();
        $this->load->model('login_model');
        $this->load->helper(array('url','pages'));
    }

    public function login() {
        $email = $this->input->post('email');
        $pass = md5($this->input->post('pass'));
        $user = $this->login_model->verify_user($email, $pass);
        if ($user != null) {
            $this->session->set_userdata($user[0]);
            echo json_encode(array(
                'userType' => $user[0]['userRol'],
                'rs' => true
            ));
        } else {
            echo json_encode(array(
                'rs' => false
            ));
        }
    }

    public function logout() {
        session_destroy();
        header('Location: /manager');
    }

}
