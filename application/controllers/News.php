<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('news_model');
        $this->load->model('planteles_model');
        $this->load->model('gallery_model');
        $this->load->model('videos_model');
        $this->load->helper(array('url','pages'));
    }

    // TRAFIC FRONT
    public function traficFront($slug = null, $post = null) {
        $this->load->model('torneos_model');
        $args['torneos'] = $this->torneos_model->get_torneos();

        if ($slug == null && $post == null) {
            $view = 'noticias';
            $args['page'] = array('section' => 'Noticias',
                                  'title_anexo' => createStringSection('Noticias'),
                                  'content' => $this->news_model->get_news(null, null, null, date('Y-m-d H:i:s')),
                                  'planteles' => $this->planteles_model->get_planteles(),
                                  'categories' => $this->news_model->get_categories());
        }

        if ($slug <> null) {
            $view = 'noticias';
            $args['page'] = array('section' => $slug,
                                  'title_anexo' => createStringSection($slug),
                                  'content' => $this->news_model->get_news(null, 'noticias/'.$slug, null, date('Y-m-d H:i:s')),
                                  'planteles' => $this->planteles_model->get_planteles(),
                                  'categories' => $this->news_model->get_categories());
        }

        if ($post <> null) {
            $view = 'noticia';
            $args['page'] = array('section' => $post,
                                  'title_anexo' => createStringSection($post),
                                  'content' => $this->news_model->get_news(null, null, 'noticias/'.$slug.'/'.$post)[0],
                                  'planteles' => $this->planteles_model->get_planteles(),
                                  'categories' => $this->news_model->get_categories());
            $args['interesting'] = array('video' => $this->videos_model->get_videos_limit(1),
                                         'gallery' => $this->gallery_model->get_albums_limit(1));
        }

        $this->load->model('sponsors_model');
        $args['sponsors'] = $this->sponsors_model->get_sponsors();

        $this->load->view('components/header', $args);
        $this->load->view($view, $args);
        $this->load->view('components/footer', $args);

    }

    // CRUDS
    public function crud($action) {
        switch ($action) {
            case 'create':
                $data = [
                    'newsTitle' => $this->input->post('newsTitle'),
                    'newsContent' => "".$this->input->post('newsContent')."",
                    'newsCategory' => $this->input->post('newsCategory'),
                    'newsAvatar' => $this->input->post('newsAvatar'),
                    'newsCreated' => date('Y-m-d H:i:s'),
                    'newsPublished' => $this->input->post('newsPublished'),
                    'newsUrl' => $this->input->post('newsUrl')
                ];
                echo $this->news_model->create_new($data);
                break;
            case 'edit':
                $nameAvatar = $this->input->post('newsAvatar');
                if ($nameAvatar == "" || $nameAvatar == null) {
                    $nameAvatar = $this->input->post('nameAvatar');
                } else {
                    if ($this->input->post('nameAvatar') != "" || $this->input->post('nameAvatar') != null) {
                        unlink($_SERVER['DOCUMENT_ROOT'] . '/assets/img/news/' . trim($this->input->post('nameAvatar')));
                    }
                }
                $data = [
                    'newsTitle' => $this->input->post('newsTitle'),
                    'newsContent' => "".$this->input->post('newsContent')."",
                    'newsCategory' => $this->input->post('newsCategory'),
                    'newsAvatar' => $nameAvatar,
                    'newsPublished' => $this->input->post('newsPublished'),
                    'newsUrl' => $this->input->post('newsUrl')
                ];
                $idNew = $this->input->post('idNew');
                echo $this->news_model->edit_new($data, $idNew);
                break;
            case 'delete':
                $idNew = $this->input->post('idnew');
                $newsAvatar = $this->input->post('newsAvatar');
                if ($newsAvatar != "" || $newsAvatar != " ") {
                    unlink($_SERVER['DOCUMENT_ROOT'] . '/assets/img/news/' . $newsAvatar);
                }
                echo $this->news_model->delete_new($idNew);
                break;
            case 'image':
                $this->upload_image();
                break;
        }
    }

    public function upload_image() {
        $fileRoute = $_SERVER['DOCUMENT_ROOT'] . '/assets/img/news/';

        $config['upload_path'] = $fileRoute;
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['encrypt_name'] = true;
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('newsAvatar')) {
            echo $this->upload->display_errors();
        } else {
            echo $this->upload->data('file_name');
        }
    }

}
