<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Calendario extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('calendar_model');
        $this->load->helper(array('url','pages'));
    }

    // CRUDS
    public function crud($action) {
        switch ($action) {
            case 'create':
                $data = [
                    'jornadaRol' => $this->input->post('jornadaRol'),
                    'playDay' => $this->input->post('playDay'),
                    'playTime' => $this->input->post('playTime'),
                    'localRol' => $this->input->post('localRol'),
                    'visitaRol' => $this->input->post('visitaRol'),
                    'fkTorneoRol' => $this->input->post('fkTorneoRol')
                ];
                echo $this->calendar_model->create_calendario($data);
                break;
            case 'goles':
                $data = [];
                if ($this->input->post('localGol') || isset($_POST['localGol'])) {
                    $data = [
                        'localGol' => $this->input->post('localGol')
                    ];
                }
                if ($this->input->post('visitaGol') || isset($_POST['visitaGol'])) {
                    $data = [
                        'visitaGol' => $this->input->post('visitaGol')
                    ];
                }
                echo $this->calendar_model->edit_calendario($data, $this->input->post('idCalendario'));
                break;
            case 'updDateTime':
                $data = [];
                if (isset($_POST['playDay'])) {
                    $data = ['playDay' => $_POST['playDay']];
                }
                if (isset($_POST['playTime'])) {
                    $data = ['playTime' => $_POST['playTime']];
                }
                echo $this->calendar_model->edit_calendario($data, $this->input->post('idCalendario'));
                break;
            case 'delete':
                $idCalendario = $this->input->post('idCalendario');
                echo $this->calendar_model->delete_calendario($idCalendario);
                break;
        }
    }

}
