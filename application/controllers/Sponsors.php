<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Sponsors extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('sponsors_model');
        $this->load->helper(array('url','pages'));
    }

    // CRUDS
    public function crud($action) {
        switch ($action) {
            case 'create':
                $data = [
                    'logotype' => $this->input->post('logotype'),
                    'sponsorOrder' => $this->input->post('sponsorOrder'),
                    'sponsorSite' => $this->input->post('sponsorSite')
                ];
                echo $this->sponsors_model->create_sponsors($data);
                break;
            case 'delete':
                $idSponsor = $this->input->post('idSponsor');
                $logotype = $this->input->post('logotype');
                if ($logotype != "" || $logotype != " ") {
                    unlink($_SERVER['DOCUMENT_ROOT'] . '/assets/img/sponsors/' . $logotype);
                }
                echo $this->sponsors_model->delete_sponsors($idSponsor);
                break;
            case 'image':
                $this->upload_image();
                break;
            case 'validateOrder':
                $sponsorOrder = $this->input->post('sponsorOrder');
                $val = $this->sponsors_model->get_sponsors(null, 'DESC', false, null, $sponsorOrder);
                if ($val != null) {
                    echo false;
                } else {
                    echo true;
                }
                break;
        }
    }

    public function upload_image() {
        $fileRoute = $_SERVER['DOCUMENT_ROOT'] . '/assets/img/sponsors/';

        $config['upload_path'] = $fileRoute;
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['encrypt_name'] = true;
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('logotype')) {
            echo $this->upload->display_errors();
        } else {
            echo $this->upload->data('file_name');
        }
    }

}
