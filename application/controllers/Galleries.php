<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Galleries extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('gallery_model');
        $this->load->helper(array('url','pages'));
    }

    public function traficFront() {
        $this->load->model('news_model');
        $this->load->model('planteles_model');
        $this->load->model('torneos_model');
        $args['torneos'] = $this->torneos_model->get_torneos();

        $args['page'] = array('section' => 'Galerias',
                              'title_anexo' => createStringSection('Galerias'),
                              'albums' => $this->gallery_model->get_albums(null, 1),
                              'images' => $this->gallery_model->get_images(),
                              'planteles' => $this->planteles_model->get_planteles(),
                              'categories' => $this->news_model->get_categories());

        $this->load->model('sponsors_model');
        $args['sponsors'] = $this->sponsors_model->get_sponsors();

        $this->load->view('components/header', $args);
        $this->load->view('galerias', $args);
        $this->load->view('components/footer', $args);
    }

    public function images($idAlbum) {
        $args['section'] = 'Imagenes';

        //VALIDACION DE SESSION
        if ($this->session->userdata('idUser') != null) {
            $args['userType'] = $this->session->userdata('userRol');
        } else {
            header('Location: /manager');
        }

        $args['images'] = $this->gallery_model->get_images(null, $idAlbum);
        $args['idAlbum'] = $idAlbum;
        $args['album'] = $this->gallery_model->get_albums($idAlbum)[0];

        $this->load->view('manager/components/header', $args);
        $this->load->view('manager/galerias/images', $args);
        $this->load->view('manager/components/footer', $args);
    }

    // CRUDS
    public function crud($action) {
        switch ($action) {
            case 'create':
                $data = [
                    'albumName' => $this->input->post('albumName'),
                    'albumAvatar' => $this->input->post('albumAvatar'),
                    'albumRegister' => date('Y-m-d H:i:s')
                ];
                echo $this->gallery_model->create_albums($data);
                break;
            case 'edit':
                $nameAvatar = $this->input->post('albumAvatar');
                if ($nameAvatar == "" || $nameAvatar == null) {
                    $nameAvatar = $this->input->post('nameAvatar');
                } else {
                    if ($this->input->post('nameAvatar') != "" || $this->input->post('nameAvatar') != null) {
                        $fileRoute = $_SERVER['DOCUMENT_ROOT'] . '/assets/img/galerias/albums/';
                        unlink($fileRoute . $this->input->post('nameAvatar'));
                    }
                }
                $data = [
                    'albumName' => $this->input->post('albumName'),
                    'albumAvatar' => $nameAvatar
                ];
                echo $this->gallery_model->edit_albums($data, $this->input->post('idAlbum'));
                break;
            case 'state':
                $data = [
                    'albumPublish' => $this->input->post('albumPublish')
                ];
                echo $this->gallery_model->edit_albums($data, $this->input->post('idAlbum'));
                break;
            case 'delete':
                $idAlbum = $this->input->post('idAlbum');
                $fileRoute = $_SERVER['DOCUMENT_ROOT'] . '/assets/img/galerias/albums/';
                unlink($fileRoute . $this->input->post('albumAvatar'));
                $images = $this->gallery_model->get_images(null, $idAlbum);
                if (count($images) != 0) {
                    foreach($images as $image){
                        $fileRouteImage = $_SERVER['DOCUMENT_ROOT'] . '/assets/img/galerias/images/';
                        unlink($fileRouteImage . trim($image['galleryImage']));
                    }
                }
                echo $this->gallery_model->delete_albums($idAlbum);
                break;
            case 'image':
                $this->upload_image();
                break;
        }
    }

    // CRUDS IMAGENES
    public function crudImage($action) {
        switch ($action) {
            case 'create':
                /*$data = [
                    'galleryImage' => $this->input->post('galleryImage'),
                    'galleryRegister' => date('Y-m-d H:i:s'),
                    'album' => $this->input->post('idAlbum')
                ];
                echo $this->gallery_model->create_image($data);*/
                //print_r($_FILES);
                $this->upload_image_gallery();
                break;
            case 'delete':
                $idGallery = $this->input->post('idGallery');
                $fileRoute = $_SERVER['DOCUMENT_ROOT'] . '/assets/img/galerias/images/';
                unlink($fileRoute . $this->input->post('galleryImage'));
                echo $this->gallery_model->delete_image($idGallery);
                break;
            case 'image':
                $this->upload_image_gallery();
                break;
        }
    }

    public function upload_image() {
        $fileRoute = $_SERVER['DOCUMENT_ROOT'] . '/assets/img/galerias/albums/';

        $config['upload_path'] = $fileRoute;
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['encrypt_name'] = true;
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('albumAvatar')) {
            echo $this->upload->display_errors();
        } else {
            echo $this->upload->data('file_name');
        }
    }

    public function upload_image_gallery() {
        //print_r($_FILES);

        $fileRoute = $_SERVER['DOCUMENT_ROOT'] . '/assets/img/galerias/images/';
        $config['upload_path'] = $fileRoute;
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['encrypt_name'] = true;

        $variableFile = $_FILES;
        $count = count($_FILES['archivo']['name']);

        for ($i = 0;$i <= $count; $i++) {
            $_FILES['archivo']['name'] = $variableFile['archivo']['name'][$i];
            $_FILES['archivo']['type'] = $variableFile['archivo']['type'][$i];
            $_FILES['archivo']['tmp_name'] = $variableFile['archivo']['tmp_name'][$i];
            $_FILES['archivo']['error'] = $variableFile['archivo']['error'][$i];
            $_FILES['archivo']['size'] = $variableFile['archivo']['size'][$i];

            $this->load->library('upload', $config);

            if ($this->upload->do_upload('archivo')) {
                $image = array('upload_data' => $this->upload->data());
                $data = [
                    'galleryImage' => $this->upload->data('file_name'),
                    'galleryRegister' => date('Y-m-d H:i:s'),
                    'album' => $this->input->post('idAlbum')
                ];
                echo $this->gallery_model->create_image($data);
            }
        }
    }

}
