<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Goleo extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('goleo_model');
        $this->load->helper(array('url','pages'));
    }

    public function crud($action) {
        switch($action) {
            case 'edit':
                $data = [];
                if (isset($_POST['nombre'])) {
                    $data = [
                        'nombre' => $_POST['nombre']
                    ];
                }
                if (isset($_POST['equipo'])) {
                    $data = [
                        'equipo' => $_POST['equipo']
                    ];
                }
                if (isset($_POST['goles'])) {
                    $data = [
                        'goles' => $_POST['goles']
                    ];
                }
                echo $this->goleo_model->edit_goleo($_POST['idgoleo'], $data);
            break;
        }
    }

}

?>