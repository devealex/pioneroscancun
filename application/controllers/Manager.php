<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Manager extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('news_model');
        $this->load->model('schools_model');
        $this->load->model('users_model');
        $this->load->model('planteles_model');
        $this->load->model('palmares_model');
        $this->load->model('gallery_model');
        $this->load->model('videos_model');
        $this->load->model('sponsors_model');
        $this->load->model('subscriptions_model');
        $this->load->model('torneos_model');
        $this->load->model('calendar_model');
        $this->load->model('grupos_model');
        $this->load->model('equipos_model');
        $this->load->model('jornadas_model');
        $this->load->model('estadisticas_model');
        $this->load->model('goleo_model');
        $this->load->helper(array('url','pages'));
    }

    public function trafic($section = null, $page = null) {

        $folder = '';
        $view = '';

        if ($section == 'noticias') {
            $folder = 'news';
            switch ($page) {
                case 'listview':
                    $args['section'] = 'Noticias';
                    $view = 'listview';
                    $args['news'] = $this->news_model->get_news();
                    $args['categories'] = $this->news_model->get_categories();
                    break;
                case 'create':
                    $args['section'] = 'Crear Noticia';
                    $view = 'form';
                    $args['action'] = 'create';
                    $args['categories'] = $this->news_model->get_categories();
                    break;
            }
        }

        if ($section == 'escuelas') {
            $folder = 'schools';
            switch ($page) {
                case 'listview':
                    $args['section'] = 'Escuelas';
                    $args['schools'] = $this->schools_model->get_schools();
                    $view = 'listview';
                    break;
            }
        }

        if ($section == 'usuarios') {
            $folder = 'users';
            switch ($page) {
                case 'listview':
                    $args['section'] = 'Usuarios';
                    $args['users'] = $this->users_model->get_users();
                    $view = 'listview';
                    break;
                case 'create':
                    $args['section'] = 'Crear Usuario';
                    $args['action'] = 'create';
                    $view = 'form';
                    break;
            }
        }

        if ($section == 'planteles') {
            $folder = 'planteles';
            switch ($page) {
                case 'listview':
                    $args['section'] = 'Planteles';
                    $view = 'listview';
                    $args['planteles'] = $this->planteles_model->get_planteles();
                    break;
                case 'create':
                    $args['section'] = 'Crear Plantel';
                    $args['action'] = 'create';
                    $view = 'form';
                    break;
            }
        }

        if ($section == 'palmares') {
            $folder = 'palmares';
            switch ($page) {
                case 'listview':
                    $args['section'] = 'Palmares';
                    $view = 'listview';
                    $args['palmares'] = $this->palmares_model->get_palmares();
                    break;
                case 'create':
                    $args['section'] = 'Agregar Campeonato';
                    $args['action'] = 'create';
                    $view = 'form';
                    break;
            }
        }

        if ($section == 'galerias') {
            $folder = 'galerias';
            switch ($page) {
                case 'listview':
                    $args['section'] = 'Galerias';
                    $view = 'listview';
                    $args['albums'] = $this->gallery_model->get_albums();
                    break;
                case 'create':
                    $args['section'] = 'Agregar Galeria';
                    $args['action'] = 'create';
                    $view = 'form';
                    break;
            }
        }

        if ($section == 'videos') {
            $folder = 'videos';
            switch ($page) {
                case 'listview':
                    $args['section'] = 'Videos';
                    $view = 'listview';
                    $args['videos'] = $this->videos_model->get_videos();
                    break;
                case 'create':
                    $args['section'] = 'Agregar Videos';
                    $args['action'] = 'create';
                    $view = 'form';
                    break;
            }
        }

        if ($section == 'patrocinadores') {
            $folder = 'sponsors';
            switch ($page) {
                case 'listview':
                    $args['section'] = 'Patrocinadores';
                    $view = 'listview';
                    $args['sponsors'] = $this->sponsors_model->get_sponsors();
                    break;
                case 'create':
                    $args['section'] = 'Agregar patrocinador';
                    $args['action'] = 'create';
                    $view = 'form';
                    break;
            }
        }

        if ($section == 'suscripciones') {
            $folder = 'newsletter';
            switch ($page) {
                case 'listview':
                    $args['section'] = 'Suscripciones';
                    $view = 'listview';
                    if ( isset($_POST['dateInitial']) && isset($_POST['dateFinal']) ) {
                        $dateInitial = $this->input->post('dateInitial');
                        $dateFinal = $this->input->post('dateFinal');
                        if ( $this->input->post('dateInitial') != null && $this->input->post('dateFinal') != null ) {
                            $args['subscriptions'] = $this->subscriptions_model->get_subscriptions($dateInitial, $dateFinal);
                            $args['dateInitial'] = $dateInitial;
                            $args['dateFinal'] = $dateFinal;
                        }
                        if ( $this->input->post('dateInitial') != null && $this->input->post('dateFinal') == null ) {
                            $args['subscriptions'] = $this->subscriptions_model->get_subscriptions($dateInitial);
                            $args['dateInitial'] = $dateInitial;
                        }
                        if ( $this->input->post('dateInitial') == null && $this->input->post('dateFinal') == null ) {
                            $args['subscriptions'] = $this->subscriptions_model->get_subscriptions();
                        }
                    } else {
                        $args['subscriptions'] = $this->subscriptions_model->get_subscriptions();
                    }
                    break;
            }
        }

        if ($section == 'torneos') {
            $folder = 'torneos';
            switch ($page) {
                case 'listview':
                    $args['section'] = 'Torneos';
                    $view = 'listview';
                    $args['torneos'] = $this->torneos_model->get_torneos();
                    break;
                case 'create':
                    $args['section'] = 'Agregar Torneo';
                    $args['action'] = 'create';
                    $view = 'form';
                    $args['equipos'] = $this->equipos_model->get_equipos();
                    $args['planteles'] = $this->planteles_model->get_planteles();
                    break;
            }
        }

        if ($section == 'jornadas') {
            $folder = 'jornadas';
            switch ($page) {
                case 'listview':
                    $args['section'] = 'Jornadas';
                    $view = 'listview';
                    if (isset($_GET['t'])) {
                        $args['jornadas'] = $this->jornadas_model->get_jornadas(null, $_GET['t']);
                        $args['idTorneo'] = $_GET['t'];
                    }
                    $args['torneos'] = $this->torneos_model->get_torneos();
                    break;
            }
        }

        if ($section == 'calendario') {
            $folder = 'calendar';
            switch ($page) {
                case 'listview':
                    $args['section'] = 'Calendario';
                    $view = 'listview';
                    if (isset($_GET['t']) && isset($_GET['j'])) {
                        $args['torneo'] = $this->torneos_model->get_torneos($_GET['t'])[0];
                        $args['calendario'] = $this->calendar_model->get_calendario(null, $_GET['t'], $_GET['j']);
                        $args['idTorneo'] = $_GET['t'];
                        $args['jornadaRol'] = $_GET['j'];
                        $args['equipos'] = $this->torneos_model->get_equipos($_GET['t']);
                    }
                    break;
                case 'create':
                    $args['section'] = 'Agregar Evento';
                    $args['action'] = 'create';
                    $args['torneos'] = $this->torneos_model->get_torneos();
                    $args['grupos'] = $this->grupos_model->get_grupos();
                    $args['equipos'] = $this->equipos_model->get_equipos();
                    $view = 'form';
                    break;
            }
        }

        if ($section == 'estadisticas') {
            $folder = 'estadisticas';
            switch ($page) {
                case 'listview':
                    $args['section'] = 'Estadisticas';
                    $view = 'listview';
                    if (isset($_GET['t'])) {
                        $args['torneo'] = $this->torneos_model->get_torneos($_GET['t'])[0];
                        $args['estadisticas'] = $this->estadisticas_model->get_estadisticas(null, $_GET['t']);
                        $args['idTorneo'] = $_GET['t'];
                    }
                    break;
            }
        }

        if ($section == 'equipos') {
            $folder = 'equipos';
            switch ($page) {
                case 'listview':
                    $args['section'] = 'Equipos';
                    $view = 'listview';
                    $args['equipos'] = $this->equipos_model->get_equipos();
                    break;
                case 'create':
                    $args['section'] = 'Agregar Equipo';
                    $args['action'] = 'create';
                    $view = 'form';
                    break;
            }
        }

        if ($section == 'goleo') {
            $folder = 'goleo';
            switch ($page) {
                case 'listview':
                    $args['section'] = 'Lista de Goleo';
                    $view = 'listview';
                    $args['goleos'] = null;
                    if (isset($_GET['t'])) {
                        $args['equipos'] = $this->torneos_model->get_equipos($_GET['t']);
                        $torneo = $this->torneos_model->get_torneos($_GET['t']);
                        if ($torneo != null || $torneo != []) {
                            $args['torneo'] = $this->torneos_model->get_torneos($_GET['t'])[0];
                            $args['goleos'] = $this->goleo_model->get_goleos($_GET['t']);
                            if ($args['goleos'] == null || $args['goleos'] == []) {
                                for($i = 1; $i <= 10; $i++) {
                                    $this->goleo_model->add_goleo([
                                        'fktorneo' => $_GET['t']
                                    ]);
                                }
                                $args['goleos'] = $this->goleo_model->get_goleos($_GET['t']);
                            }
                        }
                    }
                break;
            }
        }

        //VALIDACION DE SESSION
        if ($this->session->userdata('idUser') != null) {
            $args['userType'] = $this->session->userdata('userRol');
        } else {
            header('Location: /manager');
        }

        //VALIDACION DE LA VISTA
        if (!file_exists(APPPATH.'views/manager/'.$folder.'/'.$view.'.php')) {
            show_404();
        }

        $this->load->view('manager/components/header', $args);
        $this->load->view('manager/'.$folder.'/'.$view, $args);
        $this->load->view('manager/components/footer', $args);

    }

    public function traficEdit($section = null, $id = null) {

        $get = null;
        $name_item = '';
        $folder = '';

        switch ($section) {
            case 'noticias':
                $args['section'] = 'Modificar Noticia';
                $name_item = 'new_item';
                $folder = 'news';
                $get = $this->news_model->get_news($id)[0];
                $args['categories'] = $this->news_model->get_categories();
                break;
            case 'escuelas':
                $args['section'] = 'Modificar Escuela';
                $name_item = 'school_item';
                $folder = 'schools';
                $get = $this->schools_model->get_schools($id)[0];
                break;
            case 'usuarios':
                $args['section'] = 'Modificar Usuario';
                $name_item = 'user_item';
                $folder = 'users';
                $get = $this->users_model->get_users($id)[0];
                break;
            case 'planteles':
                $args['section'] = 'Modificar Plantel';
                $name_item = 'plantel_item';
                $folder = 'planteles';
                $get = $this->planteles_model->get_planteles($id)[0];
                break;
            case 'palmares':
                $args['section'] = 'Modificar Campeonato';
                $name_item = 'campeonato_item';
                $folder = 'palmares';
                $get = $this->palmares_model->get_palmares($id)[0];
                break;
            case 'galerias':
                $args['section'] = 'Modificar Galeria';
                $name_item = 'album_item';
                $folder = 'galerias';
                $get = $this->gallery_model->get_albums($id)[0];
                break;
            case 'videos':
                $args['section'] = 'Modificar Video';
                $name_item = 'video_item';
                $folder = 'videos';
                $get = $this->videos_model->get_videos($id)[0];
                break;
            case 'torneos':
                $args['section'] = 'Modificar Torneo';
                $name_item = 'torneo_item';
                $folder = 'torneos';
                $get = $this->torneos_model->get_torneos($id)[0];
                $args['equipos'] = $this->equipos_model->get_equipos();
                $args['misEquipos'] = $this->torneos_model->get_equipos($id);
                $args['planteles'] = $this->planteles_model->get_planteles();
                $args['jornadas'] = count($this->jornadas_model->get_jornadas(null, $id));
                break;
            case 'calendario':
                $args['section'] = 'Modificar Evento';
                $name_item = 'calendario_item';
                $folder = 'calendar';
                $get = $this->calendar_model->get_calendario($id)[0];
                $args['torneos'] = $this->torneos_model->get_torneos();
                $args['grupos'] = $this->grupos_model->get_grupos();
                $args['equipos'] = $this->equipos_model->get_equipos();
                break;
            case 'equipos':
                $args['section'] = 'Modificar Equipo';
                $name_item = 'equipo_item';
                $folder = 'equipos';
                $get = $this->equipos_model->get_equipos($id)[0];
                break;
        }

        if (!$get || $get == null) {
            show_404();
        }

        if ($this->session->userdata('idUser') != null) {
            $args['userType'] = $this->session->userdata('userRol');
        } else {
            header('Location: /manager');
        }

        $args[$name_item] = $get;
        $args['action'] = 'edit';

        $this->load->view('manager/components/header', $args);
        $this->load->view('manager/'.$folder.'/form.php', $args);
        $this->load->view('manager/components/footer', $args);
    }

}
