<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Categories extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('categories_model');
        $this->load->helper(array('url','pages'));
    }

    // CRUDS
    public function crud($action) {
        switch ($action) {
            case 'create':
                $data = [
                    'category' => $this->input->post('category'),
                    'categorySlug' => $this->input->post('slug'),
                    'categoryPublish' => 1,
                    'categoryPublished' => date('Y-m-d H:i:s')
                ];
                echo $this->categories_model->create_category($data);
                break;
            case 'edit':
                $data = [
                    'category' => $this->input->post('category'),
                    'categorySlug' => $this->input->post('slug'),
                    'categoryPublish' => 1,
                    'categoryPublished' => date('Y-m-d H:i:s')
                ];
                $idCategory = $this->input->post('idCategory');
                echo $this->categories_model->edit_category($data, $idCategory);
                break;
            case 'delete':
                $idCategory = $this->input->post('idCategory');
                echo $this->categories_model->delete_category($idCategory);
                break;
        }
    }

}
