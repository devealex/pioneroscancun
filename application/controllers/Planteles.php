<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Planteles extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('planteles_model');
        $this->load->model('players_model');
        $this->load->model('news_model');
        $this->load->helper(array('url','pages'));
    }

    //Planteles View
    public function index() {

        $this->load->model('sponsors_model');

        $args['page'] = array('section' => 'planteles',
                              'planteles' => $this->planteles_model->get_planteles(),
                              'categories' => $this->news_model->get_categories(),
                              'title_anexo' => '| Planteles');

        $args['sponsors'] = $this->sponsors_model->get_sponsors();

        $this->load->view('components/header', $args);
        $this->load->view('planteles', $args);
        $this->load->view('components/footer', $args);

    }

    //Trafico del FRONT
    public function traficFront($slug = null) {
        $this->load->model('torneos_model');
        $args['torneos'] = $this->torneos_model->get_torneos();

        $plantel = $this->planteles_model->get_planteles(null, 'plantel/'.$slug);

        if ($plantel == null || $plantel == "") {
            show_404();
        }

        $this->load->model('sponsors_model');

        $args['page'] = array('section' => $slug,
                              'title_anexo' => createStringSection($slug),
                              'content' => $plantel[0],
                              'planteles' => $this->planteles_model->get_planteles(),
                              'categories' => $this->news_model->get_categories(),
                              'sponsors' => $this->sponsors_model->get_sponsors(null, null, true, 10),
                              'players' => $this->players_model->get_players_by_team($plantel[0]['idPlantel']));
                              //'players' => $this->players_model->get_players(null, null, 'plantel/'.$slug, 1));

        $args['sponsors'] = $this->sponsors_model->get_sponsors();

        $this->load->view('components/header', $args);
        $this->load->view('plantel', $args);
        $this->load->view('components/footer', $args);

    }

    // CRUDS
    public function crud($action) {
        switch ($action) {
            case 'create':
                $data = [
                    'plantelName' => $this->input->post('plantelName'),
                    'plantelImage' => $this->input->post('plantelImage'),
                    'plantelSlug' => $this->input->post('plantelSlug'),
                    'plantelRegister' => date('Y-m-d H:i:s'),
                    'PlantelAvailable' => 1
                ];
                echo $this->planteles_model->create_plantel($data);
                break;
            case 'edit':
                $nameImage = $this->input->post('plantelImage');
                if ($nameImage == "" || $nameImage == null) {
                    $nameImage = $this->input->post('plantelImageName');
                } else {
                    unlink($_SERVER['DOCUMENT_ROOT'] . '/assets/img/planteles/' . trim($this->input->post('plantelImageName')));
                }
                $data = [
                    'plantelName' => $this->input->post('plantelName'),
                    'plantelImage' => $nameImage,
                    'plantelSlug' => $this->input->post('plantelSlug')
                ];
                $idPlantel = $this->input->post('idPlantel');
                echo $this->planteles_model->edit_plantel($data, $idPlantel);
                break;
            case 'delete':
                $idPlantel = $this->input->post('idPlantel');
                $plantelImage = $this->input->post('plantelImage');
                if ($plantelImage != "" || $plantelImage != " ") {
                    unlink($_SERVER['DOCUMENT_ROOT'] . '/assets/img/planteles/' . $plantelImage);
                }
                $players = $this->players_model->get_players($idPlantel);
                if (count($players) != 0) {
                    $fileRoute = $_SERVER['DOCUMENT_ROOT'] . '/assets/img/players/';
                    foreach ($players as $player) {
                        $playerImg = json_decode($player['playerPhotos'], true);
                        unlink($fileRoute . $playerImg['imgFrente']);
                        unlink($fileRoute . $playerImg['imgDetras']);
                    }
                }
                echo $this->planteles_model->delete_plantel($idPlantel);
                break;
            case 'image':
                $this->upload_image();
                break;
            case 'state':
                $data = [
                    'PlantelAvailable' => $this->input->post('PlantelAvailable')
                ];
                $idPlantel = $this->input->post('idPlantel');
                echo $this->planteles_model->edit_plantel($data, $idPlantel);
                break;
        }
    }

    public function upload_image() {
        $fileRoute = $_SERVER['DOCUMENT_ROOT'] . '/assets/img/planteles/';

        $config['upload_path'] = $fileRoute;
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['width'] = 1920;
        $config['height'] = 740;
        $config['encrypt_name'] = true;
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('plantelImage')) {
            echo $this->upload->display_errors();
        } else {
            echo $this->upload->data('file_name');
        }
    }

}
