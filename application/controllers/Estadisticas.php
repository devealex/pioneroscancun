<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Estadisticas extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('estadisticas_model');
        $this->load->helper(array('url','pages'));
    }

    // CRUDS
    public function crud($action) {
        switch ($action) {
            case 'edit':
                $idEstadistica = $this->input->post('idEstadistica');
                $type = $this->input->post('type');
                $value = (int) $this->input->post('value');
                $data = [
                    ''.$type.'' => $value
                ];
                echo $this->estadisticas_model->edit_estadistica($data, $idEstadistica);
                break;
        }
    }
}
