<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('users_model');
        $this->load->model('news_model');
        $this->load->helper(array('url','pages'));
    }

    // CRUDS
    public function crud($action) {
        switch ($action) {
            case 'create':
                $data = [
                    'userName' => $this->input->post('userName'),
                    'userMail' => $this->input->post('userMail'),
                    'userPassword' => md5($this->input->post('userPassword')),
                    'userRol' => $this->input->post('userRol'),
                    'userRegister' => date('Y-m-d H:i:s')
                ];
                echo $this->users_model->create_user($data);
                break;
            case 'edit':
                $userPassword = $this->input->post('userPassword');
                if ($userPassword != $this->input->post('lastPass')) {
                    $userPassword = md5($userPassword);
                }
                $data = [
                    'userName' => $this->input->post('userName'),
                    'userMail' => $this->input->post('userMail'),
                    'userPassword' => $userPassword,
                    'userRol' => $this->input->post('userRol')
                ];
                echo $this->users_model->edit_user($data, $this->input->post('idUser'));
                break;
            case 'delete':
                $idUser = $this->input->post('idUser');
                $this->delete_user($idUser);
                break;
        }
    }

    public function delete_user($idUser) {
        echo $this->users_model->delete_user($idUser);
    }

}
