<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Palmares extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('palmares_model');
        $this->load->helper(array('url','pages'));
    }

    public function traficFront() {
        $this->load->model('news_model');
        $this->load->model('planteles_model');
        $this->load->model('torneos_model');
        $args['torneos'] = $this->torneos_model->get_torneos();

        $args['page'] = array('section' => 'Palmares',
                              'title_anexo' => createStringSection('Palmares'),
                              'palmares' => $this->palmares_model->get_palmares(),
                              'planteles' => $this->planteles_model->get_planteles(),
                              'categories' => $this->news_model->get_categories());

        $this->load->model('sponsors_model');
        $args['sponsors'] = $this->sponsors_model->get_sponsors();
        
        $this->load->view('components/header', $args);
        $this->load->view('palmares', $args);
        $this->load->view('components/footer', $args);
    }

    // CRUDS
    public function crud($action) {
        switch ($action) {
            case 'create':
                $data = [
                    'cupName' => $this->input->post('cupName'),
                    'cupAvatar' => $this->input->post('cupAvatar'),
                    'championshipDate' => $this->input->post('championshipDate'),
                    'championshipDescription' => $this->input->post('championshipDescription'),
                    'championshipRegister' => date('Y-m-d H:i:s')
                ];
                echo $this->palmares_model->create_palmares($data);
                break;
            case 'edit':
                $nameAvatar = $this->input->post('cupAvatar');
                if ($nameAvatar == "" || $nameAvatar == null) {
                    $nameAvatar = $this->input->post('nameAvatar');
                } else {
                    if ($this->input->post('nameAvatar') != "" || $this->input->post('nameAvatar') != '') {
                        $fileRoute = $_SERVER['DOCUMENT_ROOT'] . '/assets/img/palmares/';
                        unlink($fileRoute . $this->input->post('nameAvatar'));
                    }
                }
                $data = [
                    'cupName' => $this->input->post('cupName'),
                    'cupAvatar' => $nameAvatar,
                    'championshipDate' => $this->input->post('championshipDate'),
                    'championshipDescription' => $this->input->post('championshipDescription')
                ];
                echo $this->palmares_model->edit_palmares($data, $this->input->post('idChampionship'));
                break;
            case 'delete':
                $idChampionship = $this->input->post('idChampionship');
                $fileRoute = $_SERVER['DOCUMENT_ROOT'] . '/assets/img/palmares/';
                unlink($fileRoute . $this->input->post('cupAvatar'));
                echo $this->palmares_model->delete_palmares($idChampionship);
                break;
            case 'image':
                $this->upload_image();
                break;
        }
    }

    public function upload_image() {
        $fileRoute = $_SERVER['DOCUMENT_ROOT'] . '/assets/img/palmares/';

        $config['upload_path'] = $fileRoute;
        $config['allowed_types'] = 'png';
        $config['encrypt_name'] = true;
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('cupAvatar')) {
            echo $this->upload->display_errors();
        } else {
            echo $this->upload->data('file_name');
        }
    }

}
