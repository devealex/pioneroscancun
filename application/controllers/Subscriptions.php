<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Subscriptions extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('subscriptions_model');
        $this->load->helper(array('url','pages'));
    }

    public function downloadExcel() {
        if ( isset($_POST['dateInitial']) && isset($_POST['dateFinal']) ) {
            $dateInitial = $this->input->post('dateInitial');
            $dateFinal = $this->input->post('dateFinal');
            if ( $this->input->post('dateInitial') != null && $this->input->post('dateFinal') != null ) {
                $args['subscriptions'] = $this->subscriptions_model->get_subscriptions($dateInitial, $dateFinal);
                $args['dateInitial'] = $dateInitial;
                $args['dateFinal'] = $dateFinal;
            }
            if ( $this->input->post('dateInitial') != null && $this->input->post('dateFinal') == null ) {
                $args['subscriptions'] = $this->subscriptions_model->get_subscriptions($dateInitial);
                $args['dateInitial'] = $dateInitial;
            }
            if ( $this->input->post('dateInitial') == null && $this->input->post('dateFinal') == null ) {
                $args['subscriptions'] = $this->subscriptions_model->get_subscriptions();
            }
        } else {
            $args['subscriptions'] = $this->subscriptions_model->get_subscriptions();
        }
        $this->load->view('manager/newsletter/newsletterList.php', $args);
    }

    public function subscribe() {
        $data = [
            'email' => $this->input->post('email'),
            'dateSubscription' => date('Y-m-d H:i:s')
        ];
        $this->sendMail($data);
        echo $this->subscriptions_model->create_subscription($data);
    }

    public function sendMail($data) {
        $body = '
        <div style="box-sizing:border-box;padding:20px;background:#fff;overflow:hidden;width:100%;font-family:sans-serif;">
            <div style="width:100%;box-sizing:border-box;padding:16px;background:#b23232;overflow:hidden;">
                <img src="https://upload.wikimedia.org/wikipedia/en/thumb/d/d7/Pioneros_Logo.png/200px-Pioneros_Logo.png" alt="pioneroscancunfc" style="max-height:150px;float:left;margin-right:100px;">
                <h1 style="float:left;font-size:30px;color:#fff;">GRACIAS POR SUSCRIBIRTE</h1>
            </div>
            <div style="width:100%;box-sizing:border-box;padding:20px;background:#fff;overflow:hidden;">
                <br>
                <strong>CORREO ELECTRONICO: </strong><p>'.$data['email'].'</p>
                <br>
            </div>
            <div style="width:100%;box-sizing:border-box;padding:16px;background:#b23232;overflow:hidden;">

            </div>
        </div>
        ';
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'ssl://mail.pioneroscancunfc.com';
        $config['smtp_port'] = 465;
        $config['smtp_timeout'] = '7';
        $config['smtp_user'] = 'hola@pioneroscancunfc.com';
        $config['smtp_pass'] = 'd3v3l0p3r';
        $config['mailtype'] = 'html';
        $config['charset'] = 'utf-8';
        $config['validation'] = FALSE;

        $this->load->library('email');
        $this->email->initialize($config);
        $this->email->from('hola@pioneroscancunfc.com', 'Administrador');
        $this->email->to($data['email']);
        $this->email->subject('Suscripción');
        $this->email->message($body);
        $this->email->send();
    }

}
