<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Pages extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('news_model');
        $this->load->model('planteles_model');
        $this->load->model('sponsors_model');
        $this->load->helper(array('url','pages'));
    }

    //Views
    public function view($page = 'index') {


        if (!file_exists(APPPATH.'views/'.$page.'.php')) {
            // Whoops, we don't have a page for that!
            show_404();
        }

        $this->load->model('torneos_model');
        $args['torneos'] = $this->torneos_model->get_torneos();


        //Load args
        $args['page'] = array('section' => $page,
                              'title_anexo' => createStringSection($page),
                              'categories' => $this->news_model->get_categories(),
                              'planteles' => $this->planteles_model->get_planteles()
                          );

        if ($page == 'index') {
            $this->load->model('videos_model');
            $this->load->model('gallery_model');
            $this->load->model('palmares_model');
            $args['news'] = $this->news_model->get_news(null, null, null, date('Y-m-d H:i:s'), 4);
            //print_r($args['news']);
            $args['videos'] = $this->videos_model->get_videos(null, date('Y-m-d'), 3);
            //print_r($args['videos']);
            $args['albums'] = $this->gallery_model->get_albums(null, 1, 4);
            //print_r($args['albums']);
            $args['palmares'] = $this->palmares_model->get_palmares(null, 5);
            //print_r($args['palmares']);
            $args['torneosPresentacion'] = [
                'ultimo' => $this->torneos_model->get_ultimo_proximo_partido(true, date('Y-m-d')),
                'proximo' => $this->torneos_model->get_ultimo_proximo_partido(false, date('Y-m-d'))
            ];
        }

        if ($page == 'estadisticas' || $page == 'calendario') {
            if (isset($_GET['torneo']) && $_GET['torneo'] != null) {
                $this->load->model('estadisticas_model');
                $this->load->model('calendar_model');
                $this->load->model('goleo_model');
                $args['goleo'] = $this->goleo_model->get_goleos(null, trim($_GET['torneo']));
                $args['estadisticas'] = $this->estadisticas_model->get_estadisticas(null,null,null, trim($_GET['torneo']));
                if ($page == 'estadisticas') {
                    $args['calendario'] = $this->calendar_model->get_calendario(null,null,null, trim($_GET['torneo']), 4);
                } else {
                    $args['calendario'] = $this->calendar_model->get_calendario(null,null,null, trim($_GET['torneo']));
                }
                //print_r($args['goleo']);
            }
        }

        $args['sponsors'] = $this->sponsors_model->get_sponsors();

        //Load views
        //if ($page != "index"){$this->load->view('components/header', $args);}
        $this->load->view('components/header', $args);
        $this->load->view($page, $args);
        $this->load->view('components/footer', $args);
        //if ($page !="index"){$this->load->view('components/footer', $args);}


    }


    //Login
    public function login() {
        //Load args
        $args['page'] = array('section' => 'login',
                              'title_anexo' => 'Acceso Administración');

        //SESSION
        if ($this->session->userdata('idUser') != null) {
            $userType = $this->session->userdata('userRol');
            if ($userType == 1 || $userType == 2) {
                header('Location: /manager/noticias/listview');
            } else if ($userType == 3) {
                header('Location: /manager/escuelas/listview');
            }
        }

        $this->load->view('manager/login', $args);

    }


}
