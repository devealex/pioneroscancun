<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Schools extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('schools_model');
        $this->load->model('news_model');
        $this->load->model('planteles_model');
        $this->load->helper(array('url','pages'));
    }

    //FRONT
    public function traficFront() {
        $this->load->model('torneos_model');
        $args['torneos'] = $this->torneos_model->get_torneos();
        
        $args['page'] = array('section' => 'Escuelas',
                              'title_anexo' => createStringSection('Escuelas'),
                              'content' => $this->schools_model->get_schools(null, true),
                              'news' => $this->news_model->get_news(null, null, null, date('Y-m-d H:i:s'), 4),
                              'planteles' => $this->planteles_model->get_planteles(),
                              'categories' => $this->news_model->get_categories());

        $this->load->model('sponsors_model');
        $args['sponsors'] = $this->sponsors_model->get_sponsors();
        
        $this->load->view('components/header', $args);
        $this->load->view('escuelas', $args);
        $this->load->view('components/footer', $args);
    }

    // CRUDS
    public function crud($action) {
        switch ($action) {
            case 'create':
                $data = [
                    'nameSchool' => $this->input->post('nameSchool'),
                    'representative' => $this->input->post('representative'),
                    'contactNumber' => $this->input->post('contactNumber'),
                    'email' => $this->input->post('email'),
                    'address' => $this->input->post('address'),
                    'registrationDate' => date('Y-m-d H:i:s')
                ];
                $this->sendMail($data);
                echo $this->schools_model->create_school($data);
                break;
            case 'edit':
                $data = [
                    'nameSchool' => $this->input->post('nameSchool'),
                    'representative' => $this->input->post('representative'),
                    'contactNumber' => $this->input->post('contactNumber'),
                    'email' => $this->input->post('email'),
                    'address' => $this->input->post('address'),
                    'facebook' => $this->input->post('facebook'),
                    'twitter' => $this->input->post('twitter'),
                    'instagram' => $this->input->post('instagram'),
                    'youtube' => $this->input->post('youtube')
                ];
                echo $this->schools_model->edit_school($data, $this->input->post('idSchool'));
                break;
            case 'state':
                $data = [
                    'state' => $this->input->post('state')
                ];
                echo $this->schools_model->edit_school($data, $this->input->post('idSchool'));
                break;
            case 'delete':
                $idSchool = $this->input->post('idSchool');
                $this->delete_school($idSchool);
                break;
        }
    }

    public function delete_school($idSchool) {
        echo $this->schools_model->delete_school($idSchool);
    }

    public function sendMail($data) {
        $body = '
        <div style="box-sizing:border-box;padding:20px;background:#fff;overflow:hidden;width:100%;font-family:sans-serif;">
            <div style="width:100%;box-sizing:border-box;padding:16px;background:#b23232;overflow:hidden;">
                <img src="https://upload.wikimedia.org/wikipedia/en/thumb/d/d7/Pioneros_Logo.png/200px-Pioneros_Logo.png" alt="pioneroscancunfc" style="max-height:150px;float:left;margin-right:100px;">
                <h1 style="float:left;font-size:30px;color:#fff;">GRACIAS POR REGISTRARTE</h1>
            </div>
            <div style="width:100%;box-sizing:border-box;padding:20px;background:#fff;overflow:hidden;">
                <br>
                <strong>Escuela: </strong><p>'.$data['nameSchool'].'</p>
                <br>
                <strong>Representante: </strong><p>'.$data['representative'].'</p>
                <br>
                <strong>Fecha Registro: </strong><p>'.$data['registrationDate'].'</p>
            </div>
            <div style="width:100%;box-sizing:border-box;padding:16px;background:#b23232;overflow:hidden;">

            </div>
        </div>
        ';
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'ssl://mail.pioneroscancunfc.com';
        $config['smtp_port'] = 465;
        $config['smtp_timeout'] = '7';
        $config['smtp_user'] = 'hola@pioneroscancunfc.com';
        $config['smtp_pass'] = 'd3v3l0p3r';
        $config['mailtype'] = 'html';
        $config['charset'] = 'utf-8';
        $config['validation'] = FALSE;

        $this->load->library('email');
        $this->email->initialize($config);
        $this->email->from('hola@pioneroscancunfc.com', 'Administrador');
        $this->email->to($data['email']);
        $this->email->subject('Registro');
        $this->email->message($body);
        $this->email->send();
    }

}
