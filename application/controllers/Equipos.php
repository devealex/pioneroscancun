<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Equipos extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('equipos_model');
        $this->load->helper(array('url','pages'));
    }

    // CRUDS
    public function crud($action) {
        switch ($action) {
            case 'create':
                $data = [
                    'equipoLogo' => $this->input->post('equipoLogo'),
                    'equipoName' => $this->input->post('equipoName')
                ];
                echo $this->equipos_model->create_equipo($data);
                break;
            case 'delete':
                $idEquipo = $this->input->post('idEquipo');
                $equipoLogo = $this->input->post('equipoLogo');
                if ($equipoLogo != "" || $equipoLogo != " ") {
                    unlink($_SERVER['DOCUMENT_ROOT'] . '/assets/img/equipos/' . $equipoLogo);
                }
                echo $this->equipos_model->delete_equipo($idEquipo);
                break;
            case 'edit':
                $equipoLogo = $this->input->post('equipoLogo');
                $idEquipo = $this->input->post('idEquipo');
                if ($equipoLogo == "" || $equipoLogo == null) {
                    $equipoLogo = $this->input->post('nameLogotype');
                } else {
                    if ($this->input->post('equipoLogo') != "" || $this->input->post('equipoLogo') != null) {
                        unlink($_SERVER['DOCUMENT_ROOT'] . '/assets/img/equipos/' . trim($this->input->post('nameLogotype')));
                    }
                }
                $data = [
                    'equipoLogo' => $equipoLogo,
                    'equipoName' => $this->input->post('equipoName')
                ];
                echo $this->equipos_model->edit_equipo($data, $idEquipo);
                break;
            case 'image':
                $this->upload_image();
                break;
        }
    }

    public function upload_image() {
        $fileRoute = $_SERVER['DOCUMENT_ROOT'] . '/assets/img/equipos/';

        $config['upload_path'] = $fileRoute;
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['encrypt_name'] = true;
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('equipoLogo')) {
            //echo $this->upload->display_errors();
            echo null;
        } else {
            echo $this->upload->data('file_name');
        }
    }

}
