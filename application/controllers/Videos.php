<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Videos extends CI_Controller {

    public function __construct() {
        parent::__construct();
        $this->load->model('videos_model');
        $this->load->helper(array('url','pages'));
    }

    public function traficFront() {
        $this->load->model('news_model');
        $this->load->model('planteles_model');
        $this->load->model('torneos_model');
        $args['torneos'] = $this->torneos_model->get_torneos();

        $args['page'] = array('section' => 'Galerias',
                              'title_anexo' => createStringSection('Galerias'),
                              'videos' => $this->videos_model->get_videos(null, date('Y-m-d')),
                              'planteles' => $this->planteles_model->get_planteles(),
                              'categories' => $this->news_model->get_categories());

        $this->load->model('sponsors_model');
        $args['sponsors'] = $this->sponsors_model->get_sponsors();

        $this->load->view('components/header', $args);
        $this->load->view('videos', $args);
        $this->load->view('components/footer', $args);
    }

    //CRUD
    public function crud($action) {
        switch ($action) {
            case 'create':
                $data = [
                    'videoIframe' => $this->input->post('videoIframe'),
                    'videoTitle' => $this->input->post('videoTitle'),
                    'videoAvatar' => $this->input->post('videoAvatar'),
                    'videoPublish' => $this->input->post('videoPublish')
                ];
                echo $this->videos_model->create_video($data);
                break;
            case 'edit':
                $nameAvatar = $this->input->post('videoAvatar');
                if ($nameAvatar == "" || $nameAvatar == null) {
                    $nameAvatar = $this->input->post('nameAvatar');
                } else {
                    if ($this->input->post('nameAvatar') != "" || $this->input->post('nameAvatar') != null) {
                        $fileRoute = $_SERVER['DOCUMENT_ROOT'] . '/assets/img/videos/';
                        unlink($fileRoute . $this->input->post('nameAvatar'));
                    }
                }
                $data = [
                    'videoIframe' => $this->input->post('videoIframe'),
                    'videoTitle' => $this->input->post('videoTitle'),
                    'videoAvatar' => $nameAvatar,
                    'videoPublish' => $this->input->post('videoPublish')
                ];
                echo $this->videos_model->edit_video($data, $this->input->post('idVideo'));
                break;
            case 'delete':
                $idVideo = $this->input->post('idVideo');
                $videoAvatar = $this->input->post('videoAvatar');
                $fileRoute = $_SERVER['DOCUMENT_ROOT'] . '/assets/img/videos/';
                unlink($fileRoute . $videoAvatar);
                echo $this->videos_model->delete_video($idVideo);
                break;
            case 'image':
                $this->upload_image();
                break;
        }
    }

    public function upload_image() {
        $fileRoute = $_SERVER['DOCUMENT_ROOT'] . '/assets/img/videos/';

        $config['upload_path'] = $fileRoute;
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['encrypt_name'] = true;
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('videoAvatar')) {
            echo $this->upload->display_errors();
        } else {
            echo $this->upload->data('file_name');
        }
    }

}
