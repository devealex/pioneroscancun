<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class players extends CI_Controller {

    public function __construct(){
        parent::__construct();
        $this->load->model('players_model');
        $this->load->model('planteles_model');
        $this->load->helper(array('url','pages'));
    }

    public function listview($idPlantel) {
        $args['section'] = 'Jugadores';
        $args['idPlantel'] = $idPlantel;
        $args['plantelName'] = $this->planteles_model->get_planteles($idPlantel)[0]['plantelName'];

        if ($this->session->userdata('idUser') != null) {
            $args['userType'] = $this->session->userdata('userRol');
        } else {
            header('Location: /manager');
        }

        if (!file_exists(APPPATH.'views/manager/players/listview.php')) {
            show_404();
        }

        $args['players'] = $this->players_model->get_players($idPlantel);

        $this->load->view('manager/components/header', $args);
        $this->load->view('manager/players/listview', $args);
        $this->load->view('manager/components/footer', $args);
    }

    public function editView($idPlayer, $idPlantel) {
        $args['section'] = 'Modificar Jugador';
        $args['action'] = 'edit';
        $args['idPlantel'] = $idPlantel;
        $args['plantelName'] = $this->planteles_model->get_planteles($idPlantel)[0]['plantelName'];

        if ($this->session->userdata('idUser') != null) {
            $args['userType'] = $this->session->userdata('userRol');
        } else {
            header('Location: /manager');
        }

        if (!file_exists(APPPATH.'views/manager/players/form.php')) {
            show_404();
        }

        $args['planteles'] = $this->planteles_model->get_planteles();
        $args['player_item'] = $this->players_model->get_players(null, $idPlayer)[0];

        $this->load->view('manager/components/header', $args);
        $this->load->view('manager/players/form', $args);
        $this->load->view('manager/components/footer', $args);
    }

    public function createView($idPlantel) {
        $args['section'] = 'Crear Jugador';
        $args['action'] = 'create';
        $args['idPlantel'] = $idPlantel;
        $args['plantelName'] = $this->planteles_model->get_planteles($idPlantel)[0]['plantelName'];

        if ($this->session->userdata('idUser') != null) {
            $args['userType'] = $this->session->userdata('userRol');
        } else {
            header('Location: /manager');
        }

        if (!file_exists(APPPATH.'views/manager/players/form.php')) {
            show_404();
        }

        $args['planteles'] = $this->planteles_model->get_planteles();

        $this->load->view('manager/components/header', $args);
        $this->load->view('manager/players/form', $args);
        $this->load->view('manager/components/footer', $args);
    }

    public function crud($action) {
        switch ($action) {
            case 'create':
                $img = '';
                if ($this->input->post('imgFrente') != '' && $this->input->post('imgDetras') != '') {
                    $img = json_encode(array('imgFrente' => $this->input->post('imgFrente'), 'imgDetras' => $this->input->post('imgDetras')));
                }
                if ($this->input->post('imgFrente') == '' && $this->input->post('imgDetras') == '') {
                    $img = json_encode(array('imgFrente' => $this->input->post('imgFrenteName'), 'imgDetras' => $this->input->post('imgDetrasName')));
                }
                $data = [
                    'playerName' => $this->input->post('playerName'),
                    'playerNumber' => $this->input->post('playerNumber'),
                    'plantel' => $this->input->post('plantel'),
                    'playerPosition' => $this->input->post('playerPosition'),
                    'playerFunction' => $this->input->post('playerFunction'),
                    'playerPhotos' => $img,
                    'playerHeight' => $this->input->post('playerHeight'),
                    'playerOrigin' => $this->input->post('playerOrigin'),
                    'previousTeam' => $this->input->post('previousTeam'),
                    'playerRegister' => date('Y-m-d H:i:s')
                ];
                echo $this->players_model->create_player($data);
                break;
            case 'edit':
                $img = '';
                if ($this->input->post('imgFrente') != '' && $this->input->post('imgDetras') != '') {
                    $img = json_encode(array('imgFrente' => $this->input->post('imgFrente'), 'imgDetras' => $this->input->post('imgDetras')));
                }
                if ($this->input->post('imgFrente') == '' && $this->input->post('imgDetras') == '') {
                    $img = json_encode(array('imgFrente' => $this->input->post('imgFrenteName'), 'imgDetras' => $this->input->post('imgDetrasName')));
                }
                $data = [
                    'playerName' => $this->input->post('playerName'),
                    'playerNumber' => $this->input->post('playerNumber'),
                    'plantel' => $this->input->post('plantel'),
                    'playerPosition' => $this->input->post('playerPosition'),
                    'playerFunction' => $this->input->post('playerFunction'),
                    'playerPhotos' => $img,
                    'playerHeight' => $this->input->post('playerHeight'),
                    'playerOrigin' => $this->input->post('playerOrigin'),
                    'previousTeam' => $this->input->post('previousTeam')
                ];
                echo $this->players_model->edit_player($data, $this->input->post('idPlayer'));
                break;
            case 'delete':
                unlink($_SERVER['DOCUMENT_ROOT'] . '/assets/img/players/' . $this->input->post('imgFrente'));
                unlink($_SERVER['DOCUMENT_ROOT'] . '/assets/img/players/' . $this->input->post('imgDetras'));
                $idPlayer = $this->input->post('idPlayer');
                echo $this->players_model->delete_player($idPlayer);
                break;
            case 'images':
                $imgFrente = $this->upload_images('imgFrente');
                $imgDetras = $this->upload_images('imgDetras');
                if ($imgFrente != null && $this->input->post('imgFrenteName') != "") {
                    unlink($_SERVER['DOCUMENT_ROOT'] . '/assets/img/players/' . $this->input->post('imgFrenteName'));
                }
                if ($imgDetras != null && $this->input->post('imgDetrasName') != "") {
                    unlink($_SERVER['DOCUMENT_ROOT'] . '/assets/img/players/' . $this->input->post('imgDetrasName'));
                }
                echo json_encode(array('imgFrente' => $imgFrente, 'imgDetras' => $imgDetras));
                break;
            case 'state':
                $data = [
                    'playerAvailable' => $this->input->post('playerAvailable')
                ];
                echo $this->players_model->edit_player($data, $this->input->post('idPlayer'));
                break;
        }
    }

    public function upload_images($input) {
        $fileRoute = $_SERVER['DOCUMENT_ROOT'] . '/assets/img/players/';

        $config['upload_path'] = $fileRoute;
        $config['allowed_types'] = 'jpg|jpeg|png|gif';
        $config['width'] = 350;
        $config['height'] = 395;
        $config['encrypt_name'] = true;
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload($input)) {
            return null;
        } else {
            return $this->upload->data('file_name');
        }
    }

}
