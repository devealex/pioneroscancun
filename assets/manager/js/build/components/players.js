$(document).ready(function(){
    var players_app = {
        'path' : window.location.protocol + '//' + window.location.hostname + '/',
        'urlocation' : window.location.pathname,
        'crud_endpoint' : 'manager/jugadores/crud/'
    };

    //Control de player input funcion
    $('select[name="playerPosition"]').on('change', function(){
        if ($(this).val() == 5) {
            $('.playerFunction-label').css('display', 'initial');
            $('input[name="playerFunction"]').css('display', 'initial');
        } else {
            $('.playerFunction-label').css('display', 'none');
            $('input[name="playerFunction"]').css('display', 'none');
            $('input[name="playerFunction"]').val('');
        }
    });

    //Eliminar Jugador
    $('.del-player').on('click', function(){
        if (confirm("¿Desea de ELIMINAR este jugador?")){
            var obj = createObjRequest();
            var action = 'delete';
            obj.datastore = {
                'idPlayer' : $(this).data('idplayer'),
                'imgFrente' : $(this).data('imgfrente'),
                'imgDetras' : $(this).data('imgdetras')
            };
            obj.endpoint = players_app.path + players_app.crud_endpoint + action;
            obj.notification.reload = true;
            obj.action = action;
            obj.debug = true;
            managementAjaxRequest(obj);
        }
    });

    //Status del jugador
    $('input[name="playerAvailable"]').on('change', function(e){
        e.preventDefault();
        var idPlayer = $(this).data('idplayer');
        var state = $(this).val();
        if (state == 1) {
            state = 0;
        } else if (state == 0) {
            state = 1;
        }
        $(this).val(state);
        var obj = createObjRequest();
        var action = 'state';
        obj.datastore = {'idPlayer' : idPlayer, 'playerAvailable' : state};
        obj.endpoint = players_app.path + players_app.crud_endpoint + action;
        obj.notification.reload = false;
        obj.action = action;
        obj.debug = true;
        managementAjaxRequest(obj);
    });

    //Show image preview
    $('input[name="imgDetras"]').on('change', function(){
        idPreview = '#prev-detras';
        filePreview(this, idPreview);
    });

    $('input[name="imgFrente"]').on('change', function(){
        idPreview = '#prev-frente';
        filePreview(this, idPreview);
    });

    function filePreview(input, idPreview) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $(idPreview).attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        } else {
            $(idPreview).attr('src', "/assets/img/players/default.jpg");
        }
    }

    //FORM TO PLAYERS
    $('form[id="form-player"]').on('submit', function(e){
        e.preventDefault();

        var action = $('input[name="action"]').val();
        var idPlantel = $('input[name="idPlantel"]').val();
        var data = managementFormData($(this));
        var obj = createObjRequest();

        // console.log(data);

        obj.datastore =  data;
        obj.endpoint = players_app.path + players_app.crud_endpoint + action;
        obj.endLocation = players_app.path + 'manager/jugadores/listview/'+idPlantel;
        obj.action = action;
        obj.notification.reload = true;
        obj.debug = true;

        if (data.playerName == '' || data.playerName.length == 0) {
            $('input[name="playerName"]').addClass('is-invalid');
            $('input[name="playerName"]').focus();
        } else if (data.playerHeight == '' || data.playerHeight.length == 0) {
            $('input[name="playerName"]').removeClass('is-invalid');
            $('input[name="playerNumber"]').removeClass('is-invalid');
            $('input[name="playerOrigin"]').removeClass('is-invalid');
            $('input[name="playerFunction"]').removeClass('is-invalid');
            $('input[name="playerHeight"]').addClass('is-invalid');
            $('input[name="playerHeight"]').focus();
        } else if (data.playerNumber == '' || data.playerNumber.length == 0) {
            $('input[name="playerName"]').removeClass('is-invalid');
            $('input[name="playerHeight"]').removeClass('is-invalid');
            $('input[name="playerOrigin"]').removeClass('is-invalid');
            $('input[name="playerFunction"]').removeClass('is-invalid');
            $('input[name="playerNumber"]').addClass('is-invalid');
            $('input[name="playerNumber"]').focus();
        } else if (data.playerOrigin == '' || data.playerOrigin.length == 0) {
            $('input[name="playerName"]').removeClass('is-invalid');
            $('input[name="playerHeight"]').removeClass('is-invalid');
            $('input[name="playerNumber"]').removeClass('is-invalid');
            $('input[name="playerFunction"]').removeClass('is-invalid');
            $('input[name="playerOrigin"]').addClass('is-invalid');
            $('input[name="playerOrigin"]').focus();
        } else if (data.playerPosition ==  5 && data.playerFunction.trim() == '') {
            $('input[name="playerName"]').removeClass('is-invalid');
            $('input[name="playerHeight"]').removeClass('is-invalid');
            $('input[name="playerNumber"]').removeClass('is-invalid');
            $('input[name="playerOrigin"]').removeClass('is-invalid');
            $('input[name="playerFunction"]').addClass('is-invalid');
            $('input[name="playerFunction"]').focus();
        } else {
            if (action == 'create') {
                if (data.imgFrente == "" || data.imgDetras == "") {
                    alert('Seleccionar las dos imagenes del jugador');
                    $('input[name="playerName"]').removeClass('is-invalid');
                    $('input[name="playerHeight"]').removeClass('is-invalid');
                    $('input[name="playerNumber"]').removeClass('is-invalid');
                    $('input[name="playerOrigin"]').removeClass('is-invalid');
                    $('input[name="playerFunction"]').removeClass('is-invalid');
                } else {
                    addPlayersFiles(this);
                }
            }

            if (action == 'edit') {
                if (data.imgFrente == "" && data.imgDetras == "") {
                    managementAjaxRequest(obj);
                } else {
                    addPlayersFiles(this);
                }
            }
        }
    });

    function addPlayersFiles(form) {
        var data = new FormData(form);
        var url = players_app.path + players_app.crud_endpoint + 'images';
        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            contentType: false,
            cache: false,
            processData:false,
            success: function(rs) {
                var res = eval ('('+rs+')');
                // console.log('Frente: ' + res.imgFrente);
                // console.log('Detras: ' + res.imgDetras);
                var data = managementFormData(form);
                var action = $('input[name="action"]').val();
                var obj = createObjRequest();
                var idPlantel = $('input[name="idPlantel"]').val();

                if (res.imgFrente == null) {
                    data['imgFrente'] = $('input[name="imgFrenteName"]').val();
                } else {
                    data['imgFrente'] = res.imgFrente;
                }
                if (res.imgDetras == null) {
                    data['imgDetras'] = $('input[name="imgDetrasName"]').val();
                } else {
                    data['imgDetras'] = res.imgDetras;
                }

                // console.log(action);
                // console.log(data);

                //Add data in object
                obj.datastore =  data;
                obj.endpoint = players_app.path + players_app.crud_endpoint + action;
                obj.endLocation = players_app.path + 'manager/jugadores/listview/'+idPlantel;
                obj.action = action;
                obj.notification.reload = true;
                obj.debug = true;
                //Send the ajax
                managementAjaxRequest(obj);
             }
        });
    }

});
