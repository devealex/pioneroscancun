$(document).ready(function(){
    var planteles_app = {
        'path' : window.location.protocol + '//' + window.location.hostname + '/',
        'urlocation' : window.location.pathname,
        'crud_endpoint' : 'manager/planteles/crud/'
    };

    //DELETE PLANTEL
    $('.del-plantel').on('click', function(){
        if (confirm("¿Desea de ELIMINAR este plantel?\r\nTodos los datos asociados a este plantel seran borrados.")){
            var obj = createObjRequest();
            var action = 'delete';
            obj.datastore = {
                'idPlantel' : $(this).data('idplantel'),
                'plantelImage' : $(this).data('plantelimage')
            };
            obj.endpoint = planteles_app.path + planteles_app.crud_endpoint + action;
            obj.notification.reload = true;
            obj.action = action;
            obj.debug = true;
            managementAjaxRequest(obj);
        }
    });

    //Status del plantel
    $('input[name="PlantelAvailable"]').on('change', function(e){
        e.preventDefault();
        var idPlantel = $(this).data('idplantel');
        var state = $(this).val();
        if (state == 1) {
            state = 0;
        } else if (state == 0) {
            state = 1;
        }
        $(this).val(state);
        var obj = createObjRequest();
        var action = 'state';
        obj.datastore = {'idPlantel' : idPlantel, 'PlantelAvailable' : state};
        obj.endpoint = planteles_app.path + planteles_app.crud_endpoint + action;
        obj.notification.reload = false;
        obj.action = action;
        obj.debug = true;
        managementAjaxRequest(obj);
    });

    //PLANTEL URL
    $('input[name="plantelName"]').on('keyup', function(){
        makeSlugPlantel();
    });

    function makeSlugPlantel() {
        //Rules to title in URL
        var title = $('input[name="plantelName"]').val().toLowerCase().trim();
        title = title.replace(/ /g, '-');
        title = title.replace(/[^a-zA-Z0-9-]/g, '');
        title = title.replace(/----/g, '-');
        title = title.replace(/---/g, '-');
        title = title.replace(/--/g, '-');
        //Show the URL
        $('input[name="plantelSlug"]').val("plantel/" + title);
    }

    //Show image preview
    $('input[name="plantelImage"]').on('change', function(){
        filePreview(this);
    });

    function filePreview(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('.img-preview').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        } else {
            $('.img-preview').attr('src', "/assets/img/planteles/default.jpg");
        }
    }

    //FORM TO PLANTELES
    $('form[id="form-plantel"]').on('submit', function(e){
        e.preventDefault();

        var action = $('input[name="action"]').val();
        var data = managementFormData($(this));
        var obj = createObjRequest();

        // console.log(data);

        obj.datastore =  data;
        obj.endpoint = planteles_app.path + planteles_app.crud_endpoint + action;
        obj.endLocation = planteles_app.path + 'manager/planteles/listview';
        obj.action = action;
        obj.notification.reload = true;
        obj.debug = true;

        if (data.plantelName == '' || data.plantelName.length == 0) {
            $('input[name="plantelName"]').addClass('is-invalid');
            $('input[name="plantelName"]').focus();
        } else if (action == "create" && data.plantelImage == "") {
            alert("Selecciona una imagen");
            $('input[name="plantelImage"]').focus();
        } else {
            if (data.plantelImage != "") {
                addNewFile(this);
            } else {
                managementAjaxRequest(obj);
            }
        }

    });

    function addNewFile(form) {
        var data = new FormData(form);
        var url = planteles_app.path + planteles_app.crud_endpoint + 'image';
        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            contentType: false,
            cache: false,
            processData:false,
            success: function(rs) {
                var data = managementFormData(form);
                var obj = createObjRequest();
                var action = $('input[name="action"]').val();
                data['plantelImage'] = rs;
                //Add data in object
                obj.datastore =  data;
                obj.endpoint = planteles_app.path + planteles_app.crud_endpoint + action;
                obj.endLocation = planteles_app.path + 'manager/planteles/listview';
                obj.action = action;
                obj.notification.reload = true;
                obj.debug = true;
                //Send the ajax
                managementAjaxRequest(obj);
             }
        });
    }

});
