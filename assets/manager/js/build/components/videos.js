$(document).ready(function(){
    var videos_app = {
        'path' : window.location.protocol + '//' + window.location.hostname + '/',
        'urlocation' : window.location.pathname,
        'crud_endpoint' : 'manager/videos/crud/'
    };

    //DELETE Video
    $('.del-video').on('click', function(){
        if (confirm("¿Desea de ELIMINAR este video?")) {
            var obj = createObjRequest();
            var action = 'delete';
            obj.datastore = {'idVideo' : $(this).data('idvideo'), 'videoAvatar' : $(this).data('videoavatar')};
            obj.endpoint = videos_app.path + videos_app.crud_endpoint + action;
            obj.notification.reload = true;
            obj.action = action;
            obj.debug = true;
            managementAjaxRequest(obj);
        }
    });

    //Show image preview
    $('input[name="videoAvatar"]').on('change', function(){
        filePreview(this);
    });

    function filePreview(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('.img-preview').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        } else {
            $('.img-preview').attr('src', "/assets/img/videos/default.jpg");
        }
    }

    //FORM TO videos
    $('form[id="form-video"]').on('submit', function(e){
        e.preventDefault();

        var action = $('input[name="action"]').val();
        var data = managementFormData($(this));
        var rs = true;
        var obj = createObjRequest();

        console.log(data);

        obj.datastore =  data;
        obj.endpoint = videos_app.path + videos_app.crud_endpoint + action;
        obj.endLocation = videos_app.path + 'manager/videos/listview';
        obj.action = action;
        obj.notification.reload = true;
        obj.debug = true;

        if (data.videoTitle == null || data.videoTitle.length == 0) {
            $('input[name="videoTitle"]').addClass('is-invalid');
            $('input[name="videoTitle"]').focus();
            rs = false;
        } else {
            $('input[name="videoTitle"]').removeClass('is-invalid');
        }
        if (data.videoIframe == null || data.videoIframe.length == 0) {
            $('input[name="videoIframe"]').addClass('is-invalid');
            $('input[name="videoIframe"]').focus();
            rs = false;
        } else {
            $('input[name="videoIframe"]').removeClass('is-invalid');
        }
        if (data.videoPublish == null || data.videoPublish.length == 0) {
            $('input[name="videoPublish"]').addClass('is-invalid');
            $('input[name="videoPublish"]').focus();
            rs = false;
        } else {
            $('input[name="videoPublish"]').removeClass('is-invalid');
        }
        if (data.videoAvatar == "" && action == 'create') {
            rs = false;
            alert('Debes elegir una imagen');
        }

        if (rs == true){
            if (data.videoAvatar != "") {
                addNewFile(this);
            } else {
                managementAjaxRequest(obj);
            }
        }

    });

    function addNewFile(form) {
        var data = new FormData(form);
        var url = videos_app.path + 'manager/videos/crud/image';
        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            contentType: false,
            cache: false,
            processData:false,
            success: function(rs) {
                var data = managementFormData(form);
                var obj = createObjRequest();
                var action = $('input[name="action"]').val();
                data['videoAvatar'] = rs;
                //Add data in object
                obj.datastore =  data;
                obj.endpoint = videos_app.path + videos_app.crud_endpoint + action;
                obj.endLocation = videos_app.path + 'manager/videos/listview';
                obj.action = action;
                obj.notification.reload = true;
                obj.debug = true;
                //Send the ajax
                managementAjaxRequest(obj);
             }
        });
    }

});
