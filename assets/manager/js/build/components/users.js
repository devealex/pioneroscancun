$(document).ready(function(){

    var users_app = {
        'path' : window.location.protocol + '//' + window.location.hostname + '/',
        'urlocation' : window.location.pathname,
        'crud_endpoint' : 'manager/usuarios/crud/'
    };

    //FORM LOGIN
    $('form[name="frmLogin"]').on('submit', function(e){
        e.preventDefault();

        var data = $(this).serializeArray();

        $.ajax({
            url: users_app.path + 'manager/login',
            type: 'POST',
            data: data,
            success: function(data) {
                var res = eval ('('+data+')');
                if (res.rs == true) {
                    if (res.userType == 1 || res.userType == 2) {
                        window.location = users_app.path + 'manager/noticias/listview';
                    } else if (res.userType == 3) {
                        window.location = users_app.path + 'manager/escuelas/listview';
                    }
                } else if (res.rs == false) {
                    showNotificacion('Usuario y/o Contraseña Incorrecto', 'warning');
                }
            }
        });

    });

    //FORM USERS
    $('form[id="form-user"]').on('submit', function(e){

        e.preventDefault();
        var action = $('input[name="action"]').val();
        var data = managementFormData($(this));
        var obj = createObjRequest();

        obj.datastore =  data;
        obj.endpoint = users_app.path + users_app.crud_endpoint + action;
        obj.endLocation = users_app.path + 'manager/usuarios/listview';
        obj.action = action;
        obj.notification.reload = true;
        obj.debug = true;

        if (data.userName == '' || data.userName.length == 0) {
            $('input[name="userName"]').addClass('is-invalid');
            $('input[name="userName"]').focus();
        } else if (data.userMail == '' || data.userMail.length == 0) {
            $('input[name="userMail"]').addClass('is-invalid');
            $('input[name="userMail"]').focus();
        } else if (data.userPassword == '' || data.userPassword.length == 0 || data.userPassword < 8) {
            $('input[name="userPassword"]').addClass('is-invalid');
            $('input[name="userPassword"]').focus();
        } else {
            managementAjaxRequest(obj);
        }
    });

    //DELETE USER
    $('.del-user').on('click', function(){
        if (confirm("¿Desea de ELIMINAR este usuario?")){
            var obj = createObjRequest();
            var action = 'delete';
            obj.datastore = {'idUser' : $(this).data('iduser')};
            obj.endpoint = users_app.path + users_app.crud_endpoint + action;
            obj.notification.reload = true;
            obj.action = action;
            obj.debug = true;
            managementAjaxRequest(obj);
        }
    });

});
