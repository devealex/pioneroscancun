$(document).ready(function(){

    var news_app = {
        'path' : window.location.protocol + '//' + window.location.hostname + '/',
        'urlocation' : window.location.pathname,
        'crud_endpoint' : 'manager/noticias/crud/'
    };

    //DELETE NEW
    $('.del-new').on('click', function(){
        if (confirm("¿Desea de ELIMINAR esta noticia?")){
            var obj = createObjRequest();
            var action = 'delete';
            obj.datastore = {
                'idnew' : $(this).data('idnew'),
                'newsAvatar' : $(this).data('newsavatar')
            };
            obj.endpoint = news_app.path + news_app.crud_endpoint + action;
            obj.notification.reload = true;
            obj.action = action;
            obj.debug = true;
            managementAjaxRequest(obj);
        }
    });

    //FORM TO News
    $('form[id="form-new"]').on('submit', function(e){
        e.preventDefault();

        var action = $('input[name="action"]').val();
        var data = managementFormData($(this));
        var quill = new Quill('#newsContent');
        var newsContent = quill.root.innerHTML;
        var rs = true;
        var obj = createObjRequest();
        data['newsContent'] = newsContent;

        console.log(data);

        obj.datastore =  data;
        obj.endpoint = news_app.path + news_app.crud_endpoint + action;
        obj.endLocation = news_app.path + 'manager/noticias/listview';
        obj.action = action;
        obj.notification.reload = true;
        obj.debug = true;

        if (data.newsTitle == null || data.newsTitle.length == 0) {
            $('input[name="newsTitle"]').addClass('is-invalid');
            $('input[name="newsTitle"]').focus();
            rs = false;
        } else {
            $('input[name="newsTitle"]').removeClass('is-invalid');
        }

        if (data.newsPublished == null || data.newsPublished.length == 0) {
            $('input[name="newsPublished"]').addClass('is-invalid');
            $('input[name="newsPublished"]').focus();
            rs = false;
        } else {
            $('input[name="newsPublished"]').removeClass('is-invalid');
        }

        if (data.newsContent == null || data.newsContent.length == 0 || /^\s+$/.test(data.newsContent) || data.newsContent == "<p><br></p>") {
            $('.focus-content').addClass('is-invalid');
            $('.focus-content').focus();
            rs = false;
        } else {
            $('.focus-content').removeClass('is-invalid');
        }

        if (rs == true){
            if (data.newsAvatar != "") {
                addNewFile(this);
            } else {
                managementAjaxRequest(obj);
            }
        }

    });

    function addNewFile(form) {
        var data = new FormData(form);
        var url = news_app.path + 'manager/noticias/crud/image';
        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            contentType: false,
            cache: false,
            processData:false,
            success: function(rs) {
                var data = managementFormData(form);
                var obj = createObjRequest();
                var action = $('input[name="action"]').val();
                var quill = new Quill('#newsContent');
                var newsContent = quill.root.innerHTML;
                data['newsContent'] = newsContent;
                data['newsAvatar'] = rs;
                //Add data in object
                obj.datastore =  data;
                obj.endpoint = news_app.path + news_app.crud_endpoint + action;
                obj.endLocation = news_app.path + 'manager/noticias/listview';
                obj.action = action;
                obj.notification.reload = true;
                obj.debug = true;
                //Send the ajax
                managementAjaxRequest(obj);
             }
        });
    }

    //NEWS URL
    $('input[name="newsTitle"]').on('keyup', function(){
        makeUrlNew();
    });

    $('select[name="newsCategory"]').on('change', function(){
        makeUrlNew();
    });

    function makeUrlNew() {
        //Rules to category in URL
        var category = $('select[name="newsCategory"] option:selected').text().toLowerCase().trim();
        category = category.replace(/ /g, '-');
        category = category.replace(/[^a-zA-Z0-9-]/g, '');
        category = category.replace(/----/g, '-');
        category = category.replace(/---/g, '-');
        category = category.replace(/--/g, '-');
        //Rules to title in URL
        var title = $('input[name="newsTitle"]').val().toLowerCase().trim();
        title = title.replace(/ /g, '-');
        title = title.replace(/[^a-zA-Z0-9-]/g, '');
        title = title.replace(/----/g, '-');
        title = title.replace(/---/g, '-');
        title = title.replace(/--/g, '-');
        //Show the URL
        $('input[name="newsUrl"]').val("noticias/" + category + "/" + title);
    }

    //Show image preview
    $('input[name="newsAvatar"]').on('change', function(){
        filePreview(this);
    });

    function filePreview(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('.img-preview').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        } else {
            $('.img-preview').attr('src', "/assets/img/news/default.jpg");
        }
    }

    //Categories
    $('.btn-edit-category').on('click', function(){
        $('input[name="category-'+$(this).data("id")+'"]').addClass('inp-category-border');
        $('.btn-edit-category').attr('disabled', 'disabled');
        $('input[name="add-category"]').attr('disabled', 'disabled');
        $('.btn-delete-category').attr('disabled', 'disabled');
        $('input[name="inp-new-category"]').attr('disabled', 'disabled');
        $('input[name="category-'+$(this).data("id")+'"]').removeAttr('disabled');
    });

    $('.btn-delete-category').on('click', function(){
        data = {
            'idCategory' : $(this).data("id")
        };
        var categoria = $('input[name="category-'+$(this).data("id")+'"]').val();
        if (confirm("Desea eliminar la categoria "+categoria+"?\r\nTodos las noticias asociadas a esta categoria seran borradas.")) {
            $.ajax({
                url: news_app.path + 'manager/categories/crud/delete',
                type: 'POST',
                data: data,
                success: function(rs) {
                    var res = eval ('('+rs+')');
                    alert(res.msg);
                    location.reload();
                },
                error: function (xhr, ajaxOptions, thrownError) {
                    alert("Hubo un error al eliminar la categoria. (Intentelo mas tarde).\r\n" + thrownError);
                }
            });
        }
    });

    $('input[class="inp-category"]').on('focusout', function(){
        data = {
            'category' : $(this).val(),
            'idCategory' : $(this).data("id")
        };
        var slug = $(this).val().trim();
        slug = slug.replace(/ /g, '-');
        slug = slug.replace(/[^a-zA-Z0-9-]/g, '');
        slug = slug.replace(/----/g, '-');
        slug = slug.replace(/---/g, '-');
        slug = slug.replace(/--/g, '-');
        slug = slug.toLowerCase();
        data['slug'] = "noticias/" + slug;
        if (data.category == "" || data.category.length == 0){
            $(this).focus();
        } else {
            $.ajax({
                url: news_app.path + 'manager/categories/crud/edit',
                type: 'POST',
                data: data,
                success: function(rs) {
                    console.log(rs);
                    $('.btn-edit-category').removeAttr('disabled');
                    $('input[name="add-category"]').removeAttr('disabled');
                    $('.btn-delete-category').removeAttr('disabled');
                    $('input[name="inp-new-category"]').removeAttr('disabled');
                }
            });
            $(this).attr('disabled', 'disabled');
            $(this).removeClass('inp-category-border');
        }
    });

    $('input[name="add-category"]').on('click', function(){
        var category = $('input[name="inp-new-category"]').val().trim();
        data = {
            'category' : category
        };
        var slug = category;
        slug = slug.replace(/ /g, '-');
        slug = slug.replace(/[^a-zA-Z0-9-]/g, '');
        slug = slug.replace(/----/g, '-');
        slug = slug.replace(/---/g, '-');
        slug = slug.replace(/--/g, '-');
        slug = slug.toLowerCase();
        data['slug'] = "noticias/" + slug;
        if (data.category == "" || data.category.length == 0){
            $('input[name="inp-new-category"]').focus();
        } else {
            $.ajax({
                url: news_app.path + 'manager/categories/crud/create',
                type: 'POST',
                data: data,
                success: function(rs) {
                    var res = eval ('('+rs+')');
                    alert(res.msg);
                    location.reload();
                }
            });
        }
    });

});
