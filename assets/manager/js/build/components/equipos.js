$(document).ready(function(){
    var equipos_app = {
        'path' : window.location.protocol + '//' + window.location.hostname + '/',
        'urlocation' : window.location.pathname,
        'crud_endpoint' : 'manager/equipos/crud/'
    };

    //DELETE NEW
    $('.del-equipo').on('click', function(){
        if (confirm("¿Desea de ELIMINAR este Equipo?\r\nTodos los datos asociados a este equipo seran borrados.")){
            var obj = createObjRequest();
            var action = 'delete';
            obj.datastore = {
                'idEquipo' : $(this).data('idequipo'),
                'equipoLogo' : $(this).data('equipologo')
            };
            obj.endpoint = equipos_app.path + equipos_app.crud_endpoint + action;
            obj.notification.reload = true;
            obj.action = action;
            obj.debug = true;
            managementAjaxRequest(obj);
        }
    });

    //FORM TO News
    $('form[id="form-equipo"]').on('submit', function(e){
        e.preventDefault();

        var data = managementFormData($(this));
        var rs = true;

        console.log(data);

        if (data.equipoName == null || data.equipoName.length == 0) {
            $('input[name="equipoName"]').addClass('is-invalid');
            $('input[name="equipoName"]').focus();
            rs = false;
        } else {
            $('input[name="equipoName"]').removeClass('is-invalid');
        }

        if (data.equipoLogo == null && data.action == 'create' || data.equipoLogo.length == 0 && data.action == 'create') {
            alert('Seleciona una imagen');
            rs = false;
        }

        if (rs == true){
            addNewFile(this);
        }

    });

    function addNewFile(form) {
        var data = new FormData(form);
        var url = equipos_app.path + 'manager/equipos/crud/image';
        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            contentType: false,
            cache: false,
            processData:false,
            success: function(rs) {
                var data = managementFormData(form);
                var obj = createObjRequest();
                var action = $('input[name="action"]').val();
                data['equipoLogo'] = rs;
                //Add data in object
                obj.datastore =  data;
                obj.endpoint = equipos_app.path + equipos_app.crud_endpoint + action;
                obj.endLocation = equipos_app.path + 'manager/equipos/listview';
                obj.action = action;
                obj.notification.reload = true;
                obj.debug = true;
                //Send the ajax
                managementAjaxRequest(obj);
             }
        });
    }

    //Show image preview
    $('input[name="equipoLogo"]').on('change', function(){
        filePreview(this);
    });

    function filePreview(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('.img-preview').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        } else {
            $('.img-preview').attr('src', "/assets/img/equipos/default.jpg");
        }
    }

});