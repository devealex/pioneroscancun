var notificacionNumber = 1;

function showNotificacion(mensaje, type) {
    var notNum = notificacionNumber;
    $('html').append('<span id="not-'+notNum+'" class="notificacion '+type+' notificacionShow"><p>'+mensaje+'</p></span>');
    notificacionNumber++;
    setTimeout(function(){
        $('span#not-'+notNum).removeClass('notificacionShow');
    }, 1000);
    setTimeout(function(){
        $('span#not-'+notNum).addClass('notificacionHidden');
    }, 2000);
    setTimeout(function(){
        $('span#not-'+notNum).remove();
    }, 3000);
}