$(document).ready(function(){
    var estadisticas_app = {
        'path' : window.location.protocol + '//' + window.location.hostname + '/',
        'urlocation' : window.location.pathname,
        'crud_endpoint' : 'manager/estadisticas/crud/'
    };

    $('input.inp-estadisticas').on('change', function(){
        var idEstadistica = $(this).data('id');
        var esType = $(this).data('estype');
        var value = $(this).val();
        var data = {
            'type' : esType,
            'value' : value,
            'idEstadistica' : idEstadistica
        };

        console.log(data);

        $.ajax({
            url: estadisticas_app.path + estadisticas_app.crud_endpoint + 'edit',
            type: 'POST',
            data: data,
            success: function(rs) {
                var res = eval ('('+rs+')');
                if (esType == 'pts') {
                    location.reload();
                } else {
                    showNotificacion(res.msg, 'success');
                }
            }
        });

    });

});