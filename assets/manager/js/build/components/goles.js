$(document).ready(function(){
    var goleo_app = {
        'path' : window.location.protocol + '//' + window.location.hostname + '/',
        'urlocation' : window.location.pathname,
        'crud_endpoint' : 'manager/goleo/crud/'
    };

    $('.input-goleo').on('change', function() {
        var valor = $(this).val();
        var campo = $(this).data('campo');
        var id = $(this).data('id');
        var data = null;
        if (campo == 'nombre') {
            data = {
                'nombre' : valor,
                'idgoleo' : id
            };
        }
        if (campo == 'equipo') {
            data = {
                'equipo' : valor,
                'idgoleo' : id
            };
        }
        if (campo == 'goles') {
            data = {
                'goles' : valor,
                'idgoleo' : id
            };
        }
        //console.log(data);
        $.ajax({
            url: goleo_app.path + goleo_app.crud_endpoint + 'edit',
            type: 'POST',
            data: data,
            success: function(rs) {
                var res = eval ('('+rs+')');
                showNotificacion(res.msg, 'success');
            }
        });
    });

});