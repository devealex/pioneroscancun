$(document).ready(function(){
    var images_app = {
        'path' : window.location.protocol + '//' + window.location.hostname + '/',
        'urlocation' : window.location.pathname,
        'crud_endpoint' : 'manager/imagenes/crud/'
    };

    //DELETE IMAGE
    $('.del-img').on('click', function(){
        if (confirm("¿Desea de ELIMINAR esta imagen?")){
            var obj = createObjRequest();
            var action = 'delete';
            obj.datastore = {'idGallery' : $(this).data('idimage'), 'galleryImage' : $(this).data('galleryimage')};
            obj.endpoint = images_app.path + images_app.crud_endpoint + action;
            obj.notification.reload = true;
            obj.action = action;
            obj.debug = true;
            managementAjaxRequest(obj);
        }
    });

    $('form[id="form-imagen"]').on('submit', function(e){
        e.preventDefault();

        var data = new FormData($(this)[0]);

        //console.log(data);

        var valArchivo = $('input[name="archivo[]"]').val();

        if (valArchivo.length != 0) {
            $.ajax({
                url: $(this).attr('action'),
                type: 'POST',
                data: data,
                contentType: false,
                cache: false,
                processData:false,
                success: function(rs) {
                    console.log(rs);
                    location.reload();
                 }
            });
        } else {
            alert('Debes seleccionar almenos una imagen');
            $('input[name="archivo[]"]').addClass('is-invalid');
            $('input[name="archivo[]"]').focus();
        }

    });


    //Show image preview
    $('input[name="galleryImage"]').on('change', function(){
        filePreview(this);
    });

    function filePreview(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('.img-preview').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        } else {
            $('.img-preview').attr('src', "/assets/img/galerias/images/default.jpg");
        }
    }

});
