$(document).ready(function () {
    var calendario_app = {
        'path': window.location.protocol + '//' + window.location.hostname + '/',
        'urlocation': window.location.pathname,
        'crud_endpoint': 'manager/calendario/crud/'
    };

    //DELETE Calendar
    $('.del-calendario').on('click', function () {
        if (confirm("¿Desea de ELIMINAR este calendario?\r\nTodos los datos asociados a este calendario seran borrados.")) {
            var obj = createObjRequest();
            var action = 'delete';
            obj.datastore = { 'idCalendario': $(this).data('idcalendario') };
            obj.endpoint = calendario_app.path + calendario_app.crud_endpoint + action;
            obj.notification.reload = true;
            obj.action = action;
            obj.debug = true;
            managementAjaxRequest(obj);
        }
    });

    //STATUS
    $('select[name="isLocal"]').on('change', function () {
        var isLocal = $(this).val();
        if (isLocal == 1) {
            $('label[id="lbG1"]').attr('for', 'localGol');
            $('input[id="inpG1"]').attr('name', 'localGol');
            $('label[id="lbG2"]').attr('for', 'visitaGol');
            $('input[id="inpG2"]').attr('name', 'visitaGol');
        }
        if (isLocal == 0) {
            $('label[id="lbG1"]').attr('for', 'visitaGol');
            $('input[id="inpG1"]').attr('name', 'visitaGol');
            $('label[id="lbG2"]').attr('for', 'localGol');
            $('input[id="inpG2"]').attr('name', 'localGol');
        }
    });

    $('input.inp-goles').on('focusout', function () {
        var golType = $(this).data('goltype');
        var goles = $(this).val();
        var idCalendario = $(this).data('id');
        var data = null;
        var difGoles = $(this).data('goles');

        if (golType == 'localGol') {
            data = {
                'localGol': goles,
                'idCalendario': idCalendario
            };
        }

        if (golType == 'visitaGol') {
            data = {
                'visitaGol': goles,
                'idCalendario': idCalendario
            };
        }

        //var fkTorneoRol = $('input[name="fkTorneoRol"]').val();
        //var jornadaRol = $('input[name="jornadaRol"]').val();

        if (goles != difGoles) {
            $.ajax({
                url: calendario_app.path + calendario_app.crud_endpoint + 'goles',
                type: 'POST',
                data: data,
                success: function (rs) {
                    var res = eval('(' + rs + ')');
                    showNotificacion(res.msg, 'success');
                }
            });
            $(this).data('goles', goles);
        }

    });

    $('.inp-date-calendar, .inp-time-calendar').on('change', function(e) {
        var data = { 'idCalendario' : $(this).data('id') };
        var type = $(this).data('type');
        if (type == 'date') {
            data.playDay = $(this).val();
        } else if (type == 'time') {
            data.playTime = $(this).val();
        }
        $.ajax({
            url: calendario_app.path + calendario_app.crud_endpoint + 'updDateTime',
            type: 'POST',
            data: data,
            success: function (rs) {
                var res = eval('(' + rs + ')');
                showNotificacion(res.msg, 'success');
            }
        });
        console.log(data);
    });

    $('button#clear').on('click', function () {
        location.reload();
    });

    $('form#form-calendar').on('submit', function (e) {
        e.preventDefault();

        var data = managementFormData($(this));
        var obj = createObjRequest();
        var valid = true;

        console.log(data);

        if (data.localRol == "" || data.localRol.length == 0) {
            $('input[name="namLocal"]').addClass('is-invalid');
            $('input[name="namLocal"]').focus();
            valid = false;
        } else {
            $('input[name="namLocal"]').removeClass('is-invalid');
        }

        if (data.visitaRol == "" || data.visitaRol.length == 0) {
            $('input[name="namVisit"]').addClass('is-invalid');
            $('input[name="namVisit"]').focus();
            valid = false;
        } else {
            $('input[name="namVisit"]').removeClass('is-invalid');
        }

        if (data.playDay == "" || data.playDay.length == 0) {
            $('input[name="dateCal"]').addClass('is-invalid');
            $('input[name="dateCal"]').focus();
            valid = false;
        } else {
            $('input[name="dateCal"]').removeClass('is-invalid');
        }

        if (data.playTime == "" || data.playTime.length == 0) {
            $('input[name="timeCal"]').addClass('is-invalid');
            $('input[name="timeCal"]').focus();
            valid = false;
        } else {
            $('input[name="timeCal"]').removeClass('is-invalid');
        }

        if (valid == true) {

            obj.datastore = data;
            obj.endpoint = calendario_app.path + calendario_app.crud_endpoint + 'create';
            obj.endLocation = calendario_app.path + 'manager/calendario/listview?t=' + data.fkTorneoRol + '&j=' + data.jornadaRol;
            obj.action = 'create';
            obj.notification.reload = true;
            obj.debug = true;

            managementAjaxRequest(obj);

        }

    });

    $('input[name="dateCal"]').on('change', function () {
        var dateCalendar = $(this).val();
        $('input[name="playDay"]').val(dateCalendar);
    });

    $('input[name="timeCal"]').on('change', function () {
        var timeCalendar = $(this).val();
        $('input[name="playTime"]').val(timeCalendar);
    });

    $('span.select-team-calendar').on('click', function () {
        var id = $(this).data('id');
        var name = $(this).data('name');
        var isSelect = $(this).data('isselect');
        var namLocal = $('input[name="namLocal"]');
        var localRol = $('input[name="localRol"]');
        var namVisit = $('input[name="namVisit"]');
        var visitaRol = $('input[name="visitaRol"]');
        if (isSelect == 0) {
            if (namLocal.val() != "" && namVisit.val() == "") {
                namVisit.val(name);
                visitaRol.val(id);
                $(this).addClass('selected');
                $(this).data('isselect', 1);
            }
            if (namLocal.val() == "" && namVisit.val() == "") {
                namLocal.val(name);
                localRol.val(id);
                $(this).addClass('selected');
                $(this).data('isselect', 1);
            }
        }
    });

});
