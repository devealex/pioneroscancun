$(document).ready(function(){
    var galleries_app = {
        'path' : window.location.protocol + '//' + window.location.hostname + '/',
        'urlocation' : window.location.pathname,
        'crud_endpoint' : 'manager/galerias/crud/'
    };

    //DELETE Gallery
    $('.del-album').on('click', function(){
        if (confirm("¿Desea de ELIMINAR este album?\r\nTodos los datos asociados a este album seran borrados.")){
            var obj = createObjRequest();
            var action = 'delete';
            obj.datastore = {'idAlbum' : $(this).data('idalbum'), 'albumAvatar' : $(this).data('albumavatar')};
            obj.endpoint = galleries_app.path + galleries_app.crud_endpoint + action;
            obj.notification.reload = true;
            obj.action = action;
            obj.debug = true;
            managementAjaxRequest(obj);
        }
    });

    //Status del album
    $('input[name="albumPublish"]').on('change', function(e){
        e.preventDefault();
        var idAlbum = $(this).data('idalbum');
        var state = $(this).val();
        if (state == 1) {
            state = 0;
        } else if (state == 0) {
            state = 1;
        }
        $(this).val(state);
        var obj = createObjRequest();
        var action = 'state';
        obj.datastore = {'idAlbum' : idAlbum, 'albumPublish' : state};
        obj.endpoint = galleries_app.path + galleries_app.crud_endpoint + action;
        obj.notification.reload = false;
        obj.action = action;
        obj.debug = true;
        managementAjaxRequest(obj);
    });

    //Show image preview
    $('input[name="albumAvatar"]').on('change', function(){
        filePreview(this);
    });

    function filePreview(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('.img-preview').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        } else {
            $('.img-preview').attr('src', "/assets/img/galerias/albums/default.jpg");
        }
    }

    //FORM TO albums
    $('form[id="form-albums"]').on('submit', function(e){
        e.preventDefault();

        var action = $('input[name="action"]').val();
        var data = managementFormData($(this));
        var rs = true;
        var obj = createObjRequest();

        obj.datastore =  data;
        obj.endpoint = galleries_app.path + galleries_app.crud_endpoint + action;
        obj.endLocation = galleries_app.path + 'manager/galerias/listview';
        obj.action = action;
        obj.notification.reload = true;
        obj.debug = true;

        if (data.albumName == null || data.albumName.length == 0) {
            $('input[name="albumName"]').addClass('is-invalid');
            $('input[name="albumName"]').focus();
            rs = false;
        } else {
            $('input[name="albumName"]').removeClass('is-invalid');
        }
        if (data.albumAvatar == "" && action == 'create') {
            rs = false;
            alert('Debes elegir una imagen');
        }

        if (rs == true){
            if (data.albumAvatar != "") {
                addNewFile(this);
            } else {
                managementAjaxRequest(obj);
            }
        }

    });

    function addNewFile(form) {
        var data = new FormData(form);
        var url = galleries_app.path + 'manager/galerias/crud/image';
        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            contentType: false,
            cache: false,
            processData:false,
            success: function(rs) {
                var data = managementFormData(form);
                var obj = createObjRequest();
                var action = $('input[name="action"]').val();
                data['albumAvatar'] = rs;
                //Add data in object
                obj.datastore =  data;
                obj.endpoint = galleries_app.path + galleries_app.crud_endpoint + action;
                obj.endLocation = galleries_app.path + 'manager/galerias/listview';
                obj.action = action;
                obj.notification.reload = true;
                obj.debug = true;
                //Send the ajax
                managementAjaxRequest(obj);
             }
        });
    }

});
