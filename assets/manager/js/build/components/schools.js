$(document).ready(function(){
    var school_app = {
        'path' : window.location.protocol + '//' + window.location.hostname + '/',
        'urlocation' : window.location.pathname,
        'crud_endpoint' : 'manager/escuelas/crud/'
    };

    //DELETE SCHOOL
    $('.del-school').on('click', function(){
        if (confirm("¿Desea de ELIMINAR esta escuela?\r\nTodos los datos asociados a esta escuela seran borrados.")){
            var obj = createObjRequest();
            var action = 'delete';
            obj.datastore = {'idSchool' : $(this).data('idschool')};
            obj.endpoint = school_app.path + school_app.crud_endpoint + action;
            obj.notification.reload = true;
            obj.action = action;
            obj.debug = true;
            managementAjaxRequest(obj);
        }
    });

    //STATUS
    $('input[name="state"]').on('change', function(){
        var state = $(this).val();
        if (state == 1){
            state = 0;
        } else if (state == 0) {
            state = 1;
        }
        $(this).val(state);
        var obj = createObjRequest();
        var action = 'state';
        obj.datastore = {'idSchool' : $(this).data('idschool'), 'state' : state};
        obj.endpoint = school_app.path + school_app.crud_endpoint + action;
        obj.notification.reload = false;
        obj.action = action;
        obj.debug = true;
        managementAjaxRequest(obj);
    });

    //FORM EDIT
    $('form[id="form-school"]').on('submit', function(e){
        e.preventDefault();

        var data = managementFormData($(this));

        console.log(data);

        if (data.nameSchool == "" || data.nameSchool.length == 0) {
            $('input[name="nameSchool"]').addClass('is-invalid');
            $('input[name="nameSchool"]').focus();
        } else if (data.representative == "" || data.representative.length == 0) {
            $('input[name="nameSchool"]').removeClass('is-invalid');
            $('input[name="representative"]').addClass('is-invalid');
            $('input[name="representative"]').focus();
        } else if (data.contactNumber == "" || data.contactNumber.length == 0 || data.contactNumber.length < 10 || data.contactNumber.length > 10) {
            $('input[name="representative"]').removeClass('is-invalid');
            $('input[name="contactNumber"]').addClass('is-invalid');
            $('input[name="contactNumber"]').focus();
        } else if (data.email == "" || data.email.length == 0) {
            $('input[name="contactNumber"]').removeClass('is-invalid');
            $('input[name="email"]').addClass('is-invalid');
            $('input[name="email"]').focus();
        } else if (data.address == "" || data.address.length == 0) {
            $('input[name="email"]').removeClass('is-invalid');
            $('textarea[name="address"]').addClass('is-invalid');
            $('textarea[name="address"]').focus();
        } else {

            $.ajax({
                url: school_app.path + school_app.crud_endpoint + 'edit',
                type: 'post',
                data: data,
                success: function(data) {
                    var rs = eval ('('+data+')');
                    alert(rs.msg);
                    if (rs.state == 1) {
                        window.location = school_app.path + 'manager/escuelas/listview';
                    }
                }
            });

        }

    });

});
