$(document).ready(function(){

    var palmares_app = {
        'path' : window.location.protocol + '//' + window.location.hostname + '/',
        'urlocation' : window.location.pathname,
        'crud_endpoint' : 'manager/palmares/crud/'
    };

    //DELETE Palmares
    $('.del-palmares').on('click', function(){
        if (confirm("¿Desea de ELIMINAR este campeonato?")){
            var obj = createObjRequest();
            var action = 'delete';
            obj.datastore = {'idChampionship' : $(this).data('idchampionship'), 'cupAvatar' : $(this).data('cupavatar')};
            obj.endpoint = palmares_app.path + palmares_app.crud_endpoint + action;
            obj.notification.reload = true;
            obj.action = action;
            obj.debug = true;
            managementAjaxRequest(obj);
        }
    });

    //FORM TO Palmares
    $('form[id="form-palmares"]').on('submit', function(e){
        e.preventDefault();

        var action = $('input[name="action"]').val();
        var data = managementFormData($(this));
        var rs = true;
        var obj = createObjRequest();

        obj.datastore =  data;
        obj.endpoint = palmares_app.path + palmares_app.crud_endpoint + action;
        obj.endLocation = palmares_app.path + 'manager/palmares/listview';
        obj.action = action;
        obj.notification.reload = true;
        obj.debug = true;

        if (data.cupName == null || data.cupName.length == 0) {
            $('input[name="cupName"]').addClass('is-invalid');
            $('input[name="cupName"]').focus();
            rs = false;
        } else {
            $('input[name="cupName"]').removeClass('is-invalid');
        }
        if (data.championshipDate == null || data.championshipDate.length == 0 || data.championshipDate.length < 4) {
            $('input[name="championshipDate"]').addClass('is-invalid');
            $('input[name="championshipDate"]').focus();
            rs = false;
        } else {
            $('input[name="championshipDate"]').removeClass('is-invalid');
        }
        if (data.cupAvatar == "" && action == 'create') {
            rs = false;
            alert('Debes elegir una imagen (png)');
        }

        if (rs == true){
            if (data.cupAvatar != "") {
                addNewFile(this);
            } else {
                managementAjaxRequest(obj);
            }
        }

    });

    function addNewFile(form) {
        var data = new FormData(form);
        var url = palmares_app.path + 'manager/palmares/crud/image';
        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            contentType: false,
            cache: false,
            processData:false,
            success: function(rs) {
                var data = managementFormData(form);
                var obj = createObjRequest();
                var action = $('input[name="action"]').val();
                data['cupAvatar'] = rs;
                //Add data in object
                obj.datastore =  data;
                obj.endpoint = palmares_app.path + palmares_app.crud_endpoint + action;
                obj.endLocation = palmares_app.path + 'manager/palmares/listview';
                obj.action = action;
                obj.notification.reload = true;
                obj.debug = true;
                //Send the ajax
                managementAjaxRequest(obj);
             }
        });
    }

    //Show image preview
    $('input[name="cupAvatar"]').on('change', function(){
        filePreview(this);
    });

    function filePreview(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('.img-preview').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        } else {
            $('.img-preview').attr('src', "/assets/img/palmares/default.jpg");
        }
    }

});
