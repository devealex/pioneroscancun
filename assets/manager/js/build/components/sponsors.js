$(document).ready(function(){
    var sponsors_app = {
        'path' : window.location.protocol + '//' + window.location.hostname + '/',
        'urlocation' : window.location.pathname,
        'crud_endpoint' : 'manager/patrocinadores/crud/'
    };

    //DELETE Sponsor
    $('.del-sponsor').on('click', function(){
        if (confirm("¿Desea de ELIMINAR este Patrocinador?")){
            var obj = createObjRequest();
            var action = 'delete';
            obj.datastore = {
                'idSponsor' : $(this).data('idsponsor'),
                'logotype' : $(this).data('logotype')
            };
            obj.endpoint = sponsors_app.path + sponsors_app.crud_endpoint + action;
            obj.notification.reload = true;
            obj.action = action;
            obj.debug = true;
            managementAjaxRequest(obj);
        }
    });

    //FORM TO Sponsor
    $('form[id="form-sponsor"]').on('submit', function(e){
        e.preventDefault();

        var data = managementFormData($(this));
        var rs = true;

        console.log(data);

        if (data.sponsorOrder == null || data.sponsorOrder.length == 0) {
            $('input[name="sponsorOrder"]').addClass('is-invalid');
            $('input[name="sponsorOrder"]').focus();
            rs = false;
        } else {
            $('input[name="sponsorOrder"]').removeClass('is-invalid');
        }

        if ($('input[name="sponsorOrder"]').data('isvalid') == false) {
            $('input[name="sponsorOrder"]').addClass('is-invalid');
            $('input[name="sponsorOrder"]').focus();
            rs = false;
        } else {
            $('input[name="sponsorOrder"]').removeClass('is-invalid');
        }

        if (data.logotype == null || data.logotype.length == 0) {
            alert('Seleciona una imagen');
            rs = false;
        }

        if (rs == true){
            addNewFile(this);
        }

    });

    $('input[name="sponsorOrder"]').on('change', function(){
        var url = sponsors_app.path + 'manager/patrocinadores/crud/validateOrder';
        $.ajax({
            url: url,
            type: 'POST',
            data: {'sponsorOrder' : $(this).val()},
            success: function(rs) {
                console.log(rs);
                
                if (rs == true) {
                    $('input[name="sponsorOrder"]').removeClass('is-invalid');
                    $('input[name="sponsorOrder"]').data('isvalid', true);
                } else if (rs == false) {
                    $('input[name="sponsorOrder"]').addClass('is-invalid');
                    $('input[name="sponsorOrder"]').focus();
                    $('input[name="sponsorOrder"]').data('isvalid', false);
                }
             }
        });
    });

    function addNewFile(form) {
        var data = new FormData(form);
        var url = sponsors_app.path + 'manager/patrocinadores/crud/image';
        $.ajax({
            url: url,
            type: 'POST',
            data: data,
            contentType: false,
            cache: false,
            processData:false,
            success: function(rs) {
                var data = managementFormData(form);
                var obj = createObjRequest();
                var action = $('input[name="action"]').val();
                data['logotype'] = rs;
                //Add data in object
                obj.datastore =  data;
                obj.endpoint = sponsors_app.path + sponsors_app.crud_endpoint + action;
                obj.endLocation = sponsors_app.path + 'manager/patrocinadores/listview';
                obj.action = action;
                obj.notification.reload = true;
                obj.debug = true;
                //Send the ajax
                managementAjaxRequest(obj);
             }
        });
    }

    //Show image preview
    $('input[name="logotype"]').on('change', function(){
        filePreview(this);
    });

    function filePreview(input) {
        if (input.files && input.files[0]) {
            var reader = new FileReader();
            reader.onload = function(e) {
                $('.img-preview').attr('src', e.target.result);
            }
            reader.readAsDataURL(input.files[0]);
        } else {
            $('.img-preview').attr('src', "/assets/img/sponsors/default.jpg");
        }
    }

});