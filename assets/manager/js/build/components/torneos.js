$(document).ready(function(){
    var torneos_app = {
        'path' : window.location.protocol + '//' + window.location.hostname + '/',
        'urlocation' : window.location.pathname,
        'crud_endpoint' : 'manager/torneos/crud/'
    };

    //URL
    $('input[name="torneo"]').on('keyup', function() {
        var slug = $(this).val().toLowerCase().trim();
        slug = slug.replace(/ /g, '-');
        slug = slug.replace(/[^a-zA-Z0-9-]/g, '');
        slug = slug.replace(/----/g, '-');
        slug = slug.replace(/---/g, '-');
        slug = slug.replace(/--/g, '-');
        console.log(slug);
        $('input[name="torneourl"]').val(slug);
    });

    //DELETE TORNEO
    $('.del-torneo').on('click', function(){
        if (confirm("¿Desea de ELIMINAR este torneo?\r\nTodos los datos asociados a este torneo seran borrados.")){
            var obj = createObjRequest();
            var action = 'delete';
            obj.datastore = {'idTorneo' : $(this).data('idtorneo')};
            obj.endpoint = torneos_app.path + torneos_app.crud_endpoint + action;
            obj.notification.reload = true;
            obj.action = action;
            obj.debug = true;
            managementAjaxRequest(obj);
        }
    });

    //STATUS
    $('input[name="torneoAvailable"]').on('change', function(){
        var torneoAvailable = $(this).val();
        if (torneoAvailable == 1){
            torneoAvailable = 0;
        } else if (torneoAvailable == 0) {
            torneoAvailable = 1;
        }
        $(this).val(torneoAvailable);
        var obj = createObjRequest();
        var action = 'state';
        obj.datastore = {'idTorneo' : $(this).data('idtorneo'), 'torneoAvailable' : torneoAvailable};
        obj.endpoint = torneos_app.path + torneos_app.crud_endpoint + action;
        obj.notification.reload = false;
        obj.action = action;
        obj.debug = true;
        managementAjaxRequest(obj);
    });

    $('span.select-team').on('click', function(){
        var id = $(this).data('id');
        var isSelect = $(this).data('isselect');
        var equipos = $('input[name="equipos"]').val().split(',');
        if (equipos == null || equipos == '') {
            equipos = [];
        }
        var equiposDel = $('input[name="equiposDel"]').val().split(',');
        if (equiposDel == null || equiposDel == '') {
            equiposDel = [];
        }
        if ( isSelect == 0 ) {
            $(this).addClass('selected');
            $(this).data('isselect', 1);
            var i = equiposDel.indexOf(""+id+"");
            equiposDel.splice(i, 1);
            equipos.push(""+id+"");
            $('input[name="equiposDel"]').val(equiposDel);
            $('input[name="equipos"]').val(equipos);
            console.log(equipos);
        }
        if ( isSelect == 1 ) {
            $(this).removeClass('selected');
            $(this).data('isselect', 0);
            var i = equipos.indexOf(""+id+"");
            equipos.splice(i, 1);
            equiposDel.push(""+id+"");
            $('input[name="equiposDel"]').val(equiposDel);
            $('input[name="equipos"]').val(equipos);
            console.log(equipos);
        }
    });

    //FORM
    $('form[id="form-torneos"]').on('submit', function(e){
        e.preventDefault();

        var action = $('input[name="action"]').val();
        var data = managementFormData($(this));
        var obj = createObjRequest();

        console.log(data);

        if (data.torneo == "" || data.torneo.length == 0) {
            $('input[name="torneo"]').addClass('is-invalid');
            $('input[name="torneo"]').focus();
        } else if (data.torneoPlantel == "" || data.torneoPlantel.length == 0) {
            $('input[name="torneo"]').removeClass('is-invalid');
            $('select[name="torneoPlantel"]').addClass('is-invalid');
            $('select[name="torneoPlantel"]').focus();
        } else if (data.jornadas == "" || data.jornadas.length == 0) {
            $('select[name="torneoPlantel"]').removeClass('is-invalid');
            $('input[name="jornadas"]').addClass('is-invalid');
            $('input[name="jornadas"]').focus();
        } else {

            obj.datastore =  data;
            obj.endpoint = torneos_app.path + torneos_app.crud_endpoint + action;
            obj.endLocation = torneos_app.path + 'manager/torneos/listview';
            obj.action = action;
            obj.notification.reload = true;
            obj.debug = true;

            managementAjaxRequest(obj);

        }

    });

});
