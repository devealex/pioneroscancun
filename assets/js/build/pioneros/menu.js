$(document).ready(function(){
    $('button.btnMovil-show').on('click', function(){
        $(this).removeClass('show');
        $(this).addClass('hidden');
        $('button.btnMovil-hidden').removeClass('hidden');
        $('button.btnMovil-hidden').addClass('show');
        $('div.navmenu').removeClass('hidden');
        $('div.navmenu').addClass('show');
    });
    $('button.btnMovil-hidden').on('click', function(){
        $(this).addClass('hidden');
        $(this).removeClass('show');
        $('button.btnMovil-show').removeClass('hidden');
        $('button.btnMovil-show').addClass('show');
        $('div.navmenu').removeClass('show');
        $('div.navmenu').addClass('hidden');
    });



    //Full Menu
    var menu = false;
    $('.is-full-menu').hide();

    //Show Menu
    $('.is-box-menu').on('click', function(){
    
        if(menu == false) {
            $('.is-full-menu').fadeIn('last');
            menu = true;
        }    

    });


    //Hide menu
    $('#is-close-menu').on('click', function(){
        
        if(menu == true) {
            $('.is-full-menu').fadeOut('last');
            menu = false;
        }  

    });

});