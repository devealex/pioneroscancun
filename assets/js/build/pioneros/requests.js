//Inputs Management
function managementFormData(form) {
    var inputs = {};
    $.each($('input, select ,textarea', form),function(k){
        if($(this).attr('type') == 'checkbox') {
            inputs[$(this).attr('name')] = $(this).is(':checked') ? 1 : 0;
        }
        else {
            inputs[$(this).attr('name')] = $(this).val();
        }
    });
    return inputs;
}

//Inputs set required class and actions
function requiredInputRule(input, nclass, focus) {
    $('#'+input+'').addClass(''+nclass+'');
    if(focus == true) {
        $('#'+input+'').focus();
    }
}

//Object to send ajaxRequests
function createObjRequest() {
    var obj = {
        'datastore' : null,
        'endpoint' : null,
        'input' : null,
        'action' : null,
        'notification' : {
            'reload' : false,
            'hide' : {
                'dom' : false,
                'val' : false,
                'overlay' : null
            },
            'msg' : false
        },
        'debug' : false,
        'endLocation' : null
    };
    return obj;
}

//AJAX Management
function managementAjaxRequest(obj) {
    $.ajax({
        data: obj.datastore,
        url: obj.endpoint,
        type: 'POST',
        success: function(data) {
            if(obj.debug == true) {
                console.log(data);
            }
            var res = eval ('('+data+')');
            alert(res.msg);
            //CREATE
            if(obj.action == 'create') {
                if(res.state == 1) {
                    window.location = obj.endLocation;
                }
            }
            //EDIT
            if(obj.action == 'edit') {
                if(res.state == 1) {
                    window.location = obj.endLocation;
                }
            }
            //DELETE
            if(obj.action == 'delete' && res.state == 1) {
                if(obj.notification.reload == true && res.state == 1) {
                    location.reload();
                }
            }
        }
    });
};
