$(document).ready(function(){
    var modalTarget = null;
    var srcVideo = null;

    $('.modal-button').on('click', function(e){
        e.preventDefault();
        modalTarget = $(this).data('target');
        srcVideo = $('#' + modalTarget).find('iframe').prop('src');
        $('#' + modalTarget).addClass('is-active');
    });

    $('.modal-close').on('click', function(e) {
        e.preventDefault();
        $('#' + modalTarget).removeClass('is-active');
        $('#' + modalTarget).find('iframe').prop('src', srcVideo);
        modalTarget = null;
    });

    $('.modal-card-head').children('.delete').on('click', function(e) {
        e.preventDefault();
        $('#' + modalTarget).removeClass('is-active');
        $('#' + modalTarget).find('iframe').prop('src', srcVideo);
        modalTarget = null;
    });

    window.onclick = function(e) {
        if (event.target.className == 'modal-background') {
            $('#' + modalTarget).removeClass('is-active');
            $('#' + modalTarget).find('iframe').prop('src', srcVideo);
            modalTarget = null;
        }
    }

});
