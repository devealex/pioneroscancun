$(document).ready(function(){
    var contact_app = {
        'path' : window.location.protocol + '//' + window.location.hostname + '/',
        'urlocation' : window.location.pathname,
        'crud_endpoint' : 'manager/contact/'
    };

    $('form[id="contacto-form"]').on('submit', function(e){
        e.preventDefault();

        var data = managementFormData($(this));

        console.log(data);

        if (data.contactName == "" || data.contactName.length == 0) {
            $('input[name="contactName"]').addClass('is-danger');
            $('input[name="contactName"]').focus();
        } else if (data.contactNumber == "" || data.contactNumber.length == 0 || data.contactNumber.length < 10 || data.contactNumber.length > 10) {
            $('input[name="contactNumber"]').addClass('is-danger');
            $('input[name="contactNumber"]').focus();
        } else if (data.email == "" || data.email.length == 0) {
            $('input[name="email"]').addClass('is-danger');
            $('input[name="email"]').focus();
        } else if (data.contactSubject == "" || data.contactSubject.length == 0) {
            $('select[name="contactSubject"]').addClass('is-danger');
            $('select[name="contactSubject"]').focus();
        } else {

            $.ajax({
                url: contact_app.path + contact_app.crud_endpoint + "contactus",
                type: 'post',
                data: data,
                success: function(data) {
                    var rs = eval ('('+data+')');
                    alert(rs.msg);
                    if (rs.state == 1) {
                        location.reload();
                    }
                }
            });

        }

    });

});