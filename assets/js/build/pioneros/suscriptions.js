$(document).ready(function(){
    var subscription_app = {
        'path' : window.location.protocol + '//' + window.location.hostname + '/',
        'urlocation' : window.location.pathname,
        'crud_endpoint' : 'manager/subscriptions/'
    };

    $('form[id="frm-suscription"]').on('submit', function(e){
        e.preventDefault();

        var data = managementFormData($(this));

        console.log(data);

        if (data.email == "" || data.email.length == 0) {
            $('input[name="email"]').addClass('is-danger');
            $('input[name="email"]').focus();
        } else {

            $.ajax({
                url: subscription_app.path + subscription_app.crud_endpoint + "subscribe",
                type: 'post',
                data: data,
                success: function(data) {
                    var rs = eval ('('+data+')');
                    alert(rs.msg);
                    if (rs.state == 1) {
                        location.reload();
                    }
                }
            });

        }

    });

});