$(document).ready(function(){

    var schools_app = {
        'path' : window.location.protocol + '//' + window.location.hostname + '/',
        'urlocation' : window.location.pathname,
        'crud_endpoint' : 'manager/escuelas/crud/'
    };

    $('form[id="registro-escuela"]').on('submit', function(e){
        e.preventDefault();

        var data = managementFormData($(this));

        // console.log(data);

        if (data.nameSchool == "" || data.nameSchool.length == 0) {
            $('input[name="nameSchool"]').addClass('is-danger');
            $('input[name="nameSchool"]').focus();
        } else if (data.representative == "" || data.representative.length == 0) {
            $('input[name="representative"]').addClass('is-danger');
            $('input[name="representative"]').focus();
        } else if (data.contactNumber == "" || data.contactNumber.length == 0 || data.contactNumber.length < 10 || data.contactNumber.length > 10) {
            $('input[name="contactNumber"]').addClass('is-danger');
            $('input[name="contactNumber"]').focus();
        } else if (data.email == "" || data.email.length == 0) {
            $('input[name="email"]').addClass('is-danger');
            $('input[name="email"]').focus();
        } else if (data.address == "" || data.address.length == 0) {
            $('textarea[name="address"]').addClass('is-danger');
            $('textarea[name="address"]').focus();
        } else {

            $.ajax({
                url: schools_app.path + schools_app.crud_endpoint + 'create',
                type: 'post',
                data: data,
                success: function(data) {
                    var rs = eval ('('+data+')');
                    alert(rs.msg);
                    if (rs.state == 1) {
                        location.reload();
                    }
                }
            });

        }

    });

});
